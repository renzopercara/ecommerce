import React from "react";
import { themeSettings, text } from "../../lib/settings";

const LoginIcon = ({ loginIsActive }) => {
  if (loginIsActive) {
    return (
      <img
        src="/assets/images/close.svg"
        className="icon"
        alt={text.close}
        title={text.close}
        style={{ minWidth: 24, padding: 4 }}
      />
    );
  }
  return (
    <img
      src="/assets/images/login.svg"
      className="icon"
      alt={text.cart}
      title={text.cart}
      style={{ minWidth: 24 }}
    />
  );
};

export default class LoginIndicator extends React.PureComponent {
  render() {
    const { onClick, loginIsActive } = this.props;
    return (
      <span className="cart-button" onClick={onClick}>
        <LoginIcon loginIsActive={loginIsActive} />
      </span>
    );
  }
}
