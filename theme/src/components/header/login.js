import React from "react";
import { NavLink } from "react-router-dom";
import * as helper from "../../lib/helper";
import settings from "../../../../config/server";

export default class Login extends React.PureComponent {
  renderLogged = () => {
    return (
      <div className="mini-login">
        <a
          className="button is-primary is-fullwidth has-text-centered"
          style={{ textTransform: "uppercase" }}
          href={"/admin"}
        >
          Ver mi Perfil
        </a>
        <a href={"/admin/logout"}>Cerrar sesión</a>
      </div>
    );
  };

  renderNotLogged = () => {
    const { loginToggle } = this.props;
    return (
      <div className="mini-login">
        <ul>
          <li>
            <a href={settings.apiBaseUrl + "/auth/google"}>
              <button className="loginBtn loginBtn--google">
                Ingresar con Google
              </button>
            </a>
          </li>
          <li>
            <a href={settings.apiBaseUrl + "/auth/facebook"}>
              <button className="loginBtn loginBtn--facebook">
                Ingresar con Facebook
              </button>
            </a>
          </li>
        </ul>
      </div>
    );
  };

  render() {
    let token = "";
    if (typeof localStorage !== "undefined") {
      token = localStorage.getItem("dashboard_token");
    }

    if (token) {
      return this.renderLogged();
    }
    return this.renderNotLogged();
  }
}
