import React, { Fragment } from "react";
import { themeSettings, text } from "../../lib/settings";
import Item from "./item";
import LoadMore from "./loadMore";
import InfiniteScroll from "react-infinite-scroller";

const ProductList = ({
  products,
  addCartItem,
  settings,
  loadMoreProducts,
  hasMore,
  loadingProducts,
  loadingMoreProducts,
  isCentered,
  className = "columns is-multiline is-mobile products",
  columnCountOnMobile,
  columnCountOnTablet,
  columnCountOnDesktop,
  columnCountOnWidescreen,
  columnCountOnFullhd
}) => {
  const items = products
    ? products.map(product => (
        <Item
          key={product.id}
          product={product}
          addCartItem={addCartItem}
          settings={settings}
          columnCountOnMobile={columnCountOnMobile}
          columnCountOnTablet={columnCountOnTablet}
          columnCountOnDesktop={columnCountOnDesktop}
          columnCountOnWidescreen={columnCountOnWidescreen}
          columnCountOnFullhd={columnCountOnFullhd}
        />
      ))
    : null;

  return (
    <Fragment>
      {/* UTILIZAMOS COMPONENTE InfiniteScroll previamente instalado */}
      <InfiniteScroll
        pageStart={0}
        loadMore={loadMoreProducts !== null ? loadMoreProducts : () => {}} // Función que se ejecuta al completar scrolleo hacia abajo
        hasMore={hasMore} // Booleano que comprueba si hay o no más datos por cargar
        loader={
          <div style={{ flex: 1 }}>
            <div style={{ margin: "auto" }} className="loader" key={0} />
          </div>
        } // Componente de carga
      >
        <div
          className={
            className +
            (loadingProducts ? " loading" : "") +
            (isCentered ? " is-centered" : "")
          }
        >
          {items}
        </div>
      </InfiniteScroll>
      {/*
                    -----     QUITAMOS BOTÓN DE CARGA DE DATOS  ------
                    <div className="load-more">
                        <LoadMore
                            loadMoreProducts={loadMoreProducts}
                            hasMore={hasMore}
                            loading={loadingMoreProducts}
                        />
                    </div>
                */}
    </Fragment>
  );
};

export default ProductList;
