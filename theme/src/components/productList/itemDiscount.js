import React from "react";

const ItemDiscount = ({ discount }) => {
  if (discount && discount.general_percentage) {
    return (
      <div className="labels-floating">
        <div className="label label-circle label-text">
          <div className="p-top-quarter">
            <strong>{discount.general_percentage}% OFF</strong>
          </div>
          {/*<div className="label-small">Comprando 25 o más.</div>*/}
        </div>
      </div>
    );
  }
  return null;
};

export default ItemDiscount;
