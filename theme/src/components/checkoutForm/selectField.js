import React from "react";

const SelectField = field => (
  <div className={field.className}>
    <label htmlFor={field.id}>
      {field.label}
      {field.meta.touched &&
        field.meta.error && <span className="error">{field.meta.error}</span>}
    </label>
    <span className="select is-fullwidth">
      <select id={field.id} {...field.input}>
        {field.options}
      </select>
    </span>
  </div>
);

export default SelectField;
