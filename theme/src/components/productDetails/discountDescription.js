import React from "react";

const parseDiscountMessage = ({
  minorist_message,
  minorist_price,
  reseller_price,
  reseller_message,
  reseller_quantity,
  mayorist_price,
  mayorist_message,
  mayorist_quantity
}) => {
  return (
    <div>
      <div className="price-line">
        <div style={{ width: "70%", flexDirection: "column" }}>
          <p className="promo-title">{minorist_message}</p>
        </div>
        <span className="product-price">${minorist_price} c/u</span>
      </div>
      <div className="price-line">
        <div style={{ width: "70%", flexDirection: "column" }}>
          <p className="promo-title">Precio Revendedor</p>
          <p className="promo-description">{reseller_message}</p>
        </div>
        <span className="product-price">${reseller_price} c/u</span>
      </div>
      <div className="price-line">
        <div style={{ width: "70%", flexDirection: "column" }}>
          <p className="promo-title">Precio Mayorista</p>
          <p className="promo-description">
            Llevando a partir de {mayorist_quantity} unidades
          </p>
        </div>
        <span className="product-price">${mayorist_price} c/u</span>
      </div>
    </div>
  );
};

const DiscountDescription = props => {
  return parseDiscountMessage(props);
};

export default DiscountDescription;
