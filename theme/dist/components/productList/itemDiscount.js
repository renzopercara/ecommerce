import React from "react";

const ItemDiscount = ({ discount }) => {
  if (discount && discount.general_percentage) {
    return React.createElement(
      "div",
      { className: "labels-floating" },
      React.createElement(
        "div",
        { className: "label label-circle label-text" },
        React.createElement(
          "div",
          { className: "p-top-quarter" },
          React.createElement(
            "strong",
            null,
            discount.general_percentage,
            "% OFF"
          )
        )
      )
    );
  }
  return null;
};

export default ItemDiscount;