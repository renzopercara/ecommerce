import React, { Fragment } from "react";
import { themeSettings, text } from "../../lib/settings";
import Item from "./item";
import LoadMore from "./loadMore";
import InfiniteScroll from "react-infinite-scroller";

const ProductList = ({
  products,
  addCartItem,
  settings,
  loadMoreProducts,
  hasMore,
  loadingProducts,
  loadingMoreProducts,
  isCentered,
  className = "columns is-multiline is-mobile products",
  columnCountOnMobile,
  columnCountOnTablet,
  columnCountOnDesktop,
  columnCountOnWidescreen,
  columnCountOnFullhd
}) => {
  const items = products ? products.map(product => React.createElement(Item, {
    key: product.id,
    product: product,
    addCartItem: addCartItem,
    settings: settings,
    columnCountOnMobile: columnCountOnMobile,
    columnCountOnTablet: columnCountOnTablet,
    columnCountOnDesktop: columnCountOnDesktop,
    columnCountOnWidescreen: columnCountOnWidescreen,
    columnCountOnFullhd: columnCountOnFullhd
  })) : null;

  return React.createElement(
    Fragment,
    null,
    React.createElement(
      InfiniteScroll,
      {
        pageStart: 0,
        loadMore: loadMoreProducts !== null ? loadMoreProducts : () => {} // Función que se ejecuta al completar scrolleo hacia abajo
        , hasMore: hasMore // Booleano que comprueba si hay o no más datos por cargar
        , loader: React.createElement(
          "div",
          { style: { flex: 1 } },
          React.createElement("div", { style: { margin: "auto" }, className: "loader", key: 0 })
        ) // Componente de carga
      },
      React.createElement(
        "div",
        {
          className: className + (loadingProducts ? " loading" : "") + (isCentered ? " is-centered" : "")
        },
        items
      )
    )
  );
};

export default ProductList;