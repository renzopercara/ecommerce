import React from "react";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";
import { themeSettings, text } from "../lib/settings";
import appSettings from "../../../config/server";
import MessengerCustomerChat from "react-messenger-customer-chat";

class FooterMenu extends React.Component {
  constructor(props) {
    super(props);

    this.isActiveToggle = () => {
      this.setState({
        isActive: !this.state.isActive
      });
    };

    this.state = {
      isActive: false
    };
  }

  render() {
    const { title, items } = this.props;
    let ulItems = null;

    if (items && items.length > 0) {
      ulItems = items.map((item, index) => React.createElement(
        "li",
        { key: index },
        React.createElement(
          NavLink,
          { to: item.url || "" },
          item.text
        )
      ));
    }

    return React.createElement(
      "div",
      { className: "column is-3" },
      React.createElement(
        "div",
        {
          className: "footer-title mobile-padding" + (this.state.isActive ? " footer-menu-open" : ""),
          onClick: this.isActiveToggle
        },
        title,
        React.createElement("span", null)
      ),
      React.createElement(
        "ul",
        { className: "footer-menu" },
        ulItems
      )
    );
  }
}

const SocialIcons = ({ icons }) => {
  if (icons && icons.length > 0) {
    const items = icons.map((icon, index) => React.createElement("a", {
      key: index,
      href: icon.url || "",
      target: "_blank",
      rel: "noopener",
      title: icon.type,
      className: icon.type
    }));
    return React.createElement(
      "p",
      { className: "social-icons" },
      items
    );
  } else {
    return null;
  }
};

const Contacts = ({ contacts }) => {
  if (contacts && contacts.length > 0) {
    const items = contacts.map((item, index) => {
      const contact = item ? item.text : null;
      if (contact && contact.indexOf("@") > 0) {
        return React.createElement(
          "li",
          { key: index },
          React.createElement(
            "a",
            { href: "mailto:" + contact },
            contact
          )
        );
      } else {
        return React.createElement(
          "li",
          { key: index },
          contact
        );
      }
    });
    return React.createElement(
      "ul",
      { className: "footer-contacts" },
      items
    );
  } else {
    return null;
  }
};

export default class Footer extends React.PureComponent {

  constructor(props) {
    super(props);

    this.handleChange = e => {
      let contactFormData = Object.assign({}, this.state.contactFormData);
      contactFormData[e.target.name] = e.target.value;
      this.setState({ contactFormData });
    };

    this.state = {
      shadowButton: null,
      bottonButton: 15,
      bottonForm: "-100%",
      contactFormData: {
        name: "",
        email: "",
        messageText: ""
      }
    };
  }

  // Submit formulario de contacto
  submitContactForm(e) {
    const { name, email, message } = this.state.contactFormData;

    e.preventDefault();

    const data = { name, email, message };

    fetch(`${appSettings.ajaxBaseUrl}/contact`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    }).then(response => response.json()).then(data => {
      if (data !== undefined && data.success === true) {
        alert("Mensaje enviado correctamente");
      }
    });
  }

  // Al ingresar texto en inputs correspondientes al formulario de contacto


  render() {
    const { settings } = this.props;
    const { shadowButton, bottonButton, bottonForm } = this.state;
    const footerLogoUrl = themeSettings.footer_logo_url && themeSettings.footer_logo_url.length > 0 ? "/assets/images/" + themeSettings.footer_logo_url : settings.logo;

    /* ESTILOS BOTÓN WHATSAPP */
    const whatsappButtonStyle = {
      position: "fixed",
      left: 15,
      bottom: 15,
      zIndex: 800,
      width: 60,
      height: 60,
      minWidth: 24
    };

    // ESTILOS BOTÓN CONTACTO
    const contactButtonStyle = {
      position: "fixed",
      right: 120,
      bottom: bottonButton,
      display: "flex",
      zIndex: 900,
      flexDirection: "row",
      padding: 15,
      alignItems: "center",
      border: "none",
      borderRadius: 30,
      backgroundColor: "#FFC600",
      fontSize: 18,
      fontWeight: "bold",
      color: "#333333",
      cursor: "pointer",
      boxShadow: shadowButton
    };

    // ESTILOS FORMULARIO
    const formStyle = {
      position: "fixed",
      right: 15,
      bottom: bottonForm,
      zIndex: 1000,
      marginTop: 20,
      marginRight: "auto",
      marginLeft: "auto",
      width: "90%",
      maxWidth: "23.75rem",
      backgroundColor: "white",
      borderRadius: "15px 15px 0 0",
      boxShadow: "0 15px 35px rgba(50, 50, 93, 0.1), 0 5px 15px rgba(0, 0, 0, 0.07)",
      overflow: "hidden"
    };

    const headerFormStyle = {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      paddingLeft: 10,
      backgroundColor: "#FFC600"
    };

    const h1Style = {
      marginTop: 0,
      fontSize: 16,
      fontWeight: "bold",
      color: "#333333"
    };

    const fieldsetStyle = {
      padding: 0,
      marginTop: 10,
      border: 0
    };

    const labelStyle = {
      display: "inline-block",
      // marginBottom: 10,
      fontWeight: "bold",
      fontSize: 12,
      textTransform: "uppercase",
      touchAction: "manipulation"
    };

    const inputStyle = {
      display: "block",
      padding: ".5rem .75rem",
      width: "100%",
      maxWidth: "100%",
      maxHeight: 100,
      fontSize: "1rem",
      lineHeight: "1.25",
      color: "#55595c",
      backgroundColor: "#fff",
      backgroundClip: "padding-box",
      borderTop: 0,
      borderRight: 0,
      borderBottom: "1px solid #FFC600",
      borderLeft: 0,
      borderRadius: "3px",
      transition: "all 0.25s cubic-bezier(0.4, 0, 1, 1)"
    };

    const inputButtonFormStyle = {
      display: "inline-block",
      width: "100%",
      padding: ".75rem 1rem",
      marginTop: "1.618rem",
      fontWeight: 400,
      textAlign: "center",
      textTransform: "uppercase",
      color: "#fff",
      verticalAlign: "middle",
      whiteSpace: "nowrap",
      backgroundColor: "#ffab00",
      border: "1px solid transparent",
      boxShadow: "0 15px 35px rgba(50, 50, 93, 0.1), 0 5px 15px rgba(0, 0, 0, 0.07)",
      cursor: "pointer",
      userSelect: "none"
    };

    return React.createElement(
      "section",
      { className: "section section-footer" },
      React.createElement("hr", null),
      React.createElement(
        "footer",
        null,
        React.createElement(
          "div",
          null,
          React.createElement(
            "a",
            { href: "https://wa.me/" + appSettings.shopWhatsapp },
            React.createElement("img", {
              src: "/assets/images/whatsapp.png",
              className: "icon",
              alt: "whatsapp",
              title: "whatsapp",
              style: whatsappButtonStyle
            })
          )
        ),
        React.createElement(MessengerCustomerChat, {
          className: "chat-facebook",
          pageId: "394184387446953",
          appId: "407727513385745",
          shouldShowDialog: false,
          minimized: false,
          themeColor: "#4267B2",
          loggedInGreeting: "Hola!. Estoy conectada. En qu\xE9 puedo ayudarte?",
          loggedOutGreeting: "Hola!. Estoy conectada. En qu\xE9 puedo ayudarte?",
          greetingDialogDisplay: "show"
        }),
        React.createElement(
          "button",
          {
            className: "contactButtonStyle",
            style: contactButtonStyle,
            onClick: () => this.setState({ bottonButton: -200, bottonForm: 15 }),
            onMouseMove: () => this.setState({ shadowButton: "0px 0px 5px rgba(0, 0, 0, 0.6)" }),
            onMouseLeave: () => this.setState({ shadowButton: null })
          },
          React.createElement(
            "svg",
            {
              style: { width: 20 },
              x: "0px",
              y: "0px",
              viewBox: "0 0 28 18",
              "aria-label": "small picture of an envelope signifying email",
              role: "img",
              "data-reactid": ".0.4.0"
            },
            React.createElement(
              "title",
              { "data-reactid": ".0.4.0.0" },
              "Envelope Icon"
            ),
            React.createElement(
              "desc",
              { "data-reactid": ".0.4.0.1" },
              "small picture of an envelope signifying email"
            ),
            React.createElement("path", {
              fill: "#333333",
              d: "M28,3.2C28,1.5,26.5,0,24.8,0H3.2C1.5,0,0,1.5,0,3.2v11.5C0,16.5,1.5,18,3.2,18h21.5c1.8,0,3.2-1.5,3.2-3.2 V3.2z M24.7,14.8c-0.2,0.1-0.4,0.2-0.6,0.2c-0.2,0-0.4-0.1-0.6-0.3l-5-5.3l-3.6,3c-0.3,0.2-0.6,0.3-0.9,0.3s-0.7-0.1-0.9-0.4l-3.6-3 l-5,5.3c-0.2,0.2-0.4,0.3-0.6,0.3c-0.2,0-0.4-0.1-0.6-0.2c-0.3-0.3-0.4-0.8,0-1.2l4.9-5.3L3.3,4.5C3,4.2,2.9,3.6,3.2,3.3 C3.5,2.9,4,2.9,4.4,3.2l9.6,7.9l9.6-7.9c0.4-0.3,0.9-0.2,1.2,0.1c0.3,0.4,0.2,0.9-0.1,1.2l-4.8,3.9l4.9,5.3 C25.1,14,25,14.5,24.7,14.8z",
              "data-reactid": ".0.4.0.2"
            })
          ),
          React.createElement(
            "p",
            { style: { marginLeft: 10, color: "#333333" } },
            "Contacto"
          )
        ),
        React.createElement(
          "form",
          { style: formStyle, onSubmit: e => this.submitContactForm(e) },
          React.createElement(
            "div",
            { style: headerFormStyle },
            React.createElement(
              "h1",
              { style: h1Style },
              "Contacto"
            ),
            React.createElement(
              "svg",
              {
                style: { cursor: "pointer" },
                xmlns: "http://www.w3.org/2000/svg",
                height: "48",
                viewBox: "0 0 48 48",
                width: "48",
                fill: "#333333",
                onClick: () => this.setState({ bottonForm: "-100%", bottonButton: 15 })
              },
              React.createElement("path", { d: "M14.83 16.42l9.17 9.17 9.17-9.17 2.83 2.83-12 12-12-12z" }),
              React.createElement("path", { d: "M0-.75h48v48h-48z", fill: "none" })
            )
          ),
          React.createElement(
            "div",
            { style: { padding: "0 20px 20px 20px" } },
            React.createElement(
              "fieldset",
              { style: fieldsetStyle },
              React.createElement(
                "label",
                { style: labelStyle },
                "Nombre"
              ),
              React.createElement("input", {
                style: inputStyle,
                type: "text",
                name: "name",
                placeholder: "Ingrese su nombre",
                required: true,
                onChange: this.handleChange
              })
            ),
            React.createElement(
              "fieldset",
              { style: fieldsetStyle },
              React.createElement(
                "label",
                { style: labelStyle },
                "Correo"
              ),
              React.createElement("input", {
                style: inputStyle,
                id: "formName",
                name: "email",
                placeholder: "Ingrese su correo",
                type: "email",
                required: true,
                onChange: this.handleChange
              })
            ),
            React.createElement(
              "fieldset",
              { style: fieldsetStyle },
              React.createElement(
                "label",
                { style: labelStyle },
                "Mensaje"
              ),
              React.createElement("textarea", {
                id: "formMessage",
                style: inputStyle,
                placeholder: "Mensaje",
                name: "message",
                required: true,
                onChange: this.handleChange
              })
            ),
            React.createElement("input", {
              id: "formButton",
              style: inputButtonFormStyle,
              type: "submit",
              placeholder: "Send message"
            })
          )
        ),
        React.createElement(
          "div",
          { className: "container" },
          React.createElement(
            "div",
            { className: "content" },
            React.createElement(
              "div",
              { className: "columns is-gapless" },
              React.createElement(
                "div",
                { className: "column is-5" },
                React.createElement(
                  "div",
                  { className: "mobile-padding" },
                  React.createElement(
                    "div",
                    { className: "footer-logo" },
                    React.createElement("img", { src: footerLogoUrl, alt: "logo" })
                  ),
                  React.createElement(
                    "p",
                    null,
                    React.createElement(
                      "small",
                      null,
                      themeSettings.footer_about
                    )
                  ),
                  React.createElement(Contacts, { contacts: themeSettings.footer_contacts }),
                  React.createElement(SocialIcons, { icons: themeSettings.footer_social })
                )
              ),
              React.createElement("div", { className: "column is-1 is-hidden-mobile" }),
              React.createElement(FooterMenu, {
                title: themeSettings.footer_menu_1_title,
                items: themeSettings.footer_menu_1_items
              }),
              React.createElement(FooterMenu, {
                title: themeSettings.footer_menu_2_title,
                items: themeSettings.footer_menu_2_items
              })
            )
          )
        )
      )
    );
  }
}
Footer.propTypes = {
  settings: PropTypes.shape({}).isRequired
};