import React, { Fragment } from "react";
import { NavLink } from "react-router-dom";
import * as helper from "../../lib/helper";
import { themeSettings, text } from "../../lib/settings";
import Disqus from "../comments/disqus";
import ViewedProducts from "../products/viewed";
import Breadcrumbs from "./breadcrumbs";
import DiscountCountdown from "./discountCountdown";
import AddToCartButton from "./addToCartButton";
import Attributes from "./attributes";
import Gallery from "./gallery";
import Options from "./options";
import Price from "./price";
import Quantity from "./quantity";
import RelatedProducts from "./relatedProducts";
import Tags from "./tags";
import SocialShareButton from "./socialShare";
import ItemDiscount from "./discountDescription";

const Description = ({ description }) => React.createElement("div", {
  className: "product-content",
  dangerouslySetInnerHTML: { __html: description }
});

export default class ProductDetails extends React.Component {
  constructor(props) {
    super(props);

    this.setQuantity = quantity => {
      this.setState({ quantity: quantity });
    };

    this.state = {
      selectedOptions: {},
      selectedVariant: null,
      isAllOptionsSelected: false,
      quantity: 1,
      colorSelected: "" // Creo estado del color seleccionado para asignarle al componente
    };

    this.onOptionChange = this.onOptionChange.bind(this);
    this.findVariantBySelectedOptions = this.findVariantBySelectedOptions.bind(this);
    this.addToCart = this.addToCart.bind(this);
    this.checkSelectedOptions = this.checkSelectedOptions.bind(this);
  }

  onOptionChange(optionId, valueId, optionName, valueName) {
    let { selectedOptions } = this.state;
    if (valueId === "") {
      delete selectedOptions[optionId];
    } else {
      selectedOptions[optionId] = valueId;
    }

    // Si option es Color entonces modifico estado del color seleccionado para mostrarlo en pantalla
    this.setState({
      selectedOptions: selectedOptions,
      colorSelected: optionName === "Color" ? valueName : this.state.colorSelected
    }, () => {
      this.findVariantBySelectedOptions();
      this.checkSelectedOptions();
    });
  }

  findVariantBySelectedOptions() {
    const { selectedOptions } = this.state;
    const { product } = this.props;

    for (const variant of product.variants) {
      const variantMutchSelectedOptions = variant.options.every(variantOption => selectedOptions[variantOption.option_id] === variantOption.value_id);
      if (variantMutchSelectedOptions) {
        this.setState({ selectedVariant: variant });
        return;
      }
    }
    this.setState({ selectedVariant: null });
  }

  addToCart() {
    const { product, addCartItem } = this.props;
    const { selectedVariant, quantity } = this.state;

    let item = {
      product_id: product.id,
      quantity: quantity
    };

    if (selectedVariant) {
      item.variant_id = selectedVariant.id;
    }

    addCartItem(item);
  }

  checkSelectedOptions() {
    const { selectedOptions } = this.state;
    const { product } = this.props;

    const allOptionsSelected = Object.keys(selectedOptions).length === product.options.length;
    this.setState({ isAllOptionsSelected: allOptionsSelected });
  }

  render() {
    const { product, settings, categories } = this.props;
    const { selectedVariant, isAllOptionsSelected } = this.state;
    const maxQuantity = product.stock_status === "discontinued" ? 0 : product.stock_backorder ? themeSettings.maxCartItemQty : selectedVariant ? selectedVariant.stock_quantity : product.stock_quantity;

    if (product) {
      return React.createElement(
        Fragment,
        null,
        React.createElement(
          "section",
          { className: "section section-product" },
          React.createElement(
            "div",
            { className: "container" },
            React.createElement(
              "div",
              { className: "columns" },
              React.createElement(
                "div",
                { className: "column is-7" },
                themeSettings.show_product_breadcrumbs && React.createElement(Breadcrumbs, { product: product, categories: categories }),
                React.createElement(Gallery, { images: product.images })
              ),
              React.createElement(
                "div",
                { className: "column is-5" },
                React.createElement(
                  "div",
                  { className: "content" },
                  React.createElement(Tags, { tags: product.tags }),
                  React.createElement(
                    "h1",
                    { className: "title is-4 product-name" },
                    product.name
                  ),
                  React.createElement(ItemDiscount, {
                    discount: product.discount,
                    reseller_price: product.reseller_price,
                    mayorist_price: product.mayorist_price,
                    minorist_price: product.minorist_price,
                    reseller_message: product.reseller_message,
                    mayorist_message: product.mayorist_message,
                    minorist_message: product.minorist_message,
                    reseller_quantity: product.reseller_quantity,
                    mayorist_quantity: product.mayorist_quantity
                  }),
                  React.createElement(Options, {
                    options: product.options,
                    onChange: this.onOptionChange,
                    colorSelected: this.state.colorSelected //Envio estado como propiedad
                  }),
                  React.createElement(Quantity, {
                    maxQuantity: maxQuantity,
                    onChange: this.setQuantity
                  }),
                  React.createElement(
                    "div",
                    { className: "button-addtocart" },
                    React.createElement(AddToCartButton, {
                      product: product,
                      variant: selectedVariant,
                      addCartItem: this.addToCart,
                      isAllOptionsSelected: isAllOptionsSelected
                    })
                  ),
                  React.createElement(
                    "div",
                    { className: "button-addtocart" },
                    React.createElement(SocialShareButton, { product: product })
                  )
                )
              )
            )
          )
        ),
        React.createElement(
          "section",
          { className: "section section-product-description" },
          React.createElement(
            "div",
            { className: "container" },
            React.createElement(
              "div",
              { className: "content" },
              React.createElement(
                "div",
                { className: "columns" },
                React.createElement(
                  "div",
                  { className: "column is-7" },
                  React.createElement(Description, { description: product.description })
                ),
                React.createElement(
                  "div",
                  { className: "column is-5" },
                  React.createElement(Attributes, { attributes: product.attributes })
                )
              )
            )
          )
        ),
        React.createElement(RelatedProducts, {
          settings: settings,
          addCartItem: this.addToCart,
          ids: product.related_product_ids,
          limit: 10
        }),
        themeSettings.show_viewed_products && React.createElement(ViewedProducts, {
          settings: settings,
          addCartItem: this.addToCart,
          product: product,
          limit: themeSettings.limit_viewed_products || 4
        }),
        themeSettings.disqus_shortname && themeSettings.disqus_shortname !== "" && React.createElement(
          "section",
          { className: "section" },
          React.createElement(
            "div",
            { className: "container" },
            React.createElement(Disqus, {
              shortname: themeSettings.disqus_shortname,
              identifier: product.id,
              title: product.name,
              url: product.url
            })
          )
        )
      );
    } else {
      return null;
    }
  }
}