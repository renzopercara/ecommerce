import React from "react";
import { NavLink } from "react-router-dom";
import * as helper from "../../lib/helper";
import { themeSettings, text } from "../../lib/settings";

// CREAMOS CONSTANTE CON TODOS LOS COLORES
const colores = [{
  name: "Rojo",
  hex: "#FF0000"
}, {
  name: "Azul",
  hex: "#0000FF"
}, {
  name: "Blanco",
  hex: "#FFFFFF"
}, {
  name: "Amarillo",
  hex: "#F7FF00"
}, {
  name: "Verde",
  hex: "#1FFF00"
}, {
  name: "Negro",
  hex: "#000000"
}, {
  name: "Naranja",
  hex: "#FF9B00"
}, {
  name: "Celeste",
  hex: "#00FFDC"
}, {
  name: "Violeta",
  hex: "#A200FF"
}, {
  name: "Rosa",
  hex: "#F45EDD"
}, {
  name: "Fucsia",
  hex: "#FF00C1"
}, {
  name: "Marrón",
  hex: "#874E00"
}, {
  name: "Dorado",
  hex: "#C5A400"
}, {
  name: "Crema",
  hex: "#E8DB9A"
}, {
  name: "Gris",
  hex: "#86877D"
}, {
  name: "Plateado",
  hex: "#C1C1C1"
}];

const Option = ({ option, onChange, colorSelected }) => {
  const values = option.values.sort((a, b) => a.name > b.name ? 1 : b.name > a.name ? -1 : 0).map((value, index) => React.createElement(
    "option",
    { key: index, value: value.id },
    value.name
  ));

  // CREAMOS CONSTANTE PARTICULAR DE COLORES
  const valuesColor = option.values.sort((a, b) => a.name > b.name ? 1 : b.name > a.name ? -1 : 0).map((value, index) => {
    // Obtenemos color a mostrar
    const color = colores.find(c => c.name === value.name);

    // Guardamos el codigo hexadecimal del color a mostrar
    const hex = color !== undefined ? color.hex : "#FFFFF";

    // Booleano comprobante de la existencia o no del color en la constante colores
    const colorTrue = color !== undefined;

    if (colorTrue) {
      // En el caso de que exista color en la constante colores
      return React.createElement(
        "div",
        {
          style: {
            width: 40,
            height: 40,
            marginRight: 10,
            padding: 3,
            backgroundColor: "white",
            borderRadius: 50,
            border: "3px solid",
            borderColor: "rgba(0,0,0,.2)",
            cursor: "pointer"
          },
          onClick: () => {
            onChange(option.id, value.id, option.name, value.name);
          }
        },
        React.createElement("div", {
          style: {
            width: "100%",
            height: "100%",
            backgroundColor: hex,
            borderRadius: 50
          }
        })
      );
    } else {
      // En el caso de que no exista color en la constante colores
      return React.createElement(
        "div",
        {
          style: {
            display: "flex",
            alignItems: "center",
            marginRight: 10,
            cursor: "pointer"
          },
          onClick: () => {
            onChange(option.id, value.id, option.name, value.name);
          }
        },
        value.name
      );
    }
  });
  const notSelectedTitle = `${text.selectOption} ${option.name}`;

  return React.createElement(
    "div",
    { className: "product-option" },
    React.createElement(
      "div",
      { className: "product-option-name" },
      // En el caso de que option sea color mostramos el valor seleccionado
      option.name !== "Color" ? option.name : option.name + ": " + colorSelected
    ),
    option.name !== "Color" ? React.createElement(
      "span",
      { className: "select is-fullwidth" },
      React.createElement(
        "select",
        {
          onChange: e => {
            onChange(option.id, e.target.value, option.name, "");
          }
        },
        React.createElement(
          "option",
          { value: "" },
          notSelectedTitle
        ),
        values
      )
    ) :
    // En el caso de que option sea color llamamos a la constante valuesColor
    React.createElement(
      "div",
      { style: { display: "flex", flexDirection: "row" } },
      valuesColor
    )
  );
};

const Options = ({ options, onChange, colorSelected }) => {
  if (options && options.length > 0) {
    const items = options.map((option, index) => React.createElement(Option, {
      key: index,
      option: option,
      onChange: onChange,
      colorSelected: colorSelected
    }));

    return React.createElement(
      "div",
      { className: "product-options" },
      items
    );
  } else {
    return null;
  }
};
export default Options;