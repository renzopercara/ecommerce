import React from "react";

const parseDiscountMessage = ({
  minorist_message,
  minorist_price,
  reseller_price,
  reseller_message,
  reseller_quantity,
  mayorist_price,
  mayorist_message,
  mayorist_quantity
}) => {
  return React.createElement(
    "div",
    null,
    React.createElement(
      "div",
      { className: "price-line" },
      React.createElement(
        "div",
        { style: { width: "70%", flexDirection: "column" } },
        React.createElement(
          "p",
          { className: "promo-title" },
          minorist_message
        )
      ),
      React.createElement(
        "span",
        { className: "product-price" },
        "$",
        minorist_price,
        " c/u"
      )
    ),
    React.createElement(
      "div",
      { className: "price-line" },
      React.createElement(
        "div",
        { style: { width: "70%", flexDirection: "column" } },
        React.createElement(
          "p",
          { className: "promo-title" },
          "Precio Revendedor"
        ),
        React.createElement(
          "p",
          { className: "promo-description" },
          reseller_message
        )
      ),
      React.createElement(
        "span",
        { className: "product-price" },
        "$",
        reseller_price,
        " c/u"
      )
    ),
    React.createElement(
      "div",
      { className: "price-line" },
      React.createElement(
        "div",
        { style: { width: "70%", flexDirection: "column" } },
        React.createElement(
          "p",
          { className: "promo-title" },
          "Precio Mayorista"
        ),
        React.createElement(
          "p",
          { className: "promo-description" },
          "Llevando a partir de ",
          mayorist_quantity,
          " unidades"
        )
      ),
      React.createElement(
        "span",
        { className: "product-price" },
        "$",
        mayorist_price,
        " c/u"
      )
    )
  );
};

const DiscountDescription = props => {
  return parseDiscountMessage(props);
};

export default DiscountDescription;