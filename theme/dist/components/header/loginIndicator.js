import React from "react";
import { themeSettings, text } from "../../lib/settings";

const LoginIcon = ({ loginIsActive }) => {
  if (loginIsActive) {
    return React.createElement("img", {
      src: "/assets/images/close.svg",
      className: "icon",
      alt: text.close,
      title: text.close,
      style: { minWidth: 24, padding: 4 }
    });
  }
  return React.createElement("img", {
    src: "/assets/images/login.svg",
    className: "icon",
    alt: text.cart,
    title: text.cart,
    style: { minWidth: 24 }
  });
};

export default class LoginIndicator extends React.PureComponent {
  render() {
    const { onClick, loginIsActive } = this.props;
    return React.createElement(
      "span",
      { className: "cart-button", onClick: onClick },
      React.createElement(LoginIcon, { loginIsActive: loginIsActive })
    );
  }
}