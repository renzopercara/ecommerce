import React from "react";
import { NavLink } from "react-router-dom";
import { themeSettings, text } from "../../lib/settings";
import * as helper from "../../lib/helper";

const CartItem = ({
  item,
  deleteCartItem,
  settings,
  updateCartItemQuantity: updateCartItemQuantity
}) => {
  const thumbnail = helper.getThumbnailUrl(item.image_url, themeSettings.cartThumbnailWidth);

  const maxQty = item.stock_backorder ? themeSettings.maxCartItemQty : item.stock_quantity >= themeSettings.maxCartItemQty ? themeSettings.maxCartItemQty : item.stock_quantity;

  return React.createElement(
    "div",
    { className: "columns is-mobile" },
    React.createElement(
      "div",
      { className: "column is-2" },
      React.createElement(
        "div",
        { className: "image" },
        React.createElement(
          NavLink,
          { to: item.path },
          React.createElement("img", { src: thumbnail })
        )
      )
    ),
    React.createElement(
      "div",
      { className: "column" },
      React.createElement(
        "div",
        null,
        React.createElement(
          NavLink,
          { to: item.path },
          item.name
        )
      ),
      item.variant_name.length > 0 && React.createElement(
        "div",
        { className: "cart-option-name" },
        item.variant_name
      ),
      React.createElement(
        "div",
        { className: "qty" },
        React.createElement(
          "span",
          null,
          text.qty,
          ":"
        ),
        React.createElement(
          "div",
          { className: "product-quantity" },
          React.createElement("a", {
            className: "decrement",
            onClick: e => {
              updateCartItemQuantity(item.id, item.quantity - 1);
            }
          }),
          React.createElement("input", {
            type: "number",
            value: item.quantity,
            maxLength: "3",
            pattern: "\\d*"
          }),
          React.createElement("a", {
            className: "increment",
            onClick: e => {
              updateCartItemQuantity(item.id, item.quantity + 1);
            }
          })
        )
      )
    ),
    React.createElement(
      "div",
      { className: "column is-4 has-text-right" },
      React.createElement(
        "div",
        { className: "mini-cart-item-price" },
        helper.formatCurrency(item.price_total, settings)
      ),
      React.createElement(
        "a",
        {
          className: "button is-light is-small",
          onClick: () => deleteCartItem(item.id)
        },
        text.remove
      )
    )
  );
};

export default class Cart extends React.PureComponent {
  render() {
    const {
      cart,
      deleteCartItem,
      settings,
      cartToggle,
      onClose,
      updateCartItemQuantity
    } = this.props;

    const btnClose = React.createElement(
      "div",
      {
        className: "button is-primary is-fullwidth has-text-centered",
        style: { textTransform: "uppercase", marginTop: 15 },
        onClick: () => onClose()
      },
      "CERRAR"
    );

    if (cart && cart.items && cart.items.length > 0) {
      const items = cart.items.map(item => React.createElement(CartItem, {
        key: item.id,
        item: item,
        deleteCartItem: deleteCartItem,
        settings: settings,
        updateCartItemQuantity: updateCartItemQuantity
      }));

      return React.createElement(
        "div",
        { className: "mini-cart" },
        items,
        React.createElement("hr", { className: "separator" }),
        React.createElement(
          "div",
          { className: "columns is-mobile is-gapless" },
          React.createElement(
            "div",
            { className: "column is-7" },
            React.createElement(
              "b",
              null,
              text.subtotal
            )
          ),
          React.createElement(
            "div",
            { className: "column is-5 has-text-right" },
            React.createElement(
              "b",
              null,
              helper.formatCurrency(cart.subtotal, settings)
            )
          )
        ),
        React.createElement(
          NavLink,
          {
            className: "button is-primary is-fullwidth has-text-centered",
            style: { textTransform: "uppercase" },
            to: "/checkout",
            onClick: cartToggle
          },
          text.proceedToCheckout
        ),
        btnClose
      );
    }
    return React.createElement(
      "div",
      { className: "mini-cart" },
      React.createElement(
        "p",
        null,
        text.cartEmpty
      ),
      btnClose
    );
  }
}