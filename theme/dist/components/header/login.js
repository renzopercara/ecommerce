import React from "react";
import { NavLink } from "react-router-dom";
import * as helper from "../../lib/helper";
import settings from "../../../../config/server";

export default class Login extends React.PureComponent {
  constructor(...args) {
    var _temp;

    return _temp = super(...args), this.renderLogged = () => {
      return React.createElement(
        "div",
        { className: "mini-login" },
        React.createElement(
          "a",
          {
            className: "button is-primary is-fullwidth has-text-centered",
            style: { textTransform: "uppercase" },
            href: "/admin"
          },
          "Ver mi Perfil"
        ),
        React.createElement(
          "a",
          { href: "/admin/logout" },
          "Cerrar sesi\xF3n"
        )
      );
    }, this.renderNotLogged = () => {
      const { loginToggle } = this.props;
      return React.createElement(
        "div",
        { className: "mini-login" },
        React.createElement(
          "ul",
          null,
          React.createElement(
            "li",
            null,
            React.createElement(
              "a",
              { href: settings.apiBaseUrl + "/auth/google" },
              React.createElement(
                "button",
                { className: "loginBtn loginBtn--google" },
                "Ingresar con Google"
              )
            )
          ),
          React.createElement(
            "li",
            null,
            React.createElement(
              "a",
              { href: settings.apiBaseUrl + "/auth/facebook" },
              React.createElement(
                "button",
                { className: "loginBtn loginBtn--facebook" },
                "Ingresar con Facebook"
              )
            )
          )
        )
      );
    }, _temp;
  }

  render() {
    let token = "";
    if (typeof localStorage !== "undefined") {
      token = localStorage.getItem("dashboard_token");
    }

    if (token) {
      return this.renderLogged();
    }
    return this.renderNotLogged();
  }
}