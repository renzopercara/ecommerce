// config used by server side only
const dbHost = process.env.DB_HOST || "127.0.0.1";
const dbPort = process.env.DB_PORT || 27017;
const dbName = process.env.DB_NAME || "shop";
const dbUser = process.env.DB_USER || "";
const dbPass = process.env.DB_PASS || "";
const dbCred =
  dbUser.length > 0 || dbPass.length > 0 ? `${dbUser}:${dbPass}@` : "";

const dbUrl =
  process.env.DB_URL || `mongodb://${dbCred}${dbHost}:${dbPort}/${dbName}`;

module.exports = {
  // used by Store (server side)
  apiBaseUrl: `http://shops.evolve-sdf.net/api/v1`,

  // used by Store (server and client side)
  ajaxBaseUrl: `http://shops.evolve-sdf.net/ajax`,

  // Access-Control-Allow-Origin
  storeBaseUrl: "http://shops.evolve-sdf.net",

  // used by API
  adminLoginUrl: "/admin/login",

  apiListenPort: 8084,
  storeListenPort: 8083,

  // used by API
  mongodbServerUrl: dbUrl,

  smtpServer: {
    host: "belenus.servidoraweb.net",
    port: 465,
    secure: true,
    user: "info@evolve-sdf.com",
    pass: "EHLOlocalhostmailfrom:",
    fromName: "Evolve Shops",
    fromAddress: "info@evolve-sdf.com"
  },

  // key to sign tokens
  jwtSecretKey: "SP69kXFR3znRi7kL8Max2GTB24wOtEQj",

  // key to sign store cookies
  cookieSecretKey: "8669X9P5yI1DAEthy1chc3M9EncyS7SM",

  // path to uploads
  categoriesUploadPath: "public/content/images/categories",
  productsUploadPath: "public/content/images/products",
  filesUploadPath: "public/content",
  themeAssetsUploadPath: "theme/assets/images",

  // url to uploads
  categoriesUploadUrl: "/images/categories",
  productsUploadUrl: "/images/products",
  filesUploadUrl: "",
  themeAssetsUploadUrl: "/assets/images",

  // store UI language
  language: "es",

  // used by API
  orderStartNumber: 1000,
  developerMode: true,

  //google API credentials
  googleApiKey: "AIzaSyCVo0OMEk-pCPmU5u3pRCcviOjKKKnZvf8",
  googleClientId:
    "132072364403-lsipibere18f4t82snu84iaups8vnurq.apps.googleusercontent.com",
  googleClientSecret: "0NYtj8wJnigyKeUzjMM6W__S",

  //facebook API credentials
  facebookAppId: "563091584100394",
  facebookAppSecret: "6db16b352278412e1ea450e2b983f7a9",

  //MercadoPago - Usuario de Test
  mercadoPagoClientId: "2845558329166249", //App ID de la aplicación
  mercadoPagoClientSecret: "Y889A7Hovpy9ymWBbWlrg5rPF2ufv5fw", //SecretKey de la aplicación
  mercadoPagoAccessToken:
    "APP_USR-82048480738380-032716-a42e815b7d63c4c3b840296dde229a86-420374963", //access token del vendedor
  mercadoPagoRedirectURI: "http://localhost:8084/api/v1/authorize-mercadopago", //redirect_uri que se setea en la aplicacion
  mercadoPagoMarketplaceFee: 2,
  shopWhatsapp: 5491163689674
};
