require("dotenv").config();
// config used by server side only
const dbHost = process.env.DB_HOST || "127.0.0.1";
const dbPort = process.env.DB_PORT || 27017;
const dbName = process.env.DB_NAME || "shop";
const dbUser = process.env.DB_USER || "";
const dbPass = process.env.DB_PASS || "";
const dbCred =
  dbUser.length > 0 || dbPass.length > 0 ? `${dbUser}:${dbPass}@` : "";

const dbUrl =
  process.env.DB_URL || `mongodb://${dbCred}${dbHost}:${dbPort}/${dbName}`;
module.exports = {
  // used by Store (server side)
  apiBaseUrl: process.env.API_BASE_URL || "https://shop.evolve-sdf.com/api/v1",

  // used by Store (server and client side)
  ajaxBaseUrl: process.env.AJAX_BASE_URL,

  // Access-Control-Allow-Origin
  storeBaseUrl: process.env.STORE_BASE_URL,

  // used by API
  adminLoginUrl: process.env.ADMIN_LOGIN_URL,

  apiListenPort: process.env.API_LISTEN_PORT,
  storeListenPort: process.env.STORE_LISTEN_PORT,

  // used by API
  mongodbServerUrl: dbUrl,

  smtpServer: {
    host: process.env.SMTP_HOST,
    port: process.env.SMTP_PORT,
    secure: process.env.SMTP_SECURE == "true",
    user: process.env.SMTP_USER,
    pass: process.env.SMTP_PASS,
    fromName: process.env.SMTP_FROM_NAME,
    fromAddress: process.env.SMTP_FROM_ADDRESS
  },

  // key to sign tokens
  jwtSecretKey: process.env.JWT_SECRET,

  // key to sign store cookies
  cookieSecretKey: process.env.COOKIE_SECRET,

  // path to uploads
  categoriesUploadPath: process.env.CATEGORIES_UPLOAD_PATH,
  productsUploadPath: process.env.PRODUCTS_UPLOAD_PATH,
  filesUploadPath: process.env.FILES_UPLOAD_PATH,
  themeAssetsUploadPath: process.env.THEME_ASSETS_UPLOAD_PATH,

  // url to uploads
  categoriesUploadUrl: process.env.CATEGORIES_UPLOAD_URL,
  productsUploadUrl: process.env.PRODUCTS_UPLOAD_URL,
  filesUploadUrl: process.env.FILES_UPLOAD_URL,
  themeAssetsUploadUrl: process.env.THEME_ASSETS_UPLOAD_URL,

  // store UI language
  language: process.env.SYS_LANGUAGE,

  // used by API
  orderStartNumber: process.env.ORDER_START_NUMBER,
  developerMode: process.env.DEVELOPER_MODE == "true",

  //google API credentials
  googleApiKey: process.env.GOOGLE_API_KEY,
  googleClientId: process.env.GOOGLE_CLIENT_ID,
  googleClientSecret: process.env.GOOGLE_CLIENT_SECRET,

  //facebook API credentials
  facebookAppId: process.env.FACEBOOK_APP_ID,
  facebookAppSecret: process.env.FACEBOOK_APP_SECRET,

  //MercadoPago - Usuario de Test
  mercadoPagoClientId: process.env.MP_CLIENT_ID || "8695355255866635", //App ID de la aplicación
  mercadoPagoClientSecret:
    process.env.MP_CLIENT_SECRET || "fcvqa8ewP9PLrdN1chiXmVuGo61qptO3", //SecretKey de la aplicación
  mercadoPagoRedirectURI:
    process.env.MP_REDIRECT_URI ||
    "https://mintomin.com/api/v1/authorize-mercadopago", //redirect_uri que se setea en la aplicacion
  mercadoPagoMarketplaceFee: process.env.MP_MARKETPLACE_FEE || 2,
  mercadoPagoNotificationUrl:
    process.env.MP_IPN_URL ||
    "https://mintomin.com/api/v1/mercadopago-ipn"
};
