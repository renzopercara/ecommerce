import express from "express";
import helmet from "helmet";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import responseTime from "response-time";
import winston from "winston";
import logger from "./lib/logger";
import settings from "./lib/settings";
import security from "./lib/security";
import { db } from "./lib/mongo";
import dashboardWebSocket from "./lib/dashboardWebSocket";
import ajaxRouter from "./ajaxRouter";
import apiRouter from "./apiRouter";
import { syncProviderShops } from "./services/provider-shops/shops-synchronization";
import MercadoPago from "./paymentGateways/MercadoPago";
import ProductOptions from "./models/product-option";
import ProductOptionsValues from "./models/product-option-value";
import Factors from "./models/factors";

const app = express();
const User = require("./models/users");
require("dotenv").config();

// security.applyMiddleware(app);
app.set("trust proxy", 1);
app.use(helmet());
app.all("*", (req, res, next) => {
  // CORS headers
  res.header(
    "Access-Control-Allow-Origin",
    security.getAccessControlAllowOrigin()
  );
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
  res.header("Access-Control-Allow-Credentials", "true");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Key, Authorization"
  );
  next();
});
app.disable("etag");
app.use(responseTime());
app.use(cookieParser(settings.cookieSecretKey));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use("/ajax", ajaxRouter);

app.use("/api", apiRouter);
app.use(logger.sendResponse);

const server = app.listen(settings.apiListenPort, () => {
  const serverAddress = server.address();
  winston.info(`API running at http://localhost:${serverAddress.port}`);
});

//configuración de passport
const passport = require("passport");
require("./lib/passport");
app.use(passport.initialize());

const addAdminUser = async () => {
  const adminCount = await User.count({ role: "superadmin" });
  const adminNotExist = adminCount === 0;
  if (adminNotExist) {
    const newUser = new User({
      username: process.env.ADMIN_USER,
      password: process.env.ADMIN_PASSWORD,
      role: "superadmin"
    });
    await newUser.save();
    winston.info("- Creado el admin");
  }
};

const addProductOptions = async () => {
  const optionsCount = await ProductOptions.count({
    $or: [{ name: "Color" }, { name: "Talle" }]
  });
  const optionsNotExist = optionsCount < 2;
  if (optionsNotExist) {
    const options = [{ name: "Color" }, { name: "Talle" }];
    const promises = options.map(option => {
      const newOption = new ProductOptions(option);
      return newOption.save();
    });
    return Promise.all(promises).then(() =>
      console.log("ProductOptions created !!")
    );
  }
  return Promise.resolve(null);
};

const optionValues = [
  {
    option: "Color",
    values: [
      "Rojo",
      "Verde",
      "Negro",
      "Blanco",
      "Azul",
      "Rosa",
      "Gris",
      "Beige",
      "Amarillo",
      "Plata",
      "Marrón"
    ]
  },
  {
    option: "Talle",
    values: [
      "XXS",
      "XS",
      "S",
      "M",
      "L",
      "XL",
      "XXL",
      "30",
      "31",
      "32",
      "33",
      "34",
      "35",
      "36",
      "37",
      "38",
      "39",
      "40",
      "41",
      "42",
      ,
      "43",
      "44",
      "45"
    ]
  }
];

const addProductOptionsValues = async () => {
  const promises = optionValues.map(async optionValue => {
    const productOption = await ProductOptions.findOne({
      name: optionValue.option
    });
    const optionValuesCount = await ProductOptionsValues.count({
      option_id: productOption._id
    });
    if (optionValuesCount < optionValue.values.length) {
      const valuesPromises = optionValue.values.map(value => {
        const newValue = new ProductOptionsValues({
          name: value,
          option_id: productOption._id
        });
        return newValue.save();
      });
      return Promise.all(valuesPromises);
    }
    return Promise.resolve(null);
  });
  return Promise.all(promises);
};

const initializeFactors = async () => {
  const factorsCount = await Factors.count();
  if (factorsCount === 0) {
    const newFactor = new Factors({});
    newFactor.save().then(() => console.log("Factores inicializados"));
  }
};

addAdminUser();
initializeFactors();
MercadoPago.configure();

addProductOptions()
  .then(() => addProductOptionsValues())
  .then(() => {
    console.log("Option values added");
  })
  .then(() => syncProviderShops())
  .then(cantProviders =>
    console.log("Sincronización de productos completa. Providers actualizados")
  );

setInterval(function() {
  syncProviderShops()
    .then(cantProviders => {
      console.log(
        "Sincronización de productos completa. Providers actualizados"
      );
    })
    .catch(err => console.error(err));
}, 15 * 60000); // 10 minutos

dashboardWebSocket.listen(server);
