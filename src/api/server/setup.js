import winston from "winston";
import url from "url";
import { MongoClient } from "mongodb";
import logger from "./lib/logger";
import settings from "./lib/settings";

const mongodbConnection = settings.mongodbServerUrl;
const mongoPathName = url.parse(mongodbConnection).pathname;
const dbName = mongoPathName.substring(mongoPathName.lastIndexOf("/") + 1);

const CONNECT_OPTIONS = {
  useNewUrlParser: true
};

const DEFAULT_LANGUAGE = "spanish";

const addPage = async (db, pageObject) => {
  const count = await db
    .collection("pages")
    .countDocuments({ slug: pageObject.slug });
  const docExists = +count > 0;
  if (!docExists) {
    await db.collection("pages").insertOne(pageObject);
    winston.info(`- Added page: /${pageObject.slug}`);
  }
};

const addAllPages = async db => {
  await addPage(db, {
    slug: "",
    meta_title: "Inicio",
    enabled: true,
    is_system: true
  });
  await addPage(db, {
    slug: "checkout",
    meta_title: "Confirmar Compra",
    enabled: true,
    is_system: true
  });
  await addPage(db, {
    slug: "checkout-success",
    meta_title: "Gracias!",
    enabled: true,
    is_system: true
  });
  await addPage(db, {
    slug: "about",
    meta_title: "Quienes somos",
    enabled: true,
    is_system: false
  });
};

const addAllProducts = async db => {
  const productCategoriesCount = await db
    .collection("productCategories")
    .countDocuments({});

  const productsCount = await db.collection("products").countDocuments({});

  const productsNotExists = productCategoriesCount === 0 && productsCount === 0;

  if (productsNotExists) {
    const catA = await db.collection("productCategories").insertOne({
      name: "Categoría A",
      slug: "category-a",
      image: "",
      parent_id: null,
      enabled: true
    });

    const catB = await db.collection("productCategories").insertOne({
      name: "Categoría B",
      slug: "category-b",
      image: "",
      parent_id: null,
      enabled: true
    });

    const catC = await db.collection("productCategories").insertOne({
      name: "Categoría C",
      slug: "category-c",
      image: "",
      parent_id: null,
      enabled: true
    });

    const catA1 = await db.collection("productCategories").insertOne({
      name: "Subcategoría 1",
      slug: "category-a-1",
      image: "",
      parent_id: catA.insertedId,
      enabled: true
    });

    const catA2 = await db.collection("productCategories").insertOne({
      name: "Subcategoría 2",
      slug: "category-a-2",
      image: "",
      parent_id: catA.insertedId,
      enabled: true
    });

    const catA3 = await db.collection("productCategories").insertOne({
      name: "Subcategoría 3",
      slug: "category-a-3",
      image: "",
      parent_id: catA.insertedId,
      enabled: true
    });

    await db.collection("products").insertOne({
      name: "Producto A",
      slug: "product-a",
      category_id: catA.insertedId,
      regular_price: 950,
      stock_quantity: 1,
      enabled: true,
      discontinued: false,
      attributes: [
        { name: "Marca", value: "Marca A" },
        { name: "Talle", value: "M" }
      ]
    });

    await db.collection("products").insertOne({
      name: "Producto B",
      slug: "product-b",
      category_id: catA.insertedId,
      regular_price: 1250,
      stock_quantity: 1,
      enabled: true,
      discontinued: false,
      attributes: [
        { name: "Marca", value: "Marca B" },
        { name: "Talle", value: "L" }
      ]
    });

    winston.info("- Se agregaron los productos");
  }
};

const addEmailTemplates = async db => {
  const emailTemplatesCount = await db
    .collection("emailTemplates")
    .countDocuments({ name: "order_confirmation" });
  const emailTemplatesNotExists = emailTemplatesCount === 0;
  if (emailTemplatesNotExists) {
    await db.collection("emailTemplates").insertOne({
      name: "order_confirmation",
      subject: "Confirmación de pedido",
      body: `<div>
			<div><b>Número de pedido</b>: {{number}}</div>
			<div><b>Método de envío</b>: {{shipping_method}}</div>
			<div><b>Método de pago</b>: {{payment_method}}</div>
      <br>
      <pre>{{payment_description}}</pre>
      <br>
      <br>
			<div style="width: 100%; margin-top: 20px;">
			  Enviando a<br /><br />
			  <b>Nombre completo</b>: {{shipping_address.full_name}}<br />
			  <b>Dirección línea 1</b>: {{shipping_address.address1}}<br />
			  <b>Dirección línea 2</b>: {{shipping_address.address2}}<br />
			  <b>Código postal</b>: {{shipping_address.postal_code}}<br />
			  <b>Ciudad</b>: {{shipping_address.city}}<br />
			  <b>Provincia</b>: {{shipping_address.state}}<br />
			  <b>Teléfono</b>: {{shipping_address.phone}}
			</div>

			<table style="width: 100%; margin-top: 20px;">
			  <tr>
				<td style="width: 40%; padding: 10px 0px; border-top: 1px solid #ccc; border-bottom: 1px solid #ccc; text-align: left;">Artículo</td>
				<td style="width: 25%; padding: 10px 0px; border-top: 1px solid #ccc; border-bottom: 1px solid #ccc; text-align: right;">Precio</td>
				<td style="width: 10%; padding: 10px 0px; border-top: 1px solid #ccc; border-bottom: 1px solid #ccc; text-align: right;">Cant.</td>
				<td style="width: 25%; padding: 10px 0px; border-top: 1px solid #ccc; border-bottom: 1px solid #ccc; text-align: right;">Total</td>
			  </tr>

			  {{#each items}}
			  <tr>
				<td style="padding: 10px 0px; border-bottom: 1px solid #ccc; text-align: left;">{{name}}<br />{{variant_name}}</td>
				<td style="padding: 10px 0px; border-bottom: 1px solid #ccc; text-align: right;">$ {{price}}</td>
				<td style="padding: 10px 0px; border-bottom: 1px solid #ccc; text-align: right;">{{quantity}}</td>
				<td style="padding: 10px 0px; border-bottom: 1px solid #ccc; text-align: right;">$ {{price_total}}</td>
			  </tr>
			  {{/each}}

			</table>

			<table style="width: 100%; margin: 20px 0;">
			  <tr>
				<td style="width: 80%; padding: 10px 0px; text-align: right;"><b>Subtotal</b></td>
				<td style="width: 20%; padding: 10px 0px; text-align: right;">$ {{subtotal}}</td>
			  </tr>
			  <tr>
				<td style="width: 80%; padding: 10px 0px; text-align: right;"><b>Envío</b></td>
				<td style="width: 20%; padding: 10px 0px; text-align: right;">$ {{shipping_total}}</td>
			  </tr>
			  <tr>
				<td style="width: 80%; padding: 10px 0px; text-align: right;"><b>Total</b></td>
				<td style="width: 20%; padding: 10px 0px; text-align: right;">$ {{grand_total}}</td>
			  </tr>
			</table>
      {{#if payment_url}}
        <div style="width: 100%">
          <a
            style="font-size: 16px; padding: 10px; text-transform: uppercase; background-color: green; color: white; width: 100%; text-align: center;"
            href={{payment_url}}
            target="_blank"
          >
            pagar
          </a>
        </div>
      {{/if}}

		  </div>`
    });

    winston.info("- Agregado el template del email de confirmación de pedido");
  }
};

const addShippingMethods = async db => {
  const shippingMethodsCount = await db
    .collection("shippingMethods")
    .countDocuments({});
  const shippingMethodsNotExists = shippingMethodsCount === 0;
  if (shippingMethodsNotExists) {
    await db.collection("shippingMethods").insertOne({
      name: "Método de envío A",
      enabled: true,
      conditions: {
        countries: [],
        states: [],
        cities: [],
        subtotal_min: 0,
        subtotal_max: 0,
        weight_total_min: 0,
        weight_total_max: 0
      }
    });
    winston.info("- Agregado método de envío");
  }
};

const addPaymentMethods = async db => {
  const paymentMethodsCount = await db
    .collection("paymentMethods")
    .countDocuments({ name: "MercadoPago" });
  const paymentMethodsNotExists = paymentMethodsCount === 0;
  if (paymentMethodsNotExists) {
    await db.collection("paymentMethods").insertOne({
      name: "MercadoPago",
      enabled: true,
      conditions: {
        countries: [],
        shipping_method_ids: [],
        subtotal_min: 0,
        subtotal_max: 0
      }
    });
    winston.info("- Agregado método de pago");
  }
};

const addProviders = async db => {
  const providersCount = await db.collection("providers").countDocuments({});
  const providersNotExist = providersCount === 0;
  if (providersNotExist) {
    await db.collection("providers").insertOne({
      access_token: "3b55b574853d3583cdf9a62df5eb73549267c11b",
      user_agent: "Marca de prueba (soporte@modapormayor.com.ar)",
      shop_id: 500275,
      name: "Shop Evolve de prueba",
      code: "MPM",
      shop_source: "tiendanube",
      synchronize: true
    });
    winston.info("- Agregado Provider de prueba");
  }
};

const createIndex = (db, collectionName, fields, options) =>
  db.collection(collectionName).createIndex(fields, options);

const createAllIndexes = async db => {
  const pagesIndexes = await db
    .collection("pages")
    .listIndexes()
    .toArray();

  if (pagesIndexes.length === 1) {
    await createIndex(db, "pages", { enabled: 1 });
    await createIndex(db, "pages", { slug: 1 });
    winston.info("- Creados los índices de las páginas");
  }

  const productCategoriesIndexes = await db
    .collection("productCategories")
    .listIndexes()
    .toArray();

  if (productCategoriesIndexes.length === 1) {
    await createIndex(db, "productCategories", { enabled: 1 });
    await createIndex(db, "productCategories", { slug: 1 });
    winston.info("- Creados los índices para: productCategories");
  }

  const productsIndexes = await db
    .collection("products")
    .listIndexes()
    .toArray();

  if (productsIndexes.length === 1) {
    await createIndex(db, "products", { slug: 1 });
    await createIndex(db, "products", { enabled: 1 });
    await createIndex(db, "products", { category_id: 1 });
    await createIndex(db, "products", { sku: 1 });
    await createIndex(db, "products", {
      "attributes.name": 1,
      "attributes.value": 1
    });
    await createIndex(
      db,
      "products",
      {
        name: "text",
        description: "text"
      },
      { default_language: DEFAULT_LANGUAGE, name: "textIndex" }
    );
    winston.info("- Creados los índices para: products");
  }

  const customersIndexes = await db
    .collection("customers")
    .listIndexes()
    .toArray();

  if (customersIndexes.length === 1) {
    await createIndex(db, "customers", { group_id: 1 });
    await createIndex(db, "customers", { email: 1 });
    await createIndex(db, "customers", { mobile: 1 });
    await createIndex(
      db,
      "customers",
      {
        full_name: "text",
        "addresses.address1": "text"
      },
      { default_language: DEFAULT_LANGUAGE, name: "textIndex" }
    );
    winston.info("- Created indexes for: customers");
  }

  const ordersIndexes = await db
    .collection("orders")
    .listIndexes()
    .toArray();

  if (ordersIndexes.length === 1) {
    await createIndex(db, "orders", { draft: 1 });
    await createIndex(db, "orders", { number: 1 });
    await createIndex(db, "orders", { customer_id: 1 });
    await createIndex(db, "orders", { email: 1 });
    await createIndex(db, "orders", { mobile: 1 });
    await createIndex(
      db,
      "orders",
      {
        "shipping_address.full_name": "text",
        "shipping_address.address1": "text"
      },
      { default_language: DEFAULT_LANGUAGE, name: "textIndex" }
    );
    winston.info("- Created indexes for: orders");
  }
};

const addOrderStatuses = async db => {
  const orderStatuses = await db.collection("orderStatuses").countDocuments();
  const orderStatusesExist = orderStatuses > 0;
  if (!orderStatusesExist) {
    await db.collection("orderStatuses").insertMany([
      {
        ref: "payment_success",
        name: "pagado",
        description: "Se confirmó el pago del pedido",
        color: "#00ff00"
      },
      {
        ref: "payment_pending",
        name: "pendiente de pago",
        description: "Se ordenó el pedido pero falta que se confirme el pago",
        color: "#EEEE00"
      },
      {
        ref: "payment_failed",
        name: "pago fallido",
        description: "Falló el pago del pedido",
        color: "#ff0000"
      }
    ]);
    winston.info("- Created order statuses");
  }
};

const addSettings = async (db, { domain }) => {
  if (domain && (domain.includes("https://") || domain.includes("http://"))) {
    await db.collection("settings").updateOne(
      {},
      {
        $set: {
          domain
        }
      },
      { upsert: true }
    );
    winston.info(`- Setteado dominio: ${domain}`);
  }
};

(async () => {
  let client = null;
  let db = null;

  try {
    client = await MongoClient.connect(mongodbConnection, CONNECT_OPTIONS);
    db = client.db(dbName);
    winston.info(`Connectado correctamente a ${mongodbConnection}`);
  } catch (e) {
    winston.error(`La conexión a MongoDB falló. ${e.message}`);
    return;
  }

  const userEmail = process.argv.length > 2 ? process.argv[2] : null;
  const domain = process.argv.length > 3 ? process.argv[3] : null;

  await db.createCollection("customers");
  await db.createCollection("orders");
  await addAllPages(db);
  // await addAllProducts(db);
  await addEmailTemplates(db);
  await addShippingMethods(db);
  await addPaymentMethods(db);
  await addProviders(db);
  await createAllIndexes(db);
  await addSettings(db, {
    domain
  });
  await addOrderStatuses(db);

  client.close();
})();
