import security from "../lib/security";
import CredentialsService from "../services/credentials/credentials";
import MercadoPago from "../paymentGateways/MercadoPago";
import { storeBaseUrl } from "../../../../config/server";

class CredentialsRoute {
  constructor(router) {
    this.router = router;
    this.registerRoutes();
  }

  registerRoutes() {
    this.router.get("/v1/credentials", this.getCredentials.bind(this));
    this.router.get(
      "/v1/authorize-mercadopago",
      this.authorizeMercadoPago.bind(this)
    );
    this.router.delete("/v1/credentials/:id", this.deleteCredential.bind(this));
  }

  getCredentials(req, res, next) {
    CredentialsService.getCredentials(req.query)
      .then(data => {
        res.send(data);
      })
      .catch(next);
  }

  authorizeMercadoPago(req, res, next) {
    const authorizationCode = req.query.code;
    return MercadoPago.authorizeSeller(authorizationCode)
      .then(authorization => {
        return res.redirect(storeBaseUrl + "/admin/settings/payments");
      })
      .catch(err => {
        console.error(err);
        return res.redirect(storeBaseUrl + "admin/settings/payments");
      });
  }

  deleteCredential(req, res, next) {
    CredentialsService.deleteCredential(req.params.id)
      .then(data => {
        res.status(data ? 200 : 404).json(data);
      })
      .catch(next);
  }
}

export default CredentialsRoute;
