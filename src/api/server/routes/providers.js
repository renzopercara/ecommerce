import security from "../lib/security";
import ProvidersService from "../services/providers/providers";

class ProviderRoute {
  constructor(router) {
    this.router = router;
    this.registerRoutes();
  }

  registerRoutes() {
    this.router.get(
      "/v1/providers",
      security.checkUserScope.bind(this, security.scope.READ_PROVIDERS),
      this.getProviders.bind(this)
    );
    this.router.post(
      "/v1/providers",
      security.checkUserScope.bind(this, security.scope.WRITE_PROVIDERS),
      this.addProvider.bind(this)
    );
    this.router.get(
      "/v1/providers/:providerId",
      security.checkUserScope.bind(this, security.scope.READ_PROVIDERS),
      this.getSingleProvider.bind(this)
    );
    this.router.put(
      "/v1/providers/:providerId",
      security.checkUserScope.bind(this, security.scope.WRITE_PROVIDERS),
      this.updateProvider.bind(this)
    );
  }

  getProviders(req, res, next) {
    ProvidersService.getAllProviders()
      .then(data => {
        res.send(data);
      })
      .catch(next);
  }

  addProvider(req, res, next) {
    ProvidersService.createProvider(req.body)
      .then(data => {
        res.send(data);
      })
      .catch(next);
  }

  getSingleProvider(req, res, next) {
    ProvidersService.getProviderById(req.params.providerId)
      .then(data => {
        if (data) {
          res.send(data);
        } else {
          res.status(404).end();
        }
      })
      .catch(next);
  }

  updateProvider(req, res, next) {
    ProvidersService.updateProvider(req.params.providerId, req.body)
      .then(data => {
        if (data) {
          res.send(data);
        } else {
          res.status(404).end();
        }
      })
      .catch(next);
  }
}

export default ProviderRoute;
