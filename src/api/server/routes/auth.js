import security from "../lib/security";
import settings from "../../../../config/server";
import passport from "passport";
import User from "../models/users";
import Provider from "../models/providers";
import jwt from "jsonwebtoken";

const FRONT_URL = process.env.STORE_BASE_URL;
const JWT_SECRET = settings.jwtSecretKey;

class UsersRoute {
  constructor(router) {
    this.router = router;
    this.registerRoutes();
  }

  registerRoutes() {
    //google login route
    this.router.get(
      "/v1/auth/google",
      passport.authenticate("google", {
        scope: ["https://www.googleapis.com/auth/plus.login"],
        session: false
      })
    );

    //google callback route
    this.router.get(
      "/v1/auth/google/callback",
      passport.authenticate("google", {
        failureRedirect: "/login",
        session: false
      }),
      function(req, res) {
        if (req.user && req.user.token) {
          return res.redirect(FRONT_URL + "/admin/oauth/" + req.user.token);
        }
        console.error("No se generó el token");
        return res.redirect("/");
      }
    );

    //facebook login route
    this.router.get(
      "/v1/auth/facebook",
      passport.authenticate("facebook", {
        session: false
      })
    );

    //facebook callback route
    this.router.get(
      "/v1/auth/facebook/callback",
      passport.authenticate("google", {
        failureRedirect: FRONT_URL + "/notfound",
        session: false
      }),
      function(req, res) {
        if (req.user && req.user.token) {
          return res.redirect(FRONT_URL + "/admin/oauth/" + req.user.token);
        }
        return res.redirect(FRONT_URL);
      }
    );

    this.router.post("/v1/auth/login", function(req, res) {
      return User.findOne({ username: req.body.username })
        .then(user => {
          if (!user || !user.validPassword(req.body.password)) {
            return res
              .status(401)
              .json({ error: "Usuario o contraseña incorrecto!" });
          }
          if (user.role === "provider") {
            return Provider.findOne({ provider_user_id: user._id }).then(
              provider => {
                const token = jwt.sign(
                  {
                    _id: user._id,
                    providerId: provider ? provider._id : "",
                    role: user.role,
                    username: user.username
                  },
                  JWT_SECRET
                );
                return res.status(200).json({ token: token });
              }
            );
          }
          const token = jwt.sign(
            {
              _id: user._id,
              role: user.role,
              username: user.username
            },
            JWT_SECRET
          );
          return res.status(200).json({ token: token });
        })
        .catch(error => {
          console.error(error);
          return res
            .status(500)
            .json({ error: "Ocurrió un error de servidor!" });
        });
    });
  }
}

export default UsersRoute;
