import security from "../lib/security";
import WebhooksService from "../services/webhooks";
import OrderStatusesService from "../services/orders/orderStatuses";
import OrdersService from "../services/orders/orders";
import Ipn from "../models/ipn";
import { storeBaseUrl } from "../../../../config/server";
import mercadopago from "mercadopago";

class WebhooksRoute {
  constructor(router) {
    this.router = router;
    this.registerRoutes();
  }

  registerRoutes() {
    this.router.get(
      "/v1/webhooks",
      security.checkUserScope.bind(this, security.scope.READ_SETTINGS),
      this.getWebhooks.bind(this)
    );
    this.router.post(
      "/v1/webhooks",
      security.checkUserScope.bind(this, security.scope.WRITE_SETTINGS),
      this.addWebhook.bind(this)
    );
    this.router.get(
      "/v1/mercadopago-ipn",
      this.processPaymentCallback.bind(this)
    );
    this.router.post(
      "/v1/mercadopago-ipn",
      this.processPaymentCallback.bind(this)
    );
    this.router.get(
      "/v1/webhooks/:id",
      security.checkUserScope.bind(this, security.scope.READ_SETTINGS),
      this.getSingleWebhook.bind(this)
    );
    this.router.put(
      "/v1/webhooks/:id",
      security.checkUserScope.bind(this, security.scope.WRITE_SETTINGS),
      this.updateWebhook.bind(this)
    );
    this.router.delete(
      "/v1/webhooks/:id",
      security.checkUserScope.bind(this, security.scope.WRITE_SETTINGS),
      this.deleteWebhook.bind(this)
    );
  }

  getWebhooks(req, res, next) {
    WebhooksService.getWebhooks(req.query)
      .then(data => {
        res.send(data);
      })
      .catch(next);
  }

  getSingleWebhook(req, res, next) {
    WebhooksService.getSingleWebhook(req.params.id)
      .then(data => {
        if (data) {
          res.send(data);
        } else {
          res.status(404).end();
        }
      })
      .catch(next);
  }

  updateOrderStatus(mercadoPagoResponse) {
    let status = "";
    if (mercadoPagoResponse.response.status === "pending") {
      status = "payment_pending";
    } else if (mercadoPagoResponse.response.status === "approved") {
      status = "payment_success";
    } else {
      status = "payment_failed";
    }
    return OrderStatusesService.getStatuses({ ref: status }).then(
      orderStatuses => {
        if (orderStatuses.length > 0) {
          let orderStatus = orderStatuses[0];
          return OrdersService.updateOrder(
            mercadoPagoResponse.response.external_reference,
            { status_id: orderStatus.id }
          );
        }
        return Promise.resolve(null);
      }
    );
  }

  processPaymentCallback(req, res, next) {
    if (
      req.body.data &&
      req.body.data.id &&
      req.body.type === "payment" &&
      req.body.action == "payment.created"
    ) {
      return mercadopago.payment
        .get(req.body.data.id)
        .then(response => {
          if (response) {
            const newIpn = new Ipn({
              notification_id: req.body.id,
              payment_id: req.body.data.id,
              payment_status: response.response.status,
              payment_status_detail: response.response.status_detail,
              type: req.body.type,
              external_reference: response.response.external_reference,
              total_paid_amount:
                response.response.transaction_details.total_paid_amount,
              action: req.body.action,
              user_id: req.body.user_id
            });

            return newIpn
              .save()
              .then(() => this.updateOrderStatus(response))
              .then(() =>
                res.status(200).json({ msg: "Order status updated" })
              );
          }
          return res
            .status(400)
            .json({ msg: "Error processing notification ipn" });
        })
        .catch(err => {
          console.error(err);
          return res.status(500).json({ msg: "Order status failed to update" });
        });
    }
    return res.status(200).json({ msg: "Unrelevant notification" });
  }

  //para testear
  processPaymentCallbackTest(req, res, next) {
    console.log("METODO - " + req.method);

    const body = {
      action: "payment.created",
      api_version: "v1",
      data: { id: "4705201760" },
      id: 5182712456,
      live_mode: true,
      type: "payment",
      user_id: "429718840"
    };
    if (
      body.data &&
      body.data.id &&
      body.type === "payment" &&
      body.action == "payment.created"
    ) {
      return mercadopago.payment
        .get(body.data.id)
        .then(response => {
          if (response) {
            const newIpn = new Ipn({
              notification_id: body.id,
              payment_id: body.data.id,
              payment_status: response.response.status,
              payment_status_detail: response.response.status_detail,
              type: body.type,
              external_reference: response.response.external_reference,
              total_paid_amount:
                response.response.transaction_details.total_paid_amount,
              action: body.action,
              user_id: body.user_id
            });

            return (
              newIpn
                .save()
                // .then(() => this.updateOrderStatus(response))
                .then(() => res.status(200).json(response))
            );
          }
          return res
            .status(400)
            .json({ msg: "Error processing notification ipn" });
        })
        .catch(err => {
          console.error(err);
          return res.status(500).json({ msg: "Order status failed to update" });
        });
    }
    return res.status(200).json({ msg: "Unrelevant notification" });
  }

  addWebhook(req, res, next) {
    WebhooksService.addWebhook(req.body)
      .then(data => {
        res.send(data);
      })
      .catch(next);
  }

  updateWebhook(req, res, next) {
    WebhooksService.updateWebhook(req.params.id, req.body)
      .then(data => {
        if (data) {
          res.send(data);
        } else {
          res.status(404).end();
        }
      })
      .catch(next);
  }

  deleteWebhook(req, res, next) {
    WebhooksService.deleteWebhook(req.params.id)
      .then(data => {
        res.status(data ? 200 : 404).end();
      })
      .catch(next);
  }
}

export default WebhooksRoute;
