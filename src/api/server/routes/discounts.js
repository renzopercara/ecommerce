import security from "../lib/security";
import DiscountsService from "../services/discounts/discounts";

class DiscountRoute {
  constructor(router) {
    this.router = router;
    this.registerRoutes();
  }

  registerRoutes() {
    this.router.get(
      "/v1/discounts",
      security.checkUserScope.bind(this, security.scope.READ_DISCOUNTS),
      this.getDiscounts.bind(this)
    );
    this.router.post(
      "/v1/discounts",
      security.checkUserScope.bind(this, security.scope.WRITE_DISCOUNTS),
      this.addDiscount.bind(this)
    );
    this.router.get(
      "/v1/discounts/:discountId",
      security.checkUserScope.bind(this, security.scope.READ_DISCOUNTS),
      this.getSingleDiscount.bind(this)
    );
    this.router.get(
      "/v1/productdiscount/:productId",
      security.checkUserScope.bind(this, security.scope.READ_DISCOUNTS),
      this.getSingleDiscountByProductId.bind(this)
    );
    this.router.put(
      "/v1/discounts/:discountId",
      security.checkUserScope.bind(this, security.scope.WRITE_DISCOUNTS),
      this.updateDiscount.bind(this)
    );
  }

  getDiscounts(req, res, next) {
    DiscountsService.getAllDiscounts()
      .then(data => {
        res.send(data);
      })
      .catch(next);
  }

  addDiscount(req, res, next) {
    DiscountsService.createDiscount(req.body)
      .then(data => {
        res.send(data);
      })
      .catch(next);
  }

  getSingleDiscount(req, res, next) {
    DiscountsService.getDiscountById(req.params.discountId)
      .then(data => {
        if (data) {
          res.send(data);
        } else {
          res.status(404).end();
        }
      })
      .catch(next);
  }

  getSingleDiscountByProductId(req, res, next) {
    DiscountsService.getDiscountByProductId(req.params.productId)
      .then(data => {
        if (data) {
          res.send(data);
        } else {
          res.status(404).end();
        }
      })
      .catch(next);
  }

  updateDiscount(req, res, next) {
    DiscountsService.updateDiscount(req.params.discountId, req.body)
      .then(data => {
        if (data) {
          res.send(data);
        } else {
          res.status(404).end();
        }
      })
      .catch(next);
  }
}

export default DiscountRoute;
