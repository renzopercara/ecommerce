import Factors from "../models/factors";

class FactorRoute {
  constructor(router) {
    this.router = router;
    this.registerRoutes();
  }

  registerRoutes() {
    this.router.get("/v1/factors", this.getFactors.bind(this));
    this.router.put("/v1/factors", this.updateFactor.bind(this));
  }

  getFactors(req, res, next) {
    Factors.findOne()
      .then(data => {
        res.send(data);
      })
      .catch(next);
  }

  updateFactor(req, res, next) {
    Factors.findOne()
      .then(factors => Factors.update({ _id: factors._id }, { $set: req.body }))
      .then(() => Factors.findOne())
      .then(data => {
        if (data) {
          res.send(data);
        } else {
          res.status(404).end();
        }
      })
      .catch(next);
  }
}

export default FactorRoute;
