"use strict";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var factorsSchema = new Schema(
  {
    minorist_message: {
      type: String,
      default: ""
    },
    mayorist_message: {
      type: String,
      default: ""
    },
    reseller_message: {
      type: String,
      default: ""
    },
    minorist_factor: {
      type: Number,
      default: 1
    },
    mayorist_factor: {
      type: Number,
      default: 1
    },
    reseller_factor: {
      type: Number,
      default: 1
    },
    mayorist_quantity: {
      type: Number,
      default: 1
    },
    reseller_quantity: {
      type: Number,
      default: 1
    }
  },
  {
    timestamps: true
  }
);

factorsSchema.methods.toJSON = function() {
  let obj = this.toObject();
  return obj;
};

module.exports = mongoose.model("Factor", factorsSchema);
