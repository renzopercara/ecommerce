"use strict";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var discountSchema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    active: {
      type: Boolean,
      default: false
    },
    general_percentage: {
      type: Number
    },
    category_id: {
      type: String,
      default: ""
    },
    product_id: {
      type: String,
      default: ""
    },
    provider_id: {
      type: String,
      default: ""
    },
    option_values_ids: [
      {
        type: String
      }
    ]
    // secondary_type: {
    //   type: String,
    //   enum: ["mount", "quantity"]
    // },
    // // quantity rules
    // minorist_quantity_percentage: {
    //   type: Number
    // },
    // minorist_quantity: {
    //   type: Number
    // },
    // mayorist_quantity_percentage: {
    //   type: Number
    // },
    // mayorist_quantity: {
    //   type: Number
    // },
    // //amount rules
    // mount_percentage: {
    //   type: Number
    // },
    // minimum_mount: {
    //   type: Number
    // }
  },
  {
    timestamps: true
  }
);

discountSchema.methods.toJSON = function() {
  let obj = this.toObject();
  return obj;
};

module.exports = mongoose.model("Discount", discountSchema);
