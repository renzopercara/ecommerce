"use strict";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var productOptionValueSchema = new Schema(
  {
    name: {
      type: String,
      unique: true,
      required: true
    },
    option_id: {
      type: Schema.Types.ObjectId,
      ref: "ProductOption"
    }
  },
  {
    timestamps: true
  }
);

productOptionValueSchema.methods.toJSON = function() {
  let obj = this.toObject();
  return obj;
};

module.exports = mongoose.model("ProductOptionValue", productOptionValueSchema);
