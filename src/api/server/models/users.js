"use strict";
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var bcrypt = require("bcrypt-nodejs");

var userSchema = new Schema(
  {
    role: {
      type: String,
      enum: ["superadmin", "provider", "client"],
      default: "client",
      required: true
    },
    username: {
      type: String
    },
    password: {
      type: String
    },
    oauth_source: {
      type: String,
      enum: ["google", "facebook"]
    },
    oauth_id: {
      type: String
    }
  },
  { timestamps: true }
);

userSchema.methods.toJSON = function() {
  let obj = this.toObject();
  return obj;
};

userSchema.methods.validPassword = function(password) {
  const valid = bcrypt.compareSync(password, this.password);
  return valid;
};

userSchema.pre("save", function(next) {
  let user = this;
  if (user.isModified("password")) {
    console.log("entro aca");
    user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(8), null);
  }
  return next();
});

userSchema.pre("updateOne", function() {
  console.log(this._update.$set.password);
  if (this._update.$set.password) {
    this._update.$set.password = bcrypt.hashSync(
      this._update.$set.password,
      bcrypt.genSaltSync(8),
      null
    );
  }
});

userSchema.pre("updateOne", function() {
  console.log(this._update.$set.password);
  if (this._update.$set.password) {
    this._update.$set.password = bcrypt.hashSync(
      this._update.$set.password,
      bcrypt.genSaltSync(8),
      null
    );
  }
});

userSchema.pre("update", function() {
  console.log(this._update.$set.password);
  if (this._update.$set.password) {
    this._update.$set.password = bcrypt.hashSync(
      this._update.$set.password,
      bcrypt.genSaltSync(8),
      null
    );
  }
});

module.exports = mongoose.model("User", userSchema);
