"use strict";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var ipnSchema = new Schema(
  {
    notification_id: {
      type: Number,
      required: true
    },
    payment_id: {
      type: String
    },
    type: {
      type: String
    },
    action: {
      type: String
    },
    user_id: {
      type: String
    },
    external_reference: {
      //id de nuestra order
      type: String
    },
    payment_status: {
      type: String
    },
    payment_status_detail: {
      type: String
    },
    total_paid_amount: {
      type: Number
    }
  },
  {
    timestamps: true
  }
);

ipnSchema.methods.toJSON = function() {
  let obj = this.toObject();
  return obj;
};

module.exports = mongoose.model("Ipn", ipnSchema);
