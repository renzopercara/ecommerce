"use strict";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const User = require("./users");

var providerSchema = new Schema(
  {
    shop_source: {
      type: String,
      enum: ["tiendanube", "shopify", "woocommerce", "manual"],
      required: true
    },
    code: {
      type: String
    },
    name: {
      type: String
    },
    synchronize: {
      type: Boolean,
      default: false
    },
    status: {
      type: String,
      enum: ["missing_credentials", "error", "active", "inactive"],
      default: "inactive"
    },
    provider_user_id: {
      type: Schema.Types.ObjectId,
      ref: "User"
    },
    //tiendanube
    user_agent: {
      type: String
    },
    access_token: {
      type: String
    },
    authorization_code: {
      type: String
    },
    shop_id: {
      type: Number
    },
    //shopify
    shopify_access_token: {
      type: String
    },
    shopify_shop_name: {
      type: String
    },
    //woocommerce
    woocommerce_url: {
      type: String
    },
    woocommerce_consumer_key: {
      type: String
    },
    woocommerce_consumer_secret: {
      type: String
    }
  },
  {
    timestamps: true
  }
);

function parseProviderName(name) {
  return name.toLowerCase().replace(/ /g, "");
}

providerSchema.pre("save", function(next) {
  let provider = this;
  if (provider.isNew) {
    const newUser = new User({
      username: parseProviderName(provider.name),
      password: provider.code,
      role: "provider"
    });
    return newUser.save().then(() => {
      this.provider_user_id = newUser._id;
      return next();
    });
  }
  return next();
});

providerSchema.post("update", function() {
  const modifiedFields = this.getUpdate().$set;
  Provider.findOne({ name: modifiedFields.name }).then(provider => {
    console.log(provider);
    User.updateOne(
      { _id: provider._id },
      { $set: { password: modifiedFields.code } }
    );
  });
});

providerSchema.methods.toJSON = function() {
  let obj = this.toObject();
  return obj;
};

const Provider = mongoose.model("Provider", providerSchema);

module.exports = Provider;
