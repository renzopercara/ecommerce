"use strict";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var productOptionSchema = new Schema(
  {
    name: {
      type: String,
      unique: true,
      required: true
    },
    control: {
      type: String,
      enum: ["select"],
      default: "select"
    }
  },
  {
    timestamps: true
  }
);

productOptionSchema.methods.toJSON = function() {
  let obj = this.toObject();
  return obj;
};

module.exports = mongoose.model("ProductOption", productOptionSchema);
