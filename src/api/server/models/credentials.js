"use strict";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var credentialSchema = new Schema(
  {
    source: {
      type: String,
      required: true,
      enum: ["mercadopago"],
      default: "mercadopago"
    },
    authorization_code: {
      type: String
    },
    access_token: {
      type: String
    },
    refresh_token: {
      type: String
    },
    mercadopago_user_id: {
      type: String
    },
    token_type: {
      type: String
    },
    expires_in: {
      type: Number
    },
    scope: {
      type: String
    },
    public_key: {
      type: String
    }
  },
  {
    timestamps: true
  }
);

credentialSchema.methods.toJSON = function() {
  let obj = this.toObject();
  return obj;
};

module.exports = mongoose.model("Credential", credentialSchema);
