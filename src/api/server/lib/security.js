import jwt from "jsonwebtoken";
import expressJwt from "express-jwt";
import settings from "./settings";
import SecurityTokensService from "../services/security/tokens";

const DEVELOPER_MODE = settings.developerMode === true;
const SET_TOKEN_AS_REVOKEN_ON_EXCEPTION = true;

const PATHS_WITH_OPEN_ACCESS = [
  "/api/v1/authorize",
  /\/api\/v1\/notifications/i,
  /\/ajax\//i
];

const scope = {
  DASHBOARD: "dashboard",
  READ_PRODUCTS: "read:products",
  WRITE_PRODUCTS: "write:products",
  READ_PRODUCT_CATEGORIES: "read:product_categories",
  WRITE_PRODUCT_CATEGORIES: "write:product_categories",
  READ_ORDERS: "read:orders",
  WRITE_ORDERS: "write:orders",
  READ_CUSTOMERS: "read:customers",
  WRITE_CUSTOMERS: "write:customers",
  READ_CUSTOMER_GROUPS: "read:customer_groups",
  WRITE_CUSTOMER_GROUPS: "write:customer_groups",
  READ_PAGES: "read:pages",
  WRITE_PAGES: "write:pages",
  READ_ORDER_STATUSES: "read:order_statuses",
  WRITE_ORDER_STATUSES: "write:order_statuses",
  READ_THEME: "read:theme",
  WRITE_THEME: "write:theme",
  READ_SITEMAP: "read:sitemap",
  READ_SHIPPING_METHODS: "read:shipping_methods",
  WRITE_SHIPPING_METHODS: "write:shipping_methods",
  READ_PAYMENT_METHODS: "read:payment_methods",
  WRITE_PAYMENT_METHODS: "write:payment_methods",
  READ_SETTINGS: "read:settings",
  WRITE_SETTINGS: "write:settings",
  READ_FILES: "read:files",
  WRITE_FILES: "write:files",
  READ_PROVIDERS: "read:providers",
  WRITE_PROVIDERS: "write:providers"
};

const rolePermissions = {
  superadmin: [
    scope.DASHBOARD,
    scope.READ_PRODUCTS,
    scope.WRITE_PRODUCTS,
    scope.READ_PRODUCT_CATEGORIES,
    scope.WRITE_PRODUCT_CATEGORIES,
    scope.READ_ORDERS,
    scope.WRITE_ORDERS,
    scope.READ_CUSTOMERS,
    scope.WRITE_CUSTOMERS,
    scope.READ_CUSTOMER_GROUPS,
    scope.WRITE_CUSTOMER_GROUPS,
    scope.READ_PAGES,
    scope.WRITE_PAGES,
    scope.READ_ORDER_STATUSES,
    scope.WRITE_ORDER_STATUSES,
    scope.READ_THEME,
    scope.WRITE_THEME,
    scope.READ_SITEMAP,
    scope.READ_SHIPPING_METHODS,
    scope.WRITE_SHIPPING_METHODS,
    scope.READ_PAYMENT_METHODS,
    scope.WRITE_PAYMENT_METHODS,
    scope.READ_SETTINGS,
    scope.WRITE_SETTINGS,
    scope.READ_FILES,
    scope.WRITE_FILES,
    scope.READ_PROVIDERS,
    scope.WRITE_PROVIDERS
  ],
  provider: [
    scope.DASHBOARD,
    scope.READ_PRODUCTS,
    scope.READ_PRODUCT_CATEGORIES,
    scope.READ_ORDERS
  ]
};

const checkUserScope = (requiredScope, req, res, next) => {
  // return extractJwt(req, res, next).then(() => {
  //   if (!req.user) {
  //     return next();
  //   }
  //   if (
  //     req.user &&
  //     req.user.role &&
  //     (req.user.role === "superadmin" ||
  //       (rolePermissions[req.user.role] &&
  //         rolePermissions[req.user.role].includes(requiredScope)))
  //   ) {
  //     return next();
  //   } else {
  //     return res.status(403).send({ error: true, message: "Forbidden" });
  //   }
  // });
  return next();
};

const verifyToken = token => {
  return new Promise((resolve, reject) => {
    jwt.verify(token, settings.jwtSecretKey, (err, decoded) => {
      if (err) {
        reject(err);
      } else {
        // check on blacklist
        resolve(decoded);
      }
    });
  });
};

const checkTokenInBlacklistCallback = async (req, payload, done) => {
  try {
    const jti = payload.jti;
    const blacklist = await SecurityTokensService.getTokensBlacklist();
    const tokenIsRevoked = blacklist.includes(jti);
    return done(null, tokenIsRevoked);
  } catch (e) {
    done(e, SET_TOKEN_AS_REVOKEN_ON_EXCEPTION);
  }
};

const applyMiddleware = app => {
  app.use(
    expressJwt({
      secret: settings.jwtSecretKey,
      isRevoked: checkTokenInBlacklistCallback
    }).unless({ path: PATHS_WITH_OPEN_ACCESS })
  );
};

const getAccessControlAllowOrigin = () => {
  return settings.storeBaseUrl || "*";
};

const extractJwt = (req, res, next) => {
  if (req.query && req.query.token) {
    return verifyToken(req.query.token).then(user => {
      if (user) {
        req.user = user;
      }
      return Promise.resolve(null);
    });
    return Promise.resolve(null);
  }
  return Promise.resolve(null);
};

export default {
  extractJwt,
  checkUserScope: checkUserScope,
  scope: scope,
  verifyToken: verifyToken,
  applyMiddleware: applyMiddleware,
  getAccessControlAllowOrigin: getAccessControlAllowOrigin,
  DEVELOPER_MODE: DEVELOPER_MODE
};
