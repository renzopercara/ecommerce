"use strict";

const passport = require("passport");
const GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;
const FacebookStrategy = require("passport-facebook").Strategy;

const jwt = require("jsonwebtoken");

const User = require("../models/users");

const settings = require("../../../../config/server");

const GOOGLE_CLIENT_ID = settings.googleClientId;
const GOOGLE_CLIENT_SECRET = settings.googleClientSecret;
const GOOGLE_CALLBACK_URL = settings.apiBaseUrl + "/auth/google/callback";

const FACEBOOK_APP_ID = settings.facebookAppId;
const FACEBOOK_APP_SECRET = settings.facebookAppSecret;
const FACEBOOK_CALLBACK_URL = settings.apiBaseUrl + "/auth/facebook/callback";

const JWT_SECRET = settings.jwtSecretKey;

// Use the GoogleStrategy within Passport.
//   Strategies in Passport require a `verify` function, which accept
//   credentials (in this case, an accessToken, refreshToken, and Google
//   profile), and invoke a callback with a user object.
passport.use(
  new GoogleStrategy(
    {
      clientID: GOOGLE_CLIENT_ID,
      clientSecret: GOOGLE_CLIENT_SECRET,
      callbackURL: GOOGLE_CALLBACK_URL
    },
    function(accessToken, refreshToken, profile, done) {
      return User.findOne({
        oauth_id: profile.id,
        oauth_source: "google"
      }).then(user => {
        if (user) {
          let loggedUser = {
            _id: user._id,
            username: user.username,
            role: user.role
          };
          loggedUser.token = jwt.sign(
            {
              _id: user._id,
              username: user.username,
              role: user.role
            },
            JWT_SECRET
          );
          return done(null, loggedUser);
        }
        const newUser = new User({
          oauth_id: profile.id,
          username: profile.displayName,
          oauth_source: "google",
          role: "client"
        });

        return newUser.save().then(() => {
          let loggedUser = {
            _id: newUser._id,
            nickname: newUser.nickname,
            avatar: newUser.avatar,
            role: newUser.role
          };
          loggedUser.token = jwt.sign(
            {
              _id: newUser._id,
              nickname: newUser.nickname,
              avatar: newUser.avatar,
              role: newUser.role
            },
            JWT_SECRET
          );
          return done(null, loggedUser);
        });
      });
    }
  )
);

//facebook

passport.use(
  new FacebookStrategy(
    {
      clientID: FACEBOOK_APP_ID,
      clientSecret: FACEBOOK_APP_SECRET,
      callbackURL: FACEBOOK_CALLBACK_URL
    },
    function(accessToken, refreshToken, profile, done) {
      return User.findOne({
        oauth_id: profile.id,
        oauth_source: "facebook"
      }).then(user => {
        if (user) {
          user.token = jwt.sign(
            {
              _id: user._id,
              username: user.username,
              role: user.role
            },
            JWT_SECRET
          );
          return done(null, user);
        }
        const newUser = new User({
          oauth_id: profile.id,
          username: profile.displayName,
          oauth_source: "facebook",
          role: "client"
        });

        return newUser.save().then(() => {
          newUser.token = jwt.sign(
            {
              _id: newUser._id,
              nickname: newUser.nickname,
              avatar: newUser.avatar,
              role: newUser.role
            },
            JWT_SECRET
          );
          return done(null, newUser);
        });
      });
    }
  )
);
