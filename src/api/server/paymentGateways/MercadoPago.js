"use strict";
const mercadopago = require("mercadopago");
const axios = require("axios");
const qs = require("qs");
const Credentials = require("../models/credentials");

import CredentialsService from "../services/credentials/credentials";
import {
  mercadoPagoMarketplaceFee,
  mercadoPagoNotificationUrl,
  mercadoPagoClientId,
  mercadoPagoClientSecret,
  mercadoPagoRedirectURI,
  apiBaseUrl
} from "../../../../config/server";

const SERVER_URL = apiBaseUrl;

function configure() {
  return Credentials.findOne({ source: "mercadopago" })
    .then(credential => {
      if (credential && credential.access_token) {
        return mercadopago.configure({
          access_token: credential.access_token
        });
      }
      throw new Error("Falta configurar mercado pago.");
    })
    .then(() => console.log("MercadPago configurado correctamente."))
    .catch(err => console.error(err));
}

function authorizeSeller(authorizationCode) {
  if (authorizationCode) {
    const data = {
      client_id: mercadoPagoClientId,
      client_secret: mercadoPagoClientSecret,
      grant_type: "authorization_code",
      code: authorizationCode,
      redirect_uri: mercadoPagoRedirectURI
    };
    const headers = {};
    const options = {
      method: "POST",
      headers: {
        accept: "application/json",
        "content-type": "application/x-www-form-urlencoded"
      },
      data: qs.stringify(data),
      url: "https://api.mercadopago.com/oauth/token"
    };
    return axios(options).then(response => {
      if (response.data && response.data.access_token) {
        let credentialsData = response.data;
        credentialsData.source = "mercadopago";
        return CredentialsService.createCredentials(credentialsData).then(
          () => {
            configure();
          }
        );
      }
      return Promise.resolve(null);
    });
  }
  return Promise.resolve(null);
}

function createPreference(order) {
  const items = order.items.map(item => {
    let discount_total = item.discount_total;
    if (isNaN(discount_total)) {
      discount_total = 0;
    }
    const unitPrice =
      item.price_total / item.quantity - discount_total / item.quantity;
    return {
      title: item.name,
      unit_price: unitPrice,
      currency_id: "ARS",
      quantity: item.quantity
    };
  });
  const preference = {
    items: items,
    notification_url: mercadoPagoNotificationUrl,
    marketplace_fee: parseInt(mercadoPagoMarketplaceFee),
    external_reference: order.id
  };
  return mercadopago.preferences
    .create(preference)
    .then(mercadopagoResponse => {
      return {
        redirectUrl: mercadopagoResponse.body.init_point,
        preferenceId: mercadopagoResponse.body.id
      };
    })
    .catch(err => {
      console.error("Error al crear la preference");
      console.error(err);
    });
}

export default {
  createPreference,
  authorizeSeller,
  configure
};
