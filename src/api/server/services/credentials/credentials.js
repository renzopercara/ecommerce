import Credentials from "../../models/credentials";

class CredentialsService {
  constructor() {}

  async createCredentials(data) {
    const credentialsCount = await Credentials.count({ source: data.source });
    if (credentialsCount === 0) {
      const newCredential = new Credentials(data);
      await newCredential.save();
      return Credentials.findOne({ _id: newCredential._id }).then(credential =>
        credential.toJSON()
      );
    }
    return Promise.resolve(null);
  }

  async deleteCredential(id) {
    return Credentials.remove({ _id: id }).then(() => this.getCredentials());
  }

  async getCredentials() {
    return Credentials.find({ source: "mercadopago" });
  }
}

export default new CredentialsService();
