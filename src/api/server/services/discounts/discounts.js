import Discounts from "../../models/discounts";
import parse from "../../lib/parse";

class DiscountsService {
  constructor() {}

  getAllDiscounts() {
    return Discounts.find({});
  }

  getDiscountById(id) {
    return Discounts.findById(id).then(discount => {
      let data = {};
      if (discount) {
        data = discount.toJSON();
      }
      return data;
    });
  }

  getDiscountByProductId(id) {
    return Discounts.findOne({ product_id: id }).then(discount => {
      let data = {};
      if (discount) {
        data = discount.toJSON();
      }
      return data;
    });
  }

  getDiscountRulesByCategory(category_id) {
    return Discounts.find({ category_id: category_id, active: true }).then(
      discounts => {
        if (discounts) {
          const data = discounts.map(discount => discount.toJSON());
          return data;
        }
        return [];
      }
    );
  }

  decideMostRelevantDiscount(discounts) {
    let filteredDiscounts = discounts.filter(
      discount =>
        discount.product_id &&
        discount.option_values_ids &&
        discount.option_values_ids.length > 0
    );
    if (filteredDiscounts.length > 0) {
      return filteredDiscounts[0];
    }
    filteredDiscounts = discounts.filter(discount => discount.product_id);
    if (filteredDiscounts.length > 0) {
      return filteredDiscounts[0];
    }
    filteredDiscounts = discounts.filter(
      discount => discount.category_id && discount.provider_id
    );
    if (filteredDiscounts.length > 0) {
      return filteredDiscounts[0];
    }
    filteredDiscounts = discounts.filter(discount => discount.category_id);
    if (filteredDiscounts.length > 0) {
      return filteredDiscounts[0];
    }
    return discounts[0];
  }

  parseDiscount(discount) {
    return {
      general_percentage: discount.general_percentage,
      name: discount.name,
      secondary_type: discount.secondary_type,
      mayorist_quantity: discount.mayorist_quantity,
      mayorist_quantity_percentage: discount.mayorist_quantity_percentage,
      minorist_quantity: discount.minorist_quantity,
      minorist_quantity_percentage: discount.minorist_quantity_percentage,
      mount: discount.mount,
      mount_percentage: discount.mount_percentage
    };
  }

  getProductDiscountRule(product) {
    let optionValues = [];
    if (product.options) {
      product.options.forEach(option => {
        let values = [];
        if (option.values) {
          values = option.values.map(value => value.option_value_id);
        }
        optionValues.concat(values);
      });
    }
    // console.log(product.id + ' . ' + product.provider_id + ' . ' + product.category_id);
    return Discounts.find({
      $and: [
        {
          $or: [
            { product_id: parse.getString(product.id) },
            {
              $and: [
                { category_id: product.category_id },
                { provider_id: product.provider_id }
              ]
            },
            { category_id: product.category_id },
            { provider_id: product.provider_id }
            // { option_values_ids: {$in: optionValues}}
          ]
        },
        { active: true }
      ]
    })
      .sort("updatedAt")
      .then(discounts => {
        if (discounts && discounts.length > 0) {
          if (discounts.length === 1) {
            return discounts[0];
          }
          const mostRelevantDiscount = this.decideMostRelevantDiscount(
            discounts
          );
          return mostRelevantDiscount;
        }
        return null;
      });
  }

  setDiscountToProduct(product) {
    return this.getProductDiscountRule(product).then(discount => {
      if (discount) {
        product.discount = this.parseDiscount(discount);
      }
      return product;
    });
  }

  updateDiscount(id, data) {
    const formattedData = this.getValidDocument(data);
    return Discounts.update({ _id: id }, { $set: formattedData }).then(() =>
      this.getDiscountById(id)
    );
  }

  createDiscount(data) {
    const formattedData = this.getValidDocument(data);
    if (formattedData.product_id) {
      return Discounts.findOne({ product_id: formattedData.product_id }).then(
        discount => {
          if (discount) {
            return Discounts.update(
              { _id: discount._id },
              { $set: formattedData }
            ).then(() => this.getDiscountById(discount._id));
          }
          const newDiscount = new Discounts(formattedData);
          return newDiscount.save();
        }
      );
    }
    const newDiscount = new Discounts(formattedData);
    return newDiscount.save();
  }

  getValidDocument(data) {
    const formatted = {};
    if (data.general_percentage && data.general_percentage !== undefined) {
      formatted.general_percentage = data.general_percentage;
    }
    if (data.category_id !== undefined) {
      formatted.category_id = data.category_id;
    }
    if (data.provider_id !== undefined) {
      formatted.provider_id = data.provider_id;
    }
    if (data.product_id !== undefined) {
      formatted.product_id = data.product_id;
    }
    if (data.name && data.name !== undefined) {
      formatted.name = data.name;
    }
    if (data.active !== undefined) {
      formatted.active = data.active;
    }

    if (data.secondary_type) {
      formatted.secondary_type = data.secondary_type;
    }
    //quantity
    if (data.mayorist_quantity_percentage) {
      formatted.mayorist_quantity_percentage =
        data.mayorist_quantity_percentage;
    }
    if (data.mayorist_quantity) {
      formatted.mayorist_quantity = data.mayorist_quantity;
    }
    if (data.minorist_quantity_percentage) {
      formatted.minorist_quantity_percentage =
        data.minorist_quantity_percentage;
    }
    if (data.minorist_quantity) {
      formatted.minorist_quantity = data.minorist_quantity;
    }
    //mount
    if (data.mount_percentage) {
      formatted.mount_percentage = data.mount_percentage;
    }
    if (data.mount) {
      formatted.mount = data.mount;
    }
    return formatted;
  }
}

export default new DiscountsService();
