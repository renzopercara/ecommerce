import Providers from "../../models/providers";

class ProvidersService {
  constructor() {}

  getAllProviders() {
    return Providers.find({});
  }

  getProviderById(id) {
    return Providers.findById(id)
      .populate("provider_user_id")
      .then(provider => {
        const data = {
          ...provider.toJSON(),
          username: provider.provider_user_id
            ? provider.provider_user_id.username
            : ""
        };
        return data;
      });
  }

  updateProvider(id, data) {
    const formattedData = this.getValidDocument(data);
    return Providers.update({ _id: id }, { $set: formattedData }).then(() =>
      this.getProviderById(id)
    );
  }

  createProvider(data) {
    const newProvider = new Providers(this.getValidDocument(data));
    return newProvider.save();
  }

  getValidDocument(data) {
    const formatted = {};
    if (data.accessToken && data.accessToken !== undefined) {
      formatted.access_token = data.accessToken;
    }

    if (data.synchronize !== undefined) {
      formatted.synchronize = data.synchronize;
    }
    if (data.shopId && data.shopId !== undefined) {
      formatted.shop_id = data.shopId;
    }
    if (data.name && data.name !== undefined) {
      formatted.name = data.name;
    }
    if (data.userAgent && data.userAgent !== undefined) {
      formatted.user_agent = data.userAgent;
    }
    if (data.code && data.code !== undefined) {
      formatted.code = data.code;
    }
    if (data.shop_source) {
      formatted.shop_source = data.shop_source;
    }
    if (data.shopify_access_token) {
      formatted.shopify_access_token = data.shopify_access_token;
    }
    if (data.shopify_shop_name) {
      formatted.shopify_shop_name = data.shopify_shop_name;
    }
    if (data.woocommerce_url) {
      formatted.woocommerce_url = data.woocommerce_url;
    }
    if (data.woocommerce_consumer_key) {
      formatted.woocommerce_consumer_key = data.woocommerce_consumer_key;
    }
    if (data.woocommerce_consumer_secret) {
      formatted.woocommerce_consumer_secret = data.woocommerce_consumer_secret;
    }
    return formatted;
  }
}

export default new ProvidersService();
