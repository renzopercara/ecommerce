import request from "request-promise";

//credenciales de modapormayor
// const clientId = '1009';
// const clientSecret = 'F7ljWiDqc3CtvmoFTAm6NYjS4SFICGyJXjebgKuIXuSrXWvv';

//otras credenciales
const clientId = "975";
const clientSecret = "f3XrJe8qsyF2GBpYuIdFhlzQqxQyJRNkLYgCjcEWoOch4QTt";

export function getStoreAccessToken(authorizationCode) {
  const options = {
    uri: "https://www.tiendanube.com/apps/authorize/token",
    method: "POST",
    body: {
      client_id: clientId,
      client_secret: clientSecret,
      grant_type: "authorization_code",
      code: "b6a022bc7793617c4d30a3a23723625905f5dd40"
    },
    json: true
  };
  return request(options)
    .then(response => {
      return response;
    })
    .catch(err => {
      console.error(err);
    });
}

function getProducts(
  accessToken,
  userAgent,
  storeId,
  providerId,
  pageNumber = 1
) {
  let options = {
    uri:
      "https://api.tiendanube.com/v1/" +
      storeId +
      "/products?per_page=150&page=" +
      pageNumber,
    headers: {
      Authentication: "bearer " + accessToken,
      "User-Agent": userAgent
    },
    json: true
  };
  return request(options)
    .then(shopProducts => {
      const products = shopProducts.map(product => {
        return {
          product: mapProduct(product, providerId),
          images: product.images.map(image => mapImage(image))
        };
      });
      return products;
    })
    .catch(err => {
      if (err.error && err.error.code === 404) {
        return [];
      } else {
        console.error(err.message);
        throw new Error(err.message);
      }
    });
}

function getCategories(accessToken, userAgent, storeId) {
  let options = {
    uri:
      "https://api.tiendanube.com/v1/" + storeId + "/categories?per_page=150",
    headers: {
      Authentication: "bearer " + accessToken,
      "User-Agent": userAgent
    },
    json: true
  };
  return request(options)
    .then(shopCategories => {
      const categories = shopCategories.map(category => mapCategory(category));
      return categories;
    })
    .catch(err => {
      if (err.error && err.error.code === 404) {
        return [];
      } else {
        console.error(err.message);
        throw new Error(err.message);
      }
    });
}

function getCategory(accessToken, userAgent, storeId, categoryId) {
  let options = {
    uri:
      "https://api.tiendanube.com/v1/" + storeId + "/categories/" + categoryId,
    headers: {
      Authentication: "bearer " + accessToken,
      "User-Agent": userAgent
    },
    json: true
  };
  return request(options)
    .then(shopCategory => {
      const category = mapCategory(shopCategory);
      return category;
    })
    .catch(err => {
      if (err.error && err.error.code === 404) {
        return {};
      } else {
        console.error(err.message);
        throw new Error(err.message);
      }
    });
}

const mapImage = image => {
  const data = {
    src: image.src,
    position: image.position,
    externalRef: image.id
  };
  return data;
};

const mapVariant = (variant, attributes) => {
  const optionsValues = variant.values.map((value, index) => {
    return {
      property: attributes[index].es,
      name: value.es
    };
  });
  const data = {
    price: variant.price,
    stock_quantity: variant.stock,
    external_ref: variant.id.toString(),
    options: optionsValues
  };
  return data;
};

const mapCategory = category => {
  const data = {
    name: category.name.es,
    slug: category.handle.es,
    external_ref: category.id,
    parent: category.parent,
    enabled: true
  };
  return data;
};

const mapProduct = (product, providerId) => {
  let data = {
    provider_id: providerId,
    name: product.name.es,
    description: product.description.es,
    enabled: product.published,
    external_ref: product.id,
    cost_price: product.variants[0].price,
    stock_quantity: product.variants[0].stock
  };
  data.categories = product.categories.map(category => mapCategory(category));
  data.variants = product.variants.map(variant =>
    mapVariant(variant, product.attributes)
  );
  return data;
};

export default {
  mapProduct,
  mapCategory,
  getCategories,
  getCategory,
  getProducts
};
