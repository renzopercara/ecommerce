import request from "request-promise";
import WooCommerceAPI from "woocommerce-api";

let WooCommerce = null;

function configure(credentials) {
  WooCommerce = new WooCommerceAPI({
    url: credentials.woocommerce_url, // Your store URL
    consumerKey: credentials.woocommerce_consumer_key, // Your consumer key
    consumerSecret: credentials.woocommerce_consumer_secret, // Your consumer secret
    wpAPI: true, // Enable the WP REST API integration
    version: "wc/v3" // WooCommerce WP REST API version
  });
}

function getCategory(id) {
  if (WooCommerce) {
    return WooCommerce.getAsync("products/categories/" + id).then(function(
      result
    ) {
      const shopCategory = JSON.parse(result.toJSON().body);
      const category = mapCategory(shopCategory);
      return category;
    });
  }
  console.error("No está configurado woocommerce");
  return Promise.resolve(null);
}

function getCategories() {
  if (WooCommerce) {
    return WooCommerce.getAsync("products/categories").then(function(result) {
      const shopCategories = JSON.parse(result.toJSON().body);
      if (shopCategories.length > 0) {
        const categories = shopCategories.map(shopCategory =>
          mapCategory(shopCategory)
        );
        return categories;
      }
      console.error(shopCategories);
      return [];
    });
  }
  console.error("No está configurado woocommerce!!");
  return Promise.resolve(null);
}

function getProducts(providerId) {
  if (WooCommerce) {
    return WooCommerce.getAsync("products").then(function(result) {
      const shopProducts = JSON.parse(result.body);

      if (shopProducts.length > 0) {
        const productsPromises = shopProducts.map(async product => {
          const formattedProduct = await mapProduct(product, providerId);
          const formattedImages = product.images.map((image, index) =>
            mapImage(image, index)
          );
          return {
            product: formattedProduct,
            images: formattedImages
          };
        });
        return Promise.all(productsPromises);
      }
      console.error(shopProducts);
      return Promise.resolve(null);
    });
  }
  console.error("No está configurado woocommerce!!");
  return Promise.resolve(null);
}

function getProductVariants(productId) {
  return WooCommerce.getAsync("products/" + productId + "/variations").then(
    function(result) {
      // console.log(result.toJSON().body);
      return JSON.parse(result.toJSON().body);
    }
  );
}

const mapProduct = async (product, providerId) => {
  let data = {
    provider_id: providerId,
    name: product.name,
    description: product.description,
    enabled: true,
    external_ref: product.id,
    cost_price: product.regular_price,
    stock_quantity: product.stock_quantity
  };
  data.options = mapOptions(product.attributes);
  const variants = await getProductVariants(product.id);
  data.variants = variants.map(variant =>
    mapVariant(variant, product.regular_price)
  );
  data.categories = product.categories.map(category => mapCategory(category));
  return data;
};

const mapCategory = category => {
  return {
    name: category.name,
    slug: category.slug,
    enabled: category.slug !== "uncategorized",
    parent: category.parent,
    external_ref: category.id
  };
};

const mapOptions = options => {
  let woocommerceOptions = [];
  options.forEach(option => {
    option.options.forEach(value =>
      woocommerceOptions.push({ property: option.name, name: value })
    );
  });
  return woocommerceOptions;
};

const mapVariant = (variant, productPrice) => {
  const variantOptions = variant.attributes.map(item => {
    return {
      property: item.name,
      name: item.option
    };
  });
  return {
    price: variant.price ? variant.price : productPrice,
    stock_quantity: variant.stock_quantity,
    external_ref: variant.id,
    enabled: true,
    options: variantOptions
  };
};

const mapImage = (image, position) => {
  const data = {
    src: image.src,
    position: position,
    externalRef: image.id
  };
  return data;
};

export default {
  getProducts,
  getCategories,
  getCategory,
  mapImage,
  mapCategory,
  mapProduct,
  mapVariant,
  mapOptions,
  configure
};
