import Tiendanube from "./tiendanube";
import Shopify from "./shopify";
import WooCommerce from "./woocommerce";
import ProductsService from "../../services/products/products";
import ProductVariantsService from "../../services/products/variants";
import ProductImagesService from "../../services/products/images";
import ProductCategoriesService from "../../services/products/productCategories";
import ProductOptionsService from "../../services/products/options";
import ProductOptionValuesService from "../../services/products/optionValues";
import Providers from "../../models/providers";

// SINCRONIZACION

const syncVariant = (productId, productVariants, variantsIds, variant) => {
  if (
    productVariants.length > 0 &&
    variantsIds.includes(variant.external_ref)
  ) {
    const variantId =
      productVariants[variantsIds.indexOf(variant.external_ref)].id;
    return ProductVariantsService.updateVariant(productId, variantId, variant);
  }
  return ProductVariantsService.addVariant(productId, variant);
};

const verifyIncludesProperty = (array, key, value) => {
  let valueIndex = -1;
  array.forEach((item, index) => {
    if (item[key].includes(value)) {
      valueIndex = index;
    }
  });
  return Promise.resolve(valueIndex);
};

function processOption(productId, existingOptions, option, optionIndex) {
  return verifyIncludesProperty(existingOptions, "name", option.property).then(
    index => {
      //me fijo si existe la opcion
      if (index > -1) {
        return verifyIncludesProperty(
          existingOptions[index].values,
          "name",
          option.name
        ).then(valueIndex => {
          //me fijo si existe el valor de la opcion
          if (valueIndex > -1) {
            return Promise.resolve(null);
          }
          return ProductOptionValuesService.addOptionValue(
            productId,
            existingOptions[index].id,
            {
              name: option.name
            }
          );
        });
      }
      // si no existe creo la opción
      return ProductOptionsService.addOption(productId, {
        name: option.property,
        control: "select",
        required: true,
        position: optionIndex
      }).then(productOptions => {
        let newOption = {};
        productOptions.forEach((productOption, index) => {
          if (productOption.name === option.property) {
            newOption = productOption;
          }
        });
        return ProductOptionValuesService.addOptionValue(
          productId,
          newOption.id,
          {
            name: option.name
          }
        );
      });
      return Promise.resolve(null);
    }
  );
}

const syncProductOptions = (productId, options) => {
  return ProductOptionsService.getOptions(productId).then(existingOptions => {
    return options.reduce(async (previousPromise, nextOption, index) => {
      await previousPromise;
      return processOption(productId, existingOptions, nextOption, index).then(
        () => Promise.resolve(null)
      );
    }, Promise.resolve());
  });
};

const syncProductVariants = (productId, variants) => {
  return ProductVariantsService.getVariants(productId).then(productVariants => {
    const variantsIds = productVariants.map(
      productVariant => productVariant.external_ref
    );
    return variants.reduce(async (previousPromise, nextVariant) => {
      await previousPromise;
      if (nextVariant.options) {
        return syncProductOptions(productId, nextVariant.options).then(() =>
          syncVariant(productId, productVariants, variantsIds, nextVariant)
        );
      }
      return syncVariant(productId, productVariants, variantsIds, nextVariant);
    }, Promise.resolve());
  });
};

//---------------------------------------------

const syncProductImages = (productId, images) => {
  let promises = images.map(image =>
    ProductImagesService.syncImage(productId, image)
  );
  return Promise.all(promises).then(() =>
    ProductImagesService.removeOldImages(productId, images)
  );
};

const syncCategory = (category, fetchCategory) => {
  let newCategoryId = "";
  return ProductCategoriesService.getCategories({
    slug: category.slug,
    external_ref: category.external_ref
  }).then(categoriesFound => {
    if (categoriesFound && categoriesFound.length > 0) {
      return categoriesFound[0].id;
    }
    if (category.parent !== 0) {
      return ProductCategoriesService.getCategories({
        external_ref: category.parent,
        fields: "external_ref, id"
      })
        .then(parentCategory => {
          if (parentCategory[0]) {
            category.parent_id = parentCategory[0].id;
            return;
          }
          if (fetchCategory) {
            return fetchCategory(category.parent)
              .then(fetchedCategory => syncCategory(fetchedCategory))
              .then(categoryId => {
                category.parent_id = categoryId;
              });
          }
          return;
        })
        .then(() => {
          return ProductCategoriesService.addCategory(category).then(() => {
            return ProductCategoriesService.getCategories({
              name: category.name
            }).then(categoriesFound => categoriesFound[0].id);
          });
        });
    }
    if (category.parent === 0) {
      return ProductCategoriesService.addCategory(category).then(() => {
        return ProductCategoriesService.getCategories({
          name: category.name
        }).then(categoriesFound => categoriesFound[0].id);
      });
    }
  });
};

const syncProductCategories = (productId, categories) => {
  if (categories && categories.length > 0) {
    const promises = categories.map(category => syncCategory(category));
    return Promise.all(promises).then(category_ids => {
      return ProductsService.updateProduct(productId, {
        category_id: category_ids[0],
        category_ids
      });
    });
  }
  return Promise.resolve(null);
};

const syncProduct = product => {
  return ProductsService.getProductByExternalRef(product.external_ref)
    .then(foundProduct => {
      if (foundProduct && foundProduct._id) {
        return ProductsService.updateProduct(foundProduct._id, product);
      }
      return ProductsService.addProduct(product);
    })
    .then(() =>
      ProductsService.getProductByExternalRef(product.external_ref).then(
        foundProduct => foundProduct._id
      )
    );
};

// TIENDANUBE

function syncTiendanubeShop(provider, pageNumber = 1) {
  const accessToken = provider.access_token;
  const userAgent = provider.user_agent;
  const shopId = provider.shop_id;
  return Tiendanube.getCategories(accessToken, userAgent, shopId, pageNumber)
    .then(categories => {
      return categories.reduce(async (previousPromise, nextCategory) => {
        await previousPromise;
        return syncCategory(nextCategory, categoryId => {
          return Tiendanube.getCategory(
            accessToken,
            userAgent,
            shopId,
            categoryId
          );
        });
      }, Promise.resolve(null));
    })
    .then(() =>
      Tiendanube.getProducts(
        accessToken,
        userAgent,
        shopId,
        provider._id,
        pageNumber
      )
    )
    .then(products => {
      if (products && products.length > 0) {
        let promises = products.map(item => {
          let productId = "";
          const tiendanubeProduct = item.product;
          const tiendanubeProductImages = item.images;
          return syncProduct(tiendanubeProduct)
            .then(id => {
              productId = id;
              return;
            })
            .then(() =>
              syncProductVariants(productId, tiendanubeProduct.variants)
            )
            .then(() => syncProductImages(productId, tiendanubeProductImages))
            .then(() =>
              syncProductCategories(productId, tiendanubeProduct.categories)
            );
        });
        return Promise.all(promises).then(() => {
          pageNumber += 1;
          return syncTiendanubeShop(provider, pageNumber);
        });
      }
      return Promise.resolve(null);
    })
    .catch(err => {
      console.error(err);
      return Providers.update(
        { _id: provider._id },
        { $set: { status: "error" } }
      );
    });
}

function processTiendanubeProvider(provider) {
  if (provider.access_token && provider.shop_id && provider.user_agent) {
    return syncTiendanubeShop(provider).then(() => {
      if (provider.status !== "active") {
        return Providers.update(
          { _id: provider._id },
          { $set: { status: "active" } }
        );
      }
      return Promise.resolve(null);
    });
  }
  return Providers.update(
    { _id: provider._id },
    { $set: { status: "missing_credentials" } }
  );
}

// SHOPIFY

function syncShopifyShop(provider) {
  return Shopify.getProducts(provider._id)
    .then(products => {
      if (products && products.length > 0) {
        let promises = products.map(item => {
          let productId = "";
          const shopifyProduct = item.product;
          const shopifyProductImages = item.images;
          return syncProduct(shopifyProduct)
            .then(id => {
              productId = id;
              return;
            })
            .then(() => syncProductVariants(productId, shopifyProduct.variants))
            .then(() => syncProductImages(productId, shopifyProductImages))
            .then(() =>
              syncProductCategories(productId, shopifyProduct.categories)
            );
        });
        return Promise.all(promises);
      }
      return Promise.resolve(null);
    })
    .catch(err => console.error(err));
}

function processShopifyProvider(provider) {
  if (provider.shopify_access_token && provider.shopify_shop_name) {
    Shopify.configure(
      provider.shopify_shop_name,
      provider.shopify_access_token
    );
    return syncShopifyShop(provider);
  }
  return Providers.update(
    { _id: provider._id },
    { $set: { status: "missing_credentials" } }
  );
}

// WOOCOMMERCE

function syncWoocommerceShop(provider) {
  return WooCommerce.getCategories()
    .then(categories => {
      return categories.reduce(async (previousPromise, nextCategory) => {
        await previousPromise;
        return syncCategory(nextCategory, categoryId =>
          WooCommerce.getCategory(categoryId)
        );
      }, Promise.resolve(null));
    })
    .then(() => WooCommerce.getProducts(provider._id));
  return WooCommerce.getProducts(provider._id)
    .then(products => {
      if (products && products.length > 0) {
        let promises = products.map(item => {
          const woocommerceProduct = item.product;
          const images = item.images;
          let productId = "";
          return syncProduct(woocommerceProduct)
            .then(id => {
              productId = id;
              return;
            })
            .then(() =>
              syncProductVariants(productId, woocommerceProduct.variants)
            )
            .then(() => syncProductImages(productId, images))
            .then(() =>
              syncProductCategories(productId, woocommerceProduct.categories)
            );
        });
        return Promise.all(promises);
      }
      return Promise.resolve(null);
    })
    .catch(err => console.error(err));
}

function processWoocommerceProvider(provider) {
  if (
    provider.woocommerce_url &&
    provider.woocommerce_consumer_key &&
    provider.woocommerce_consumer_secret
  ) {
    WooCommerce.configure(provider);
    return syncWoocommerceShop(provider);
  }
  return Providers.update(
    { _id: provider._id },
    { $set: { status: "missing_credentials" } }
  );
}

// GENERAL

function processProvider(provider) {
  switch (provider.shop_source) {
    case "tiendanube":
      return processTiendanubeProvider(provider);
    case "shopify":
      return processShopifyProvider(provider);
    case "woocommerce":
      return processWoocommerceProvider(provider);
    default:
      return Promise.resolve(null);
  }
}

export function syncProviderShops() {
  return Providers.find({ synchronize: true })
    .then(providers => {
      if (providers) {
        return providers.reduce(async (previousPromise, nextProvider) => {
          await previousPromise;
          console.log("EMPIEZO A SINCRONIZAR PROVIDER" + nextProvider);
          return processProvider(nextProvider);
        }, Promise.resolve(providers.length));
      }
      return Promise.resolve(0);
    })
    .catch(err => {
      console.error("An error ocurred synchronizing shops");
      console.error(err);
    });
}
