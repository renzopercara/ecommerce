import Shopify from "shopify-api-node";
// const username = "2054a10ff8ed14264a56bd978b8c3772";
// const password = "c3a773ca65408c8f4398fa13d78846b4";
// const shop = "evolve-de-prueba";
let shopify = null;

function configure(shopName, accessToken) {
  shopify = new Shopify({
    shopName: shopName,
    accessToken: accessToken
  });
}

function getProducts(providerId) {
  return shopify.product
    .list()
    .then(shopProducts => {
      const products = shopProducts.map(product => {
        return {
          product: mapProduct(product, providerId),
          images: product.images.map(image => mapImage(image))
        };
      });
      return products;
    })
    .catch(err => {
      if (err.error && err.error.code === 404) {
        return [];
      } else {
        console.error(err.message);
        throw new Error(err.message);
      }
    });
}

function getCategories(username, password, shop) {}

function getCategory(username, password, shop, categoryId) {}

const mapOptions = options => {
  let shopifyOptions = [];
  options.forEach(option => {
    option.values.forEach(value =>
      shopifyOptions.push({ property: option.name, name: value })
    );
  });
  return shopifyOptions;
};

const mapVariant = variant => {
  const variantOptions = [
    {
      property: "Talle",
      name: variant.option1
    },
    {
      property: "Color",
      name: variant.option2
    }
  ];
  return {
    price: variant.price,
    stock_quantity: variant.inventory_quantity,
    external_ref: variant.id,
    enabled: true,
    options: variantOptions
  };
};

const mapCategory = product => {
  const data = {
    name: product.product_type,
    slug: product.product_type,
    enabled: true,
    parent: 0
  };
  return data;
};

const mapImage = image => {
  const data = {
    src: image.src,
    position: image.position,
    externalRef: image.id
  };
  return data;
};

const mapProduct = (product, providerId) => {
  let data = {
    provider_id: providerId,
    name: product.title,
    description: product.body_html,
    enabled: true,
    external_ref: product.id,
    cost_price: product.variants[0].price,
    stock_quantity: product.variants[0].inventory_quantity
  };
  data.variants = product.variants.map(variant => mapVariant(variant));
  data.options = mapOptions(product.options);
  data.categories = [mapCategory(product)];
  return data;
};

export default {
  getProducts,
  getCategories,
  getCategory,
  mapCategory,
  mapProduct,
  configure
};
