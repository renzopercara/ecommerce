import request from "request-promise";
import sampleProducts from "./magento-sample-products";
// const username = "2054a10ff8ed14264a56bd978b8c3772";
// const password = "c3a773ca65408c8f4398fa13d78846b4";
// const shop = "evolve-de-prueba";

function getProducts(username, password, shop) {
  let options = {
    uri: `https://${username}:${password}@${shop}/admin/api/2019-04/products.json`,
    headers: {
      "Content-Type": "application/json"
    },
    json: true
  };
  return request(options)
    .then(response => {
      return response.products;
    })
    .catch(err => {
      if (err.error && err.error.code === 404) {
        return [];
      } else {
        console.error(err.message);
        throw new Error(err.message);
      }
    });
}

const mapOptions = options => {
  let shopifyOptions = [];
  options.forEach(option => {
    option.values.forEach(value =>
      shopifyOptions.push({ property: option.name, name: value })
    );
  });
  return shopifyOptions;
};

const mapVariant = variant => {
  return {
    price: variant.price,
    stock_quantity: variant.stock_quantity,
    external_ref: variant.id,
    enabled: true
  };
};

const mapCategory = product => {
  const data = {
    name: product.product_type,
    enabled: true
  };
  return data;
};

const mapImage = image => {
  const data = {
    src,
    position: image.position,
    externalRef: image.id
  };
  return data;
};

const mapProduct = (product, providerId) => {
  let data = {
    provider_id: providerId,
    name: product.name,
    description: product.description,
    enabled: true,
    external_ref: product.entity_id,
    cost_price: product.price,
    stock_quantity: 0
  };

  return data;
};

export default {
  getProducts,
  mapCategory,
  mapProduct
};
