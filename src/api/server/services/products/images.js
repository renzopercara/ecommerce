import { ObjectID } from "mongodb";
import path from "path";
import url from "url";
import formidable from "formidable";
import fse from "fs-extra";
import settings from "../../lib/settings";
import { db } from "../../lib/mongo";
import utils from "../../lib/utils";
import parse from "../../lib/parse";
import SettingsService from "../settings/settings";
import download from "image-downloader";

class ProductImagesService {
  constructor() {}

  getErrorMessage(err) {
    return { error: true, message: err.toString() };
  }

  getImages(productId) {
    if (!ObjectID.isValid(productId)) {
      return Promise.reject("Invalid identifier");
    }
    let productObjectID = new ObjectID(productId);

    return SettingsService.getSettings().then(generalSettings =>
      db
        .collection("products")
        .findOne({ _id: productObjectID }, { fields: { images: 1 } })
        .then(product => {
          if (product && product.images && product.images.length > 0) {
            let images = product.images.map(image => {
              image.url = url.resolve(
                generalSettings.domain,
                settings.productsUploadUrl +
                  "/" +
                  product._id +
                  "/" +
                  image.filename
              );
              return image;
            });

            images = images.sort((a, b) => a.position - b.position);
            return images;
          } else {
            return [];
          }
        })
    );
  }

  deleteImage(productId, imageId) {
    if (!ObjectID.isValid(productId) || !ObjectID.isValid(imageId)) {
      return Promise.reject("Invalid identifier");
    }
    let productObjectID = new ObjectID(productId);
    let imageObjectID = new ObjectID(imageId);

    return this.getImages(productId)
      .then(images => {
        if (images && images.length > 0) {
          let imageData = images.find(
            i => i.id.toString() === imageId.toString()
          );
          if (imageData) {
            let filename = imageData.filename;
            let filepath = path.resolve(
              settings.productsUploadPath + "/" + productId + "/" + filename
            );
            fse.removeSync(filepath);
            return db
              .collection("products")
              .updateOne(
                { _id: productObjectID },
                { $pull: { images: { id: imageObjectID } } }
              );
          } else {
            return true;
          }
        } else {
          return true;
        }
      })
      .then(() => true);
  }

  async addImage(req, res) {
    const productId = req.params.productId;
    if (!ObjectID.isValid(productId)) {
      res.status(500).send(this.getErrorMessage("Invalid identifier"));
      return;
    }

    let uploadedFiles = [];
    const productObjectID = new ObjectID(productId);
    const uploadDir = path.resolve(
      settings.productsUploadPath + "/" + productId
    );
    fse.ensureDirSync(uploadDir);

    let form = new formidable.IncomingForm();
    form.uploadDir = uploadDir;

    form
      .on("fileBegin", (name, file) => {
        // Emitted whenever a field / value pair has been received.
        file.name = utils.getCorrectFileName(file.name);
        file.path = uploadDir + "/" + file.name;
      })
      .on("file", async (field, file) => {
        // every time a file has been uploaded successfully,
        if (file.name) {
          const imageData = {
            id: new ObjectID(),
            alt: "",
            position: 99,
            filename: file.name
          };

          uploadedFiles.push(imageData);

          await db.collection("products").updateOne(
            {
              _id: productObjectID
            },
            {
              $push: { images: imageData }
            }
          );
        }
      })
      .on("error", err => {
        res.status(500).send(this.getErrorMessage(err));
      })
      .on("end", () => {
        res.send(uploadedFiles);
      });

    form.parse(req);
  }

  verifyIncludesProperty(array, key, value) {
    let valueIndex = -1;
    array.forEach((item, index) => {
      if (item[key] === value) {
        valueIndex = index;
      }
    });
    return Promise.resolve(valueIndex);
  }

  removeOldImages(productId, images) {
    const productObjectID = new ObjectID(productId);
    return db
      .collection("products")
      .findOne({
        _id: productObjectID
      })
      .then(product => {
        const promises = product.images.map(image => {
          return this.verifyIncludesProperty(
            images,
            "externalRef",
            image.externalRef
          ).then(index => {
            if (index === -1) {
              return this.deleteImage(productId, image.id);
            }
            return Promise.resolve(null);
          });
        });
        return Promise.all(promises);
      });
  }

  syncImage(productId, image) {
    return this.verifyImageModified(productId, image).then(
      ({ status, imageId }) => {
        if (status === "new") {
          return this.downloadImage(productId, image).then(imageData => {
            if (imageData !== null) {
              return this.saveNewImageDownloaded(productId, imageData);
            }
            return Promise.resolve(null);
          });
        }
        if (status === "modified" && imageId) {
          return this.downloadImage(productId, image).then(imageData => {
            if (imageData !== null) {
              imageData.id = imageId;
              return this.updateImage(productId, imageId, imageData);
            }
            return Promise.resolve(null);
          });
        }
        if (imageId) {
          const imageData = {
            position: image.position
          };
          return this.updateImage(productId, imageId, imageData);
        }
        return;
      }
    );
  }

  verifyImageModified(productId, image) {
    let productObjectID = new ObjectID(productId);
    return db
      .collection("products")
      .findOne({
        _id: productObjectID
      })
      .then(product => {
        const images = product.images;
        const imagesIds = images.map(image => image.externalRef);
        const index = imagesIds.indexOf(image.externalRef);
        if (index !== -1) {
          if (images[index].src !== image.src) {
            return { status: "modified", imageId: images[index].id };
          }
          return { status: "not_modified", imageId: images[index].id };
        }
        return { status: "new" };
      });
  }

  downloadImageToAllSizes(src, productId, externalRef) {
    let uploadDir = path.resolve(settings.productsUploadPath + "/" + productId);
    fse.ensureDirSync(uploadDir);
    let options = {
      url: src,
      headers: {
        "user-agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"
      },
      dest: uploadDir + "/" + externalRef + ".jpg"
    };
    return download.image(options);
  }

  downloadImage(productId, productImage) {
    const uploadDir = path.resolve(
      settings.productsUploadPath + "/" + productId
    );
    fse.ensureDirSync(uploadDir);
    return this.downloadImageToAllSizes(
      productImage.src,
      productId,
      productImage.externalRef
    )
      .then(() => {
        const imageData = {
          id: new ObjectID(),
          position: productImage.position,
          alt: parse.getString(productImage.externalRef),
          filename: productImage.externalRef + ".jpg",
          src: productImage.src,
          externalRef: productImage.externalRef
        };
        return imageData;
      })
      .catch(err => {
        // console.error(err);
        return null;
      });
  }

  saveNewImageDownloaded(productId, imageData) {
    if (!ObjectID.isValid(productId)) {
      return Promise.reject("Invalid product identifier");
    }
    let productObjectID = new ObjectID(productId);

    return db.collection("products").updateOne(
      {
        _id: productObjectID
      },
      { $push: { images: imageData } }
    );
  }

  updateImage(productId, imageId, data) {
    if (!ObjectID.isValid(productId) || !ObjectID.isValid(imageId)) {
      return Promise.reject("Invalid identifier");
    }
    let productObjectID = new ObjectID(productId);
    let imageObjectID = new ObjectID(imageId);

    const imageData = this.getValidDocumentForUpdate(data);

    return db.collection("products").updateOne(
      {
        _id: productObjectID,
        "images.id": imageObjectID
      },
      { $set: imageData }
    );
  }

  getValidDocumentForUpdate(data) {
    if (Object.keys(data).length === 0) {
      return new Error("Required fields are missing");
    }

    let image = {};

    if (data.alt !== undefined) {
      image["images.$.alt"] = parse.getString(data.alt);
    }

    if (data.position !== undefined) {
      image["images.$.position"] =
        parse.getNumberIfPositive(data.position) || 0;
    }

    if (data.src && data.src !== undefined) {
      image["images.$.src"] = parse.getString(data.src);
    }

    if (data.externalRef && data.externalRef !== undefined) {
      image["images.$.externalRef"] = parse.getString(data.externalRef);
    }

    return image;
  }
}

export default new ProductImagesService();
