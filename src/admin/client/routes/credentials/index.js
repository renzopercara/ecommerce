import React from "react";
import Credentials from "modules/credentials";

export default () => (
  <div className="row row--no-gutter col-full-height">
    <h1>Configuración de Credenciales (MercadoPago) </h1>
    <div className="col-xs-12 col-sm-8 col-md-9 col--no-gutter scroll col-full-height">
      <Credentials />
    </div>
  </div>
);
