import React from "react";
import * as auth from "lib/auth";
import { Redirect } from "react-router-dom";

export default class OAuth extends React.Component {
  componentWillMount() {
    const token = this.props.match.params.token;
    auth.storeToken(token);
    window.location = "https://mintomin.com/";
  }

  render() {
    return null;
  }
}
