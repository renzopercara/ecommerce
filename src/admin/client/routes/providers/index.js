import React from "react";
import ProvidersList from "modules/providers/list";

export default () => (
  <div className="row row--no-gutter col-full-height">
    <div
      style={{ backgroundColor: "#fff", overflowX: "scroll" }}
      className="col-xs-12 col-sm-8 col-md-9 col--no-gutter scroll col-full-height"
    >
      <ProvidersList />
    </div>
  </div>
);
