import React from "react";
import messages from "lib/text";
import { login } from "../api/auth";
import settings from "lib/settings";
import * as auth from "lib/auth";

import RaisedButton from "material-ui/RaisedButton";
import Paper from "material-ui/Paper";
import TextField from "material-ui/TextField";
import SocialLogin from "../../../../theme/src/components/header/login";

export default class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: localStorage.getItem("dashboard_email") || "",
      isFetching: false,
      isAuthorized: false,
      error: null
    };
  }

  handleChange = event => {
    const id = event.target.id;
    this.setState({
      [id]: event.target.value
    });
  };

  handleKeyPress = e => {
    if (e.keyCode === 13 || e.which === 13) {
      this.handleSubmit();
    }
  };

  handleSubmit = () => {
    this.setState({
      isFetching: true,
      isAuthorized: false,
      error: null
    });
    login({ username: this.state.username, password: this.state.password })
      .then(data => {
        if (data.error) {
          this.setState({
            isFetching: false,
            isAuthorized: false,
            error: data.error
          });
        }
        if (data.token) {
          auth.storeToken(data.token);
          this.setState({
            isFetching: false,
            isAuthorized: true,
            error: null
          });
          this.props.history.push("/admin");
        }
      })
      .catch(data => {
        this.setState({
          isFetching: false,
          isAuthorized: false,
          error: data.error
        });
      });
  };

  render() {
    const { isFetching, isAuthorized, error } = this.state;

    let response = null;
    if (isFetching) {
      response = (
        <div className="loginSuccessResponse">{messages.messages_loading}</div>
      );
    } else if (error) {
      response = <div className="loginErrorResponse">{error}</div>;
    }

    return (
      <div className="row col-full-height left-xs middle-xs fondo">
        <div className="col-xs-12 col-sm-8 col-md-6 col-lg-4">
          <Paper className="loginBox">
            <div className="loginTitle text-center"><img src="https://mintomin.com/logo.png" class="logoTipo"/></div>
            <div className="loginInput">
              <TextField
                type="text"
                id="username"
                onChange={this.handleChange}
                onKeyPress={this.handleKeyPress}
                label={"Usuario"}
                fullWidth={true}
                hintStyle={{ width: "100%" }}
                hintText={"Usuario"}
              />
              <TextField
                type="password"
                id="password"
                onChange={this.handleChange}
                onKeyPress={this.handleKeyPress}
                label={"Contraseña"}
                fullWidth={true}
                hintStyle={{ width: "100%" }}
                hintText={"Contraseña"}
              />
            </div>
            <RaisedButton
              label="Iniciar Sesión"
              fullWidth={true}
              primary={true}
              disabled={isFetching}
              onClick={this.handleSubmit}
            />
          </Paper>
        </div>
      </div>
    );
  }
}
