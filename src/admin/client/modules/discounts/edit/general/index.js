import { connect } from "react-redux";
import { withRouter } from "react-router";
import { updateDiscount, createDiscount } from "../../actions";
import DiscountGeneralForm from "./components/form";

const mapStateToProps = (state, ownProps) => {
  return {
    initialValues: state.discounts.editDiscount,
    providers: state.providers.items,
    categories: state.productCategories.items
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onSubmit: values => {
      const { discountId } = ownProps.match.params;
      if (discountId) {
        dispatch(updateDiscount(discountId, values));
      } else {
        dispatch(createDiscount(values));
      }
    }
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(DiscountGeneralForm)
);
