import React from "react";
import { connect } from "react-redux";
import { Field, reduxForm, formValueSelector } from "redux-form";
import MenuItem from "material-ui/MenuItem";
import { TextField, SelectField, Checkbox } from "redux-form-material-ui";

import messages from "lib/text";
import style from "./style.css";

import Paper from "material-ui/Paper";
import FlatButton from "material-ui/FlatButton";
import RaisedButton from "material-ui/RaisedButton";

const validate = values => {
  const errors = {};
  const requiredFields = ["name"];

  requiredFields.map(field => {
    if (values && !values[field]) {
      errors[field] = messages.errors_required;
    }
  });

  return errors;
};

const asyncValidate = null;

let DiscountGeneralForm = ({
  handleSubmit,
  pristine,
  reset,
  submitting,
  initialValues,
  categories,
  secondary_type,
  providers
}) => {
  return (
    <form onSubmit={handleSubmit}>
      <Paper className="paper-box" zDepth={1}>
        <div className={style.innerBox}>
          <Field
            name="name"
            component={TextField}
            floatingLabelText={"Nombre del descuento *"}
            fullWidth={true}
          />
          <Field
            name="general_percentage"
            component={TextField}
            floatingLabelText={"Porcentaje de descuento"}
            fullWidth={false}
          />
          <Field
            name="active"
            component={Checkbox}
            label="Activo"
            fullWidth={true}
          />
          <Field
            name="category_id"
            component={SelectField}
            fullWidth={true}
            floatingLabelText="Categoría"
            hintText="Seleccione categoría"
          >
            <MenuItem value={""} primaryText={"Todos"} />
            {categories &&
              categories.map(category => {
                return (
                  <MenuItem value={category.id} primaryText={category.name} />
                );
              })}
          </Field>
          <Field
            name="provider_id"
            component={SelectField}
            fullWidth={true}
            floatingLabelText="Proveedor"
            hintText="Seleccione proveedor"
          >
            <MenuItem value={""} primaryText={"Todos"} />
            {providers &&
              providers.map(provider => {
                return (
                  <MenuItem value={provider._id} primaryText={provider.name} />
                );
              })}
          </Field>
        </div>
        <div
          className={
            "buttons-box " +
            (pristine ? "buttons-box-pristine" : "buttons-box-show")
          }
        >
          <FlatButton
            label={messages.cancel}
            className={style.button}
            onClick={reset}
            disabled={pristine || submitting}
          />
          <RaisedButton
            type="submit"
            label={messages.save}
            primary={true}
            className={style.button}
            disabled={pristine || submitting}
          />
        </div>
      </Paper>
    </form>
  );
};

DiscountGeneralForm = reduxForm({
  form: "DiscountGeneralForm",
  validate,
  enableReinitialize: true
})(DiscountGeneralForm);

const selector = formValueSelector("DiscountGeneralForm");

DiscountGeneralForm = connect(state => ({
  secondary_type: selector(state, "secondary_type")
}))(DiscountGeneralForm);

export default DiscountGeneralForm;
