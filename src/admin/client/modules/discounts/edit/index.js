import React from "react";
import messages from "lib/text";

import DiscountGeneral from "modules/discounts/edit/general";

class DiscountEditContainer extends React.Component {
  componentDidMount() {
    this.props.fetchData();
    this.props.fetchCategories();
    this.props.fetchProviders();
  }

  componentWillUnmount() {
    this.props.eraseData();
  }

  render() {
    return (
      <div>
        <div style={{ margin: 20, color: "rgba(0, 0, 0, 0.52)" }}>
          {messages.description}
        </div>
        <DiscountGeneral />
      </div>
    );
  }
}

import { connect } from "react-redux";
import { withRouter } from "react-router";
import { fetchDiscount, cancelDiscountEdit } from "../actions";
import { fetchCategories } from "../../productCategories/actions";
import { fetchProviders } from "../../providers/actions";

const mapStateToProps = (state, ownProps) => {
  return {
    discount: state.discounts.editDiscount
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    fetchData: () => {
      const { discountId } = ownProps.match.params;
      if (discountId) {
        dispatch(fetchDiscount(discountId));
      }
    },
    eraseData: () => {
      dispatch(cancelDiscountEdit());
    },
    fetchCategories: () => {
      dispatch(fetchCategories());
    },
    fetchProviders: () => {
      dispatch(fetchProviders());
    }
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(DiscountEditContainer)
);
