import React from "react";
import { Field, reduxForm } from "redux-form";
import { TextField } from "redux-form-material-ui";

import style from "./style.css";
import messages from "lib/text";

import Paper from "material-ui/Paper";
import FlatButton from "material-ui/FlatButton";
import RaisedButton from "material-ui/RaisedButton";

let FactorsForm = ({
  handleSubmit,
  pristine,
  reset,
  submitting,
  initialValues
}) => {
  return (
    <form onSubmit={handleSubmit}>
      <Paper className="paper-box" zDepth={1}>
        <div className={style.innerBox}>
          <Field
            name="minorist_message"
            component={TextField}
            floatingLabelText={"Mensaje minorista"}
            fullWidth={false}
          />
          <Field
            name="minorist_factor"
            component={TextField}
            floatingLabelText={"Factor minorista"}
            fullWidth={false}
          />
          <Field
            name="mayorist_message"
            component={TextField}
            floatingLabelText={"Mensaje mayorista"}
            fullWidth={false}
          />
          <Field
            name="mayorist_factor"
            component={TextField}
            floatingLabelText={"Factor mayorista"}
            fullWidth={false}
          />
          <Field
            name="mayorist_quantity"
            component={TextField}
            floatingLabelText={"Cantidad mayorista"}
            fullWidth={false}
          />
          <Field
            name="reseller_message"
            component={TextField}
            floatingLabelText={"Mensaje revendedor"}
            fullWidth={false}
          />
          <Field
            name="reseller_factor"
            component={TextField}
            floatingLabelText={"Factor revendedor"}
            fullWidth={false}
          />
          <Field
            name="reseller_quantity"
            component={TextField}
            floatingLabelText={"Cantidad revendedor"}
            fullWidth={false}
          />
        </div>
        <div
          className={
            "buttons-box " +
            (pristine ? "buttons-box-pristine" : "buttons-box-show")
          }
        >
          <FlatButton
            label={messages.cancel}
            className={style.button}
            onClick={reset}
            disabled={pristine || submitting}
          />
          <RaisedButton
            type="submit"
            label={messages.save}
            primary={true}
            className={style.button}
            disabled={pristine || submitting}
          />
        </div>
      </Paper>
    </form>
  );
};

FactorsForm = reduxForm({
  form: "FactorsForm",
  enableReinitialize: true
})(FactorsForm);

export default FactorsForm;
