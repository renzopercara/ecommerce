import { connect } from "react-redux";
import { withRouter } from "react-router";
import { updateFactors } from "../../actions";
import FactorsForm from "./factors";

const mapStateToProps = (state, ownProps) => {
  return {
    initialValues: state.discounts.factors
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onSubmit: values => {
      dispatch(updateFactors(values));
    }
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(FactorsForm)
);
