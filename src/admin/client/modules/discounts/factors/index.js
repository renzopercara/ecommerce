import React from "react";
import messages from "lib/text";

import FactorsForm from "modules/discounts/factors/form";

class FactorsContainer extends React.Component {
  componentDidMount() {
    this.props.fetchData();
  }

  render() {
    return (
      <div>
        <div style={{ margin: 20, color: "rgba(0, 0, 0, 0.52)" }}>
          {"Factores"}
        </div>
        <FactorsForm />
      </div>
    );
  }
}

import { connect } from "react-redux";
import { withRouter } from "react-router";
import { fetchFactors } from "../actions";

const mapStateToProps = (state, ownProps) => {
  return {
    factors: state.discounts.factors
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    fetchData: () => {
      dispatch(fetchFactors());
    }
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(FactorsContainer)
);
