import React from "react";
import Subheader from "material-ui/Subheader";
import messages from "lib/text";

const Head = ({}) => (
  <Subheader>
    <div className="row row--no-gutter middle-xs">
      <div className="col-xs-6 col--no-gutter">
        <div className="row row--no-gutter middle-xs">
          <div className="col-xs-8">Nombre del Descuento</div>
        </div>
      </div>
      <div className="col-xs-2 col--no-gutter">Porcentaje</div>
    </div>
  </Subheader>
);

export default Head;
