import React from "react";
import { Link } from "react-router-dom";
import Divider from "material-ui/Divider";
import FontIcon from "material-ui/FontIcon";
import messages from "lib/text";
import * as helper from "lib/helper";
import style from "./style.css";

const formatStatus = status => {
  switch (status) {
    case "active":
      return "Activo";
    case "inactive":
      return "Inactivo";
    case "missing_credentials":
      return "Faltan credenciales";
    case "error":
      return "Error de sincronización";
    default:
      return "-";
  }
};

const ItemStock = ({ status, quantity }) => {
  let stockValue = "";
  let stockClass = "";
  switch (status) {
    case "discontinued":
      stockValue = messages.discounts_discontinued;
      stockClass = style.discontinued;
      break;
    case "backorder":
      stockValue = messages.discounts_backorder;
      stockClass = style.backorder;
      break;
    case "preorder":
      stockValue = messages.discounts_preorder;
      stockClass = style.preorder;
      break;
    case "available":
      stockValue = quantity;
      stockClass = style.inStock;
      break;
    case "out_of_stock":
    default:
      stockValue = messages.discounts_outOfStock;
      stockClass = style.outOfStock;
      break;
  }

  return <div className={stockClass}>{stockValue}</div>;
};

const DiscountItem = ({ discount, onSelect, selected, settings }) => {
  let discountClass = style.productName;
  const discountName =
    discount.name && discount.name.length > 0
      ? discount.name
      : `<${messages.draft}>`;

  return (
    <div className={"products-item" + (selected === true ? " selected" : "")}>
      <div className={"row row--no-gutter middle-xs " + style.innerItem}>
        <div className="col-xs-6 col--no-gutter">
          <div className="col-xs-8">
            <Link
              to={"/admin/discount/" + discount._id}
              className={discountClass}
            >
              {discountName}
              <br />
            </Link>
          </div>
        </div>
        <div className={"col-xs-2 " + style.sku}>
          {discount.general_percentage}%
        </div>
      </div>
      <Divider />
    </div>
  );
};

export default DiscountItem;
