import { connect } from "react-redux";
import { withRouter } from "react-router";
import {
  fetchDiscounts,
  fetchMoreDiscounts,
  selectDiscount,
  deselectDiscount,
  selectAllDiscount,
  deselectAllDiscount,
  fetchFactors
} from "../actions";
import List from "./components/list";

const mapStateToProps = (state, ownProps) => {
  return {
    settings: state.settings.settings,
    items: state.discounts.items || [],
    selected: state.discounts.selected,
    loadingItems: state.discounts.loadingItems,
    hasMore: state.discounts.hasMore,
    totalCount: state.discounts.totalCount
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onLoad: () => {
      dispatch(fetchDiscounts());
    },
    loadFactors: () => {
      dispatch(fetchFactors());
    },
    onSelect: event => {
      const discountId = event.target.value;
      const checked = event.target.checked;

      if (checked) {
        dispatch(selectDiscount(discountId));
      } else {
        dispatch(deselectDiscount(discountId));
      }
    },
    onSelectAll: event => {
      const checked = event.target.checked;

      if (checked) {
        dispatch(selectAllDiscount());
      } else {
        dispatch(deselectAllDiscount());
      }
    },
    loadMore: () => {
      dispatch(fetchMoreDiscounts());
    }
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(List)
);
