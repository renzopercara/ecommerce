import * as t from "./actionTypes";
import {
  getDiscounts,
  doUpdateDiscount,
  doCreateDiscount,
  getDiscountById,
  getDiscountByProductId,
  getFactors,
  doUpdateFactors
} from "../../api/discounts";
import messages from "lib/text";

function requestDiscount() {
  return {
    type: t.DISCOUNT_DETAIL_REQUEST
  };
}

function receiveDiscount(item) {
  return {
    type: t.DISCOUNT_DETAIL_RECEIVE,
    item
  };
}

function receiveDiscountError(error) {
  return {
    type: t.DISCOUNT_DETAIL_FAILURE,
    error
  };
}

export function cancelDiscountEdit() {
  return {
    type: t.DISCOUNT_DETAIL_ERASE
  };
}

function requestDiscounts() {
  return {
    type: t.DISCOUNTS_REQUEST
  };
}

function requestMoreDiscounts() {
  return {
    type: t.DISCOUNTS_MORE_REQUEST
  };
}

function receiveDiscountsMore({ has_more, total_count, data }) {
  return {
    type: t.DISCOUNTS_MORE_RECEIVE,
    has_more,
    total_count,
    data
  };
}

function receiveDiscounts({ has_more, total_count, data }) {
  return {
    type: t.DISCOUNTS_RECEIVE,
    has_more,
    total_count,
    data
  };
}

function receiveDiscountsError(error) {
  return {
    type: t.DISCOUNTS_FAILURE,
    error
  };
}

export function selectDiscount(id) {
  return {
    type: t.DISCOUNTS_SELECT,
    discountId: id
  };
}

export function deselectDiscount(id) {
  return {
    type: t.DISCOUNTS_DESELECT,
    discountId: id
  };
}

export function deselectAllDiscount() {
  return {
    type: t.DISCOUNTS_DESELECT_ALL
  };
}

export function selectAllDiscount() {
  return {
    type: t.DISCOUNTS_SELECT_ALL
  };
}

function requestUpdateDiscount() {
  return {
    type: t.DISCOUNT_UPDATE_REQUEST
  };
}

function receiveUpdateDiscount(item) {
  return {
    type: t.DISCOUNT_UPDATE_SUCCESS,
    item
  };
}

function errorUpdateDiscount(error) {
  return {
    type: t.DISCOUNT_UPDATE_FAILURE,
    error
  };
}

function receiveUpdateDiscount(factors) {
  return {
    type: t.DISCOUNT_UPDATE_FACTORS,
    factors
  };
}

function receiveFactors(factors) {
  return {
    type: t.DISCOUNT_RECEIVE_FACTORS,
    factors
  };
}

export function createDiscount(data) {
  return (dispatch, getState) => {
    dispatch(requestUpdateDiscount());
    return doCreateDiscount(data)
      .then(json => {
        dispatch(receiveUpdateDiscount(json.data));
      })
      .catch(err => {
        dispatch(errorUpdateDiscount(err));
      });
  };
}

export function fetchProductDiscount(productId) {
  return (dispatch, getState) => {
    const state = getState();
    if (state.discounts.loadingItems) {
      // do nothing
    } else {
      dispatch(requestDiscount());

      return getDiscountByProductId(productId)
        .then(json => {
          dispatch(receiveDiscount(json.data));
        })
        .catch(error => {
          dispatch(receiveDiscountError(error));
        });
    }
  };
}
export function updateDiscount(id, data) {
  return (dispatch, getState) => {
    dispatch(requestUpdateDiscount());
    return doUpdateDiscount(id, data)
      .then(json => {
        dispatch(receiveUpdateDiscount(json.data));
      })
      .catch(err => {
        dispatch(errorUpdateDiscount(err));
      });
  };
}

export function fetchDiscount(id) {
  return (dispatch, getState) => {
    const state = getState();
    if (state.discounts.loadingItems) {
      // do nothing
    } else {
      dispatch(requestDiscount());

      return getDiscountById(id)
        .then(json => {
          dispatch(receiveDiscount(json.data));
        })
        .catch(error => {
          dispatch(receiveDiscountError(error));
        });
    }
  };
}

export function fetchDiscounts() {
  return (dispatch, getState) => {
    const state = getState();
    if (state.discounts.loadingItems) {
      // do nothing
    } else {
      dispatch(requestDiscounts());
      dispatch(deselectAllDiscount());
      return getDiscounts()
        .then(json => {
          dispatch(receiveDiscounts(json));
        })
        .catch(error => {
          dispatch(receiveDiscountsError(error));
        });
    }
  };
}

export function fetchMoreDiscounts() {
  return (dispatch, getState) => {
    const state = getState();
    if (!state.discounts.loadingItems) {
      dispatch(requestMoreDiscounts());

      return getMoreDiscounts()
        .then(({ status, json }) => {
          dispatch(receiveDiscountsMore(json));
        })
        .catch(error => {
          dispatch(receiveDiscountsError(error));
        });
    }
  };
}

export function fetchFactors() {
  return (dispatch, getState) => {
    return getFactors()
      .then(json => {
        dispatch(receiveFactors(json.data));
      })
      .catch(error => {
        console.error(error);
      });
  };
}

export function updateFactors(data) {
  return (dispatch, getState) => {
    return doUpdateFactors(data)
      .then(json => {
        dispatch(receiveUpdateFactor(json.data));
      })
      .catch(err => {
        console.error(err);
      });
  };
}
