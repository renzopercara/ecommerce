import React from "react";
import { Field, reduxForm } from "redux-form";
import { TextField, Checkbox } from "redux-form-material-ui";

import messages from "lib/text";
import style from "./style.css";

import Paper from "material-ui/Paper";
import FlatButton from "material-ui/FlatButton";
import RaisedButton from "material-ui/RaisedButton";

const validate = values => {
  const errors = {};
  const requiredFields = [];

  requiredFields.map(field => {
    if (values && !values[field]) {
      errors[field] = messages.errors_required;
    }
  });

  return errors;
};

const ProductDiscountsForm = ({
  handleSubmit,
  pristine,
  reset,
  submitting,
  initialValues,
  product
}) => {
  if (initialValues) {
    return (
      <form onSubmit={handleSubmit}>
        <Paper className="paper-box" zDepth={1}>
          <div className={style.innerBox}>
            <Field
              name="_id"
              type="hidden"
              style={{ height: 0 }}
              component={TextField}
            />
            <Field
              name="product_id"
              type="hidden"
              style={{ height: 0 }}
              component={TextField}
            />
            <Field
              name="name"
              component={TextField}
              floatingLabelText={"Nombre del descuento *"}
              fullWidth={true}
            />
            <p className="field-hint">
              {
                "Este descuento se aplicará solo al product y va a pisar todos los descuentos por categoría o proveedor"
              }
            </p>
            <Field
              name="general_percentage"
              component={TextField}
              floatingLabelText={"Porcentaje de descuento *"}
              fullWidth={true}
            />
            <Field
              name="active"
              component={Checkbox}
              label="Activo"
              fullWidth={true}
            />
          </div>
          <div
            className={
              "buttons-box " +
              (pristine ? "buttons-box-pristine" : "buttons-box-show")
            }
          >
            <FlatButton
              label={messages.cancel}
              className={style.button}
              onClick={reset}
              disabled={pristine || submitting}
            />
            <RaisedButton
              type="submit"
              label={messages.save}
              primary={true}
              className={style.button}
              disabled={pristine || submitting}
            />
          </div>
        </Paper>
      </form>
    );
  } else {
    return null;
  }
};

export default reduxForm({
  form: "ProductDiscountsForm",
  validate,
  enableReinitialize: true
})(ProductDiscountsForm);
