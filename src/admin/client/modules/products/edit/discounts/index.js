import { connect } from "react-redux";
import { withRouter } from "react-router";
import { updateDiscount, createDiscount } from "../../../discounts/actions";
import ProductDiscountsForm from "./components/form";

const mapStateToProps = (state, ownProps) => {
  let initialValues = {};
  if (state.discounts.editDiscount) {
    initialValues._id = state.discounts.editDiscount._id;
    initialValues.active = state.discounts.editDiscount.active;
    initialValues.general_percentage =
      state.discounts.editDiscount.general_percentage;
  }
  if (state.products.editProduct) {
    (initialValues.product_id = state.products.editProduct.id),
      (initialValues.name = state.products.editProduct.name);
  }
  return {
    initialValues: initialValues,
    product: state.products.editProduct
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onSubmit: values => {
      if (values._id) {
        dispatch(
          updateDiscount(values._id, {
            name: values.name,
            general_percentage: values.general_percentage,
            active: values.active
          })
        );
      } else {
        dispatch(
          createDiscount({
            product_id: values.product_id,
            name: values.name,
            general_percentage: values.general_percentage,
            active: values.active
          })
        );
      }
    }
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ProductDiscountsForm)
);
