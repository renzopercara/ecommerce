import React from "react";
import Subheader from "material-ui/Subheader";
import messages from "lib/text";

const Head = ({ onSelectAll }) => (
  <Subheader>
    <div
      style={{ display: "flex", flexDirection: "row" }}
      className="row--no-gutter"
    >
      <div style={{ minWidth: 413 }} className="col-xs-5 col--no-gutter">
        <div className="row row--no-gutter middle-xs">
          <div className="col-xs-1 col--no-gutter">
            <input type="checkbox" onChange={onSelectAll} />
          </div>
          <div className="col-xs-11">{messages.products_name}</div>
        </div>
      </div>
      <div style={{ minWidth: 167 }} className="col-xs-2 col--no-gutter">
        {messages.providers_name}
      </div>
      <div style={{ minWidth: 84 }} className="col-xs-1 col--no-gutter">
        {messages.products_sku}
      </div>
      <div style={{ minWidth: 84 }} className="col-xs-1 col--no-gutter">
        {messages.products_stock}
      </div>
      <div
        className="col-xs-1 col--no-gutter"
        style={{ minWidth: 84, textAlign: "right", paddingRight: 23 }}
      >
        {messages.products_price}
      </div>
      <div style={{ minWidth: 167 }} className="col-xs-2 col--no-gutter">
        {messages.last_update}
      </div>
    </div>
  </Subheader>
);

export default Head;
