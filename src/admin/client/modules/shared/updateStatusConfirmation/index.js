import React from "react";
import messages from "lib/text";
import Dialog from "material-ui/Dialog";
import FlatButton from "material-ui/FlatButton";
import SelectField from "material-ui/SelectField";
import style from "../../orders/listFilter/components/style.css";
import MenuItem from "material-ui/MenuItem";

export default class UpdateStatusConfirmation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: props.open,
      statusSelected: ""
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.open !== nextProps.open) {
      this.setState({
        open: nextProps.open
      });
    }
  }

  close = () => {
    this.setState({ open: false });
  };

  handleCancel = () => {
    this.close();
    if (this.props.onCancel) {
      this.props.onCancel();
    }
  };

  handleSave = () => {
    const { statusSelected } = this.state;

    this.close();
    if (this.props.onSave) {
      this.props.onSave(statusSelected);
    }
  };

  render() {
    const { statusSelected } = this.state;
    const { orderStatuses } = this.props;

    const actions = [
      <FlatButton
        label={messages.cancel}
        onClick={this.handleCancel}
        style={{ marginRight: 10 }}
      />,
      <FlatButton
        label={messages.save}
        primary={true}
        keyboardFocused={true}
        onClick={this.handleSave}
      />
    ];

    return (
      <Dialog
        title={messages.update_status_title}
        actions={actions}
        modal={false}
        open={this.state.open}
        onRequestClose={this.handleCancel}
        contentStyle={{ maxWidth: 540 }}
        titleStyle={{ fontSize: "18px", lineHeight: "28px" }}
      >
        <div style={{ wordWrap: "break-word" }}>
          {messages.update_status_description}
        </div>

        <SelectField
          className={style.select}
          fullWidth={true}
          value={statusSelected}
          onChange={(event, index, value) => {
            this.setState({ statusSelected: value });
          }}
          floatingLabelText={messages.select_status}
        >
          {orderStatuses.map(x => {
            return <MenuItem key={x.id} value={x.id} primaryText={x.name} />;
          })}
        </SelectField>
      </Dialog>
    );
  }
}
