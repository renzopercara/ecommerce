import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Field, reduxForm, formValueSelector } from "redux-form";
import { TextField, SelectField, Checkbox } from "redux-form-material-ui";
import Editor from "modules/shared/editor";

import messages from "lib/text";
import style from "./style.css";

import Paper from "material-ui/Paper";
import FlatButton from "material-ui/FlatButton";
import RaisedButton from "material-ui/RaisedButton";
import MenuItem from "material-ui/MenuItem";

const validate = values => {
  const errors = {};
  const requiredFields = ["name"];

  requiredFields.map(field => {
    if (values && !values[field]) {
      errors[field] = messages.errors_required;
    }
  });

  return errors;
};

const asyncValidate = null;

let ProviderGeneralForm = ({
  handleSubmit,
  pristine,
  reset,
  submitting,
  initialValues,
  shopSource
}) => {
  return (
    <form onSubmit={handleSubmit}>
      <Paper className="paper-box" zDepth={1}>
        <div className={style.innerBox}>
          <Field
            component={SelectField}
            autoWidth={true}
            name="shop_source"
            floatingLabelText={"Tipo de shop"}
          >
            <MenuItem value="tiendanube" primaryText={"Tiendanube"} />
            <MenuItem value="shopify" primaryText={"Shopify"} />
            <MenuItem value="woocommerce" primaryText={"Woocommerce"} />
            <MenuItem value="manual" primaryText={"Manual"} />
          </Field>
          <Field
            name="name"
            component={TextField}
            floatingLabelText={messages.providers_name + " *"}
            fullWidth={true}
          />
          <Field
            name="code"
            component={TextField}
            floatingLabelText={"Código"}
            fullWidth={true}
          />
          {(shopSource === "tiendanube" ||
            shopSource == "manual" ||
            (initialValues &&
              (initialValues.shopSource === "tiendanube" ||
                initialValues.shopSource === "manual"))) && (
            <div>
              <Field
                name="accessToken"
                component={TextField}
                floatingLabelText={"Access Token"}
                fullWidth={true}
              />
              <Field
                name="shopId"
                component={TextField}
                floatingLabelText={"ID del shop"}
                fullWidth={true}
              />
              <Field
                name="userAgent"
                component={TextField}
                floatingLabelText={"Nombre del shop (info@contacto.com)"}
                fullWidth={true}
              />
            </div>
          )}
          {(shopSource === "shopify" ||
            (initialValues && initialValues.shopSource === "shopify")) && (
            <div>
              <Field
                name="shopify_access_token"
                component={TextField}
                floatingLabelText={"Access Token"}
                fullWidth={true}
              />
              <Field
                name="shopify_shop_name"
                component={TextField}
                floatingLabelText={"nombre-del-shop"}
                fullWidth={true}
              />
            </div>
          )}
          {(shopSource === "woocommerce" ||
            (initialValues && initialValues.shopSource === "woocommerce")) && (
            <div>
              <Field
                name="woocommerce_url"
                component={TextField}
                floatingLabelText={"Shop url"}
                fullWidth={true}
              />
              <Field
                name="woocommerce_consumer_key"
                component={TextField}
                floatingLabelText={"Consumer key"}
                fullWidth={true}
              />
              <Field
                name="woocommerce_consumer_secret"
                component={TextField}
                floatingLabelText={"Consumer secret"}
                fullWidth={true}
              />
            </div>
          )}
          <Field
            name="synchronize"
            component={Checkbox}
            label="Sincronizar"
            fullWidth={true}
          />
        </div>
        <div
          className={
            "buttons-box " +
            (pristine ? "buttons-box-pristine" : "buttons-box-show")
          }
        >
          <FlatButton
            label={messages.cancel}
            className={style.button}
            onClick={reset}
            disabled={pristine || submitting}
          />
          <RaisedButton
            type="submit"
            label={messages.save}
            primary={true}
            className={style.button}
            disabled={pristine || submitting}
          />
        </div>
      </Paper>
    </form>
  );
};

ProviderGeneralForm = reduxForm({
  form: "ProviderGeneralForm",
  validate,
  asyncValidate,
  asyncBlurFields: [],
  enableReinitialize: true
})(ProviderGeneralForm);

const selector = formValueSelector("ProviderGeneralForm");

ProviderGeneralForm = connect(state => ({
  shopSource: selector(state, "shop_source")
}))(ProviderGeneralForm);

export default ProviderGeneralForm;
