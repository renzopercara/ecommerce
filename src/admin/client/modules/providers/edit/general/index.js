import { connect } from "react-redux";
import { withRouter } from "react-router";
import { updateProvider, createProvider } from "../../actions";
import ProviderGeneralForm from "./components/form";

const mapStateToProps = (state, ownProps) => {
  return {
    initialValues: state.providers.editProvider
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onSubmit: values => {
      const { providerId } = ownProps.match.params;
      if (providerId) {
        dispatch(updateProvider(providerId, values));
      } else {
        dispatch(createProvider(values));
      }
    }
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ProviderGeneralForm)
);
