import React from "react";
import messages from "lib/text";

import ProviderGeneral from "modules/providers/edit/general";

class ProviderEditContainer extends React.Component {
  componentDidMount() {
    this.props.fetchData();
  }

  componentWillUnmount() {
    this.props.eraseData();
  }

  render() {
    return (
      <div>
        <div style={{ margin: 20, color: "rgba(0, 0, 0, 0.52)" }}>
          {messages.description}
        </div>
        <ProviderGeneral />
      </div>
    );
  }
}

import { connect } from "react-redux";
import { withRouter } from "react-router";
import { fetchProvider, cancelProviderEdit } from "../actions";

const mapStateToProps = (state, ownProps) => {
  return {
    provider: state.providers.editProvider
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    fetchData: () => {
      const { providerId } = ownProps.match.params;
      if (providerId) {
        dispatch(fetchProvider(providerId));
      }
    },
    eraseData: () => {
      dispatch(cancelProviderEdit());
    }
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ProviderEditContainer)
);
