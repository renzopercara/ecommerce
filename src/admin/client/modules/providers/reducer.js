import * as t from "./actionTypes";

const initialState = {
  editProvider: {},
  items: [],
  selected: [],
  hasMore: false,
  totalCount: 0,

  isUpdating: false,
  loadingItems: false,

  errorFetchEdit: null,
  errorLoadingItems: null,
  errorUpdate: null,

  filter: {
    search: "",
    enabled: null,
    discontinued: false,
    onSale: null,
    stockStatus: null
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case t.PROVIDER_DETAIL_REQUEST:
      return Object.assign({}, state, {});
    case t.PROVIDER_DETAIL_RECEIVE:
      return Object.assign({}, state, {
        editProvider: action.item
      });
    case t.PROVIDER_DETAIL_ERASE:
      return Object.assign({}, state, {
        isUpdating: false,
        editProvider: null,
        editProviderImages: null,
        editProviderOptions: null,
        editProviderVariants: null
      });
    case t.PROVIDER_DETAIL_FAILURE:
      return Object.assign({}, state, {
        errorFetchEdit: action.error
      });
    case t.PROVIDERS_REQUEST:
      return Object.assign({}, state, {
        loadingItems: true
      });
    case t.PROVIDERS_RECEIVE:
      return Object.assign({}, state, {
        loadingItems: false,
        hasMore: action.has_more,
        totalCount: action.total_count,
        items: action.data
      });
    case t.PROVIDERS_FAILURE:
      return Object.assign({}, state, {
        errorLoadingItems: action.error
      });
    case t.PROVIDERS_SELECT:
      return Object.assign({}, state, {
        selected: [...state.selected, action.productId]
      });
    case t.PROVIDERS_DESELECT:
      return Object.assign({}, state, {
        selected: state.selected.filter(id => id !== action.productId)
      });
    case t.PROVIDERS_DESELECT_ALL:
      return Object.assign({}, state, {
        selected: []
      });
    case t.PROVIDERS_SELECT_ALL:
      let selected = state.items.map(item => item.id);
      return Object.assign({}, state, {
        selected: selected
      });
    case t.PROVIDERS_SET_FILTER:
      const newFilter = Object.assign({}, state.filter, action.filter);
      return Object.assign({}, state, {
        filter: newFilter
      });
    case t.PROVIDERS_MORE_REQUEST:
      return Object.assign({}, state, {
        loadingItems: true
      });
    case t.PROVIDERS_MORE_RECEIVE:
      return Object.assign({}, state, {
        loadingItems: false,
        hasMore: action.has_more,
        totalCount: action.total_count,
        items: [...state.items, ...action.data]
      });
    case t.PROVIDER_UPDATE_REQUEST:
      return Object.assign({}, state, {
        isUpdating: true
      });
    case t.PROVIDER_UPDATE_FAILURE:
    case t.PROVIDER_UPDATE_SUCCESS:
      return Object.assign({}, state, {
        isUpdating: false,
        editProvider: action.item
      });
    case t.PROVIDER_DELETE_SUCCESS:
    default:
      return state;
  }
};
