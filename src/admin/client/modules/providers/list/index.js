import { connect } from "react-redux";
import { withRouter } from "react-router";
import {
  fetchProviders,
  fetchMoreProviders,
  selectProvider,
  deselectProvider,
  selectAllProvider,
  deselectAllProvider
} from "../actions";
import List from "./components/list";

const mapStateToProps = (state, ownProps) => {
  return {
    settings: state.settings.settings,
    items: state.providers.items || [],
    selected: state.providers.selected,
    loadingItems: state.providers.loadingItems,
    hasMore: state.providers.hasMore,
    totalCount: state.providers.totalCount
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onLoad: () => {
      dispatch(fetchProviders());
    },
    onSelect: event => {
      const providerId = event.target.value;
      const checked = event.target.checked;

      if (checked) {
        dispatch(selectProvider(providerId));
      } else {
        dispatch(deselectProvider(providerId));
      }
    },
    onSelectAll: event => {
      const checked = event.target.checked;

      if (checked) {
        dispatch(selectAllProvider());
      } else {
        dispatch(deselectAllProvider());
      }
    },
    loadMore: () => {
      dispatch(fetchMoreProviders());
    }
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(List)
);
