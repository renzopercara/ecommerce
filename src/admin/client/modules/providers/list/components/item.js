import React from "react";
import { Link } from "react-router-dom";
import Divider from "material-ui/Divider";
import FontIcon from "material-ui/FontIcon";
import messages from "lib/text";
import * as helper from "lib/helper";
import style from "./style.css";

const formatStatus = status => {
  switch (status) {
    case "active":
      return "Activo";
    case "inactive":
      return "Inactivo";
    case "missing_credentials":
      return "Faltan credenciales";
    case "error":
      return "Error de sincronización";
    default:
      return "-";
  }
};

const ItemStock = ({ status, quantity }) => {
  let stockValue = "";
  let stockClass = "";
  switch (status) {
    case "discontinued":
      stockValue = messages.providers_discontinued;
      stockClass = style.discontinued;
      break;
    case "backorder":
      stockValue = messages.providers_backorder;
      stockClass = style.backorder;
      break;
    case "preorder":
      stockValue = messages.providers_preorder;
      stockClass = style.preorder;
      break;
    case "available":
      stockValue = quantity;
      stockClass = style.inStock;
      break;
    case "out_of_stock":
    default:
      stockValue = messages.providers_outOfStock;
      stockClass = style.outOfStock;
      break;
  }

  return <div className={stockClass}>{stockValue}</div>;
};

const ProviderItem = ({ provider, onSelect, selected, settings }) => {
  let providerClass = style.productName;
  if (!provider.synchronize) {
    providerClass += " " + style.providerInactive;
  } else {
    providerClass += " " + style.providerActive;
  }

  const providerName =
    provider.name && provider.name.length > 0
      ? provider.name
      : `<${messages.draft}>`;

  return (
    <div className={"products-item" + (selected === true ? " selected" : "")}>
      <div
        style={{ display: "flex", flexDirection: "row" }}
        className={"row--no-gutter middle-xs " + style.innerItem}
      >
        <div style={{ minWidth: 320 }} className="col-xs-4 col--no-gutter">
          <div className="col-xs-8">
            <Link
              to={"/admin/provider/" + provider._id}
              className={providerClass}
            >
              {providerName}
              <br />
            </Link>
          </div>
        </div>
        <div style={{ minWidth: 167 }} className={"col-xs-2 " + style.sku}>
          {provider.code}
        </div>
        <div style={{ minWidth: 81 }} className={"col-xs-1 " + style.sku}>
          {provider.shop_source}
        </div>
        <div style={{ minWidth: 81 }} className={"col-xs-1 " + style.sku}>
          {provider.synchronize ? "Si" : "No"}
        </div>
        <div style={{ minWidth: 167 }} className={"col-xs-2 " + style.sku}>
          {provider.updatedAt}
        </div>
        <div style={{ minWidth: 167 }} className={"col-xs-2 " + style.sku}>
          {formatStatus(provider.status)}
        </div>
      </div>
      <Divider />
    </div>
  );
};

export default ProviderItem;
