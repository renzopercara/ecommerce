import React from "react";
import Subheader from "material-ui/Subheader";
import messages from "lib/text";

const Head = ({}) => (
  <Subheader>
    <div
      style={{ display: "flex", flexDirection: "row" }}
      className="row--no-gutter middle-xs"
    >
      <div style={{ minWidth: 320 }} className="col-xs-4 col--no-gutter">
        <div className="row row--no-gutter middle-xs">
          <div className="col-xs-8">Nombre del proveedor</div>
        </div>
      </div>
      <div style={{ minWidth: 167 }} className="col-xs-2 col--no-gutter">
        Código
      </div>
      <div style={{ minWidth: 81 }} className="col-xs-1 col--no-gutter">
        Plataforma
      </div>
      <div style={{ minWidth: 81 }} className="col-xs-1 col--no-gutter">
        Sincronizado
      </div>
      <div style={{ minWidth: 167 }} className="col-xs-2 col--no-gutter">
        Fecha últ. actualización
      </div>
      <div style={{ minWidth: 167 }} className="col-xs-2 col--no-gutter">
        Estado
      </div>
    </div>
  </Subheader>
);

export default Head;
