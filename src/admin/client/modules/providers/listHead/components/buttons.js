import React from "react";
import { Link } from "react-router-dom";
import messages from "lib/text";
import FontIcon from "material-ui/FontIcon";
import IconButton from "material-ui/IconButton";
const Fragment = React.Fragment;

export default class Buttons extends React.Component {
  render() {
    const { onCreate } = this.props;

    return (
      <Fragment>
        <IconButton
          touch={true}
          tooltipPosition="bottom-left"
          tooltip={messages.addProvider}
          onClick={() => this.props.history.push("/admin/provider")}
        >
          <FontIcon color="#fff" className="material-icons">
            add
          </FontIcon>
        </IconButton>
      </Fragment>
    );
  }
}
