import * as t from "./actionTypes";
import {
  getProviders,
  doUpdateProvider,
  doCreateProvider,
  getProviderById
} from "../../api/providers";
import messages from "lib/text";

function requestProvider() {
  return {
    type: t.PROVIDER_DETAIL_REQUEST
  };
}

function parseProvider(item) {
  return {
    name: item.name,
    code: item.code,
    accessToken: item.access_token,
    shopId: item.shop_id,
    userAgent: item.user_agent,
    shop_source: item.shop_source,
    synchronize: item.synchronize,
    shopify_access_token: item.shopify_access_token,
    shopify_shop_name: item.shopify_shop_name,
    woocommerce_url: item.woocommerce_url,
    woocommerce_consumer_key: item.woocommerce_consumer_key,
    woocommerce_consumer_secret: item.woocommerce_consumer_secret
  };
}

function receiveProvider(item) {
  return {
    type: t.PROVIDER_DETAIL_RECEIVE,
    item: parseProvider(item)
  };
}

function receiveProviderError(error) {
  return {
    type: t.PROVIDER_DETAIL_FAILURE,
    error
  };
}

export function cancelProviderEdit() {
  return {
    type: t.PROVIDER_DETAIL_ERASE
  };
}

function requestProviders() {
  return {
    type: t.PROVIDERS_REQUEST
  };
}

function requestMoreProviders() {
  return {
    type: t.PROVIDERS_MORE_REQUEST
  };
}

function receiveProvidersMore({ has_more, total_count, data }) {
  return {
    type: t.PROVIDERS_MORE_RECEIVE,
    has_more,
    total_count,
    data
  };
}

function receiveProviders({ has_more, total_count, data }) {
  return {
    type: t.PROVIDERS_RECEIVE,
    has_more,
    total_count,
    data
  };
}

function receiveProvidersError(error) {
  return {
    type: t.PROVIDERS_FAILURE,
    error
  };
}

export function selectProvider(id) {
  return {
    type: t.PROVIDERS_SELECT,
    providerId: id
  };
}

export function deselectProvider(id) {
  return {
    type: t.PROVIDERS_DESELECT,
    providerId: id
  };
}

export function deselectAllProvider() {
  return {
    type: t.PROVIDERS_DESELECT_ALL
  };
}

export function selectAllProvider() {
  return {
    type: t.PROVIDERS_SELECT_ALL
  };
}

function requestUpdateProvider() {
  return {
    type: t.PROVIDER_UPDATE_REQUEST
  };
}

function receiveUpdateProvider(item) {
  return {
    type: t.PROVIDER_UPDATE_SUCCESS,
    item: parseProvider(item)
  };
}

function errorUpdateProvider(error) {
  return {
    type: t.PROVIDER_UPDATE_FAILURE,
    error
  };
}

export function createProvider(data) {
  return (dispatch, getState) => {
    dispatch(requestUpdateProvider());
    return doCreateProvider(data)
      .then(json => {
        dispatch(receiveUpdateProvider(json.data));
      })
      .catch(err => {
        dispatch(errorUpdateProvider(err));
      });
  };
}

export function updateProvider(id, data) {
  return (dispatch, getState) => {
    dispatch(requestUpdateProvider());
    return doUpdateProvider(id, data)
      .then(json => {
        dispatch(receiveUpdateProvider(json.data));
      })
      .catch(err => {
        dispatch(errorUpdateProvider(err));
      });
  };
}

export function fetchProvider(id) {
  return (dispatch, getState) => {
    const state = getState();
    if (state.providers.loadingItems) {
      // do nothing
    } else {
      dispatch(requestProvider());

      return getProviderById(id)
        .then(json => {
          dispatch(receiveProvider(json.data));
        })
        .catch(error => {
          dispatch(receiveProviderError(error));
        });
    }
  };
}

export function fetchProviders() {
  return (dispatch, getState) => {
    const state = getState();
    if (state.providers.loadingItems) {
      // do nothing
    } else {
      dispatch(requestProviders());
      dispatch(deselectAllProvider());

      // let filter = getFilter(state);

      return getProviders()
        .then(json => {
          dispatch(receiveProviders(json));
        })
        .catch(error => {
          dispatch(receiveProvidersError(error));
        });
    }
  };
}

export function fetchMoreProviders() {
  return (dispatch, getState) => {
    const state = getState();
    if (!state.providers.loadingItems) {
      dispatch(requestMoreProviders());

      const offset = state.providers.items.length;
      // let filter = getFilter(state, offset);

      return getMoreProviders()
        .then(({ status, json }) => {
          dispatch(receiveProvidersMore(json));
        })
        .catch(error => {
          dispatch(receiveProvidersError(error));
        });
    }
  };
}
