import { doDeleteCredential, doGetCredentials } from "../../api/credentials";

function receiveCredentials(items) {
  return {
    type: "CREDENTIALS_RECEIVED",
    items
  };
}

function requestCredentials() {
  return {
    type: "CREDENTIALS_REQUEST"
  };
}

function failureCredentials() {
  return {
    type: "CREDENTIALS_FAILURE"
  };
}

function requestDeleteCredentials() {
  return {
    type: "DELETE_CREDENTIALS_REQUEST"
  };
}

function receiveDeleteCredentials(items) {
  return {
    type: "DELETE_CREDENTIALS_RECEIVE",
    items
  };
}

function failureDeleteCredentials() {
  return {
    type: "DELETE_CREDENTIALS_FAILURE"
  };
}

export function deleteCredential(id) {
  return dispatch => {
    dispatch(requestDeleteCredentials());
    return doDeleteCredential(id)
      .then(credentials => dispatch(receiveDeleteCredentials(credentials)))
      .catch(() => dispatch(failureDeleteCredentials()));
  };
}

export function getCredentials() {
  return dispatch => {
    dispatch(requestCredentials);
    return doGetCredentials()
      .then(credentials => dispatch(receiveCredentials(credentials)))
      .catch(() => dispatch(failureCredentials()));
  };
}
