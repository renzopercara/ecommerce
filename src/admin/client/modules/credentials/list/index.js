import React from "react";
import RaisedButton from "material-ui/RaisedButton";
import FontIcon from "material-ui/FontIcon";
import { List, ListItem } from "material-ui/List";

import {
  mercadoPagoClientId,
  mercadoPagoRedirectURI
} from "../../../../../../config/server";
export default class Credentials extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.onLoad();
  }

  renderMercadoPago = () => {
    const { credentials } = this.props;
    const mercadoPagoCredential = credentials[0];
    if (mercadoPagoCredential && mercadoPagoCredential.access_token) {
      return (
        <div>
          <p>access_token: {mercadoPagoCredential.access_token}</p>
          <RaisedButton
            disabled={!mercadoPagoCredential}
            label={"Eliminar credencial"}
            labelPosition="before"
            primary={true}
            icon={<FontIcon className="material-icons">delete</FontIcon>}
            onClick={() => this.props.onDelete(mercadoPagoCredential._id)}
          />
        </div>
      );
    }
    if (mercadoPagoClientId && mercadoPagoRedirectURI) {
      const authorizeUrl =
        "https://auth.mercadopago.com.ar/authorization?client_id=" +
        mercadoPagoClientId +
        "&response_type=code&platform_id=mp&redirect_uri=" +
        mercadoPagoRedirectURI;
      return <a href={authorizeUrl}>Autorizar mercado pago</a>;
    }
    return <b>Debe configurar el clientId y redirectURI de la aplicación</b>;
  };

  render() {
    return <div>{this.renderMercadoPago()}</div>;
  }
}
