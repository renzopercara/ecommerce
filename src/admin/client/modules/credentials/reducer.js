const initialState = {
  items: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "CREDENTIALS_REQUEST":
      return Object.assign({}, state, {});
    case "CREDENTIALS_RECEIVED":
      return Object.assign({}, state, { items: action.items });
    case "CREDENTIALS_FAILURE":
      return Object.assign({}, state, {});
    case "DELETE_CREDENTIALS_REQUEST":
      return Object.assign({}, state, {});
    case "DELETE_CREDENTIALS_RECEIVE":
      return Object.assign({}, state, { items: action.items });
    case "DELETE_CREDENTIALS_FAILURE":
      return Object.assign({}, state, {});
    default:
      return Object.assign({}, state, {});
  }
};
