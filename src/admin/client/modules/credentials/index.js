import { connect } from "react-redux";
import { getCredentials, deleteCredential } from "./actions";
import List from "./list";

const mapStateToProps = state => {
  return {
    credentials: state.credentials.items
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onLoad: () => {
      dispatch(getCredentials());
    },
    onDelete: id => {
      dispatch(deleteCredential(id));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(List);
