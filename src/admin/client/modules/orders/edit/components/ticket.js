import React from "react";
import { Link } from "react-router-dom";
import moment from "moment";
import messages from "lib/text";
import Paper from "material-ui/Paper";
import RaisedButton from "material-ui/RaisedButton";
import styles from "./ticket.css";
import ReactToPrint from "react-to-print";
import axios from "axios";
import config from "./../../../../../../../config/admin";

const ProviderTicketInfo = ({ products }) => {
  return (
    <div>
      <div className={`${styles.spaceBetween} ${styles.mb5}`}>
        <span className={`${styles.bold} ${styles.txtSmall}`}>Cantidad</span>
        <span className={`${styles.bold} ${styles.txtSmall}`}>Producto</span>
      </div>
      {products}
      <div className={styles.separator} />
    </div>
  );
};

const AdminTicketInfo = ({ products, order, unit_subtotal, subtotal }) => {
  let productsCount = 0;
  order.items.forEach(item => {
    console.log(item);
    productsCount += item.quantity;
  });
  return (
    <div>
      <div className={`${styles.spaceBetween} ${styles.mb5}`}>
        <span className={`${styles.bold} ${styles.txtSmall}`}>Cantidad</span>
        <span className={`${styles.bold} ${styles.txtSmall}`}>Producto</span>
        <span className={`${styles.bold} ${styles.txtSmall}`}>P. Unit</span>
        <span className={`${styles.bold} ${styles.txtSmall}`}>Total</span>
      </div>

      {products}

      <div className={styles.separatorLight} />

      <div className={`${styles.spaceBetween} ${styles.mb5}`}>
        <div className={styles.row}>
          <span className={styles.txtSmall}>Subtotal </span>
          <span className={`${styles.bold} ${styles.txtSmall}`}>
            ({productsCount} productos):
          </span>
        </div>
        <span>${subtotal}</span>
      </div>

      <div className={`${styles.spaceBetween} ${styles.mb5}`}>
        <span className={styles.txtSmall}>Costo de envio:</span>
        <span>${order.shipping_price}</span>
      </div>

      <div className={`${styles.spaceBetween} ${styles.mb5}`}>
        <span className={`${styles.txtBig} ${styles.bold}`}>Total:</span>
        <span className={`${styles.txtBig} ${styles.bold}`}>
          ${subtotal + order.shipping_price}
        </span>
      </div>

      <div className={styles.separator} />
      <div className={`${styles.spaceBetween} ${styles.mb5}`}>
        <span className={`${styles.bold} ${styles.txtSmall}`}>Dirección:</span>
        <span>
          {" "}
          {(order.shipping_address && order.shipping_address.address1) ||
            ""}{" "}
          {(order.shipping_address && order.shipping_address.address2) || ""}{" "}
          {(order.shipping_address && order.shipping_address.city) || ""}{" "}
          {order.shipping_address && order.shipping_address.postal_code
            ? "CP" + order.shipping_address.postal_code
            : ""}{" "}
          {(order.shipping_address && order.shipping_address.state) || ""}{" "}
          {(order.shipping_address && order.shipping_address.country) || ""}
        </span>
      </div>
      <div className={`${styles.spaceBetween} ${styles.mb5}`}>
        <span className={`${styles.bold} ${styles.txtSmall}`}>Cliente:</span>
        <span>
          {" "}
          {order.full_name || ""}{" "}
          {(order.documento.tipo && order.documento.tipo.toUpperCase()) || ""}{" "}
          {order.documento.numero || ""}
        </span>
      </div>
      <div className={styles.separator} />

      <div style={{ marginBottom: 16 }}>
        <span className={styles.txtSmall}>Medio de pago: </span>
        <span className={`${styles.bold} ${styles.txtSmall}`}>
          {order.payment_method}
        </span>
      </div>
    </div>
  );
};

class Ticket extends React.Component {
  state = {
    products: []
  };

  componentDidMount() {
    const { order } = this.props;
    this.setState({ products: order.items });
  }

  render() {
    const { order, user } = this.props;
    const { products } = this.state;
    const date_created = moment(order.date_placed).format("DD/MM/YY");
    let subtotal = order.shipping_price;
    let unit_subtotal = 0;
    const productos = products.map(x => {
      subtotal += x.price_total;
      unit_subtotal += x.price;

      return (
        <div key={x.product_id} className={styles.producto}>
          <span className={styles.quantityWidth}>{x.quantity}</span>
          <span
            className={
              user.role === "provider"
                ? styles.providerTicket
                : styles.maxWidth70
            }
          >
            {x.name} / {x.variant && x.variant_name}
          </span>
          {user.role === "superadmin" && (
            <span style={{ textAlign: "center" }}>${x.price} </span>
          )}
          {user.role === "superadmin" && (
            <span style={{ textAlign: "center" }}>${x.price_total} </span>
          )}
        </div>
      );
    });

    return (
      <div className={`${styles.container} ${styles.column}`}>
        <span
          className={`${styles.bold} ${styles.txtBig}`}
          style={{ marginTop: 16 }}
        >
          Orden: #{order.number}
        </span>
        <span className={`${styles.mt5} ${styles.txtRegular}`}>
          Realizada el: {date_created}
        </span>

        <img className={styles.logo} src="/logo.png" alt="logo" />
        <div className={styles.separator} />

        {user.role === "superadmin" ? (
          <AdminTicketInfo
            products={productos}
            unit_subtotal={unit_subtotal}
            subtotal={subtotal}
            order={order}
          />
        ) : (
          <ProviderTicketInfo products={productos} />
        )}
      </div>
    );
  }
}

export default class OrderTicket extends React.Component {
  render() {
    const { order, user } = this.props;

    return (
      <div>
        <div style={{ margin: 20, color: "rgba(0, 0, 0, 0.52)" }}>
          {messages.ticket}
        </div>

        <Paper className="paper-box" zDepth={1}>
          <ReactToPrint
            trigger={() => (
              <RaisedButton
                label={messages.print}
                style={{ marginRight: 15, margin: 16 }}
                onClick={() => alert("a")}
              />
            )}
            bodyClass={styles.printContainer}
            content={() => this.ticket}
          />
          <Ticket ref={x => (this.ticket = x)} order={order} user={user} />
        </Paper>
      </div>
    );
  }
}
