import React from "react";

import messages from "lib/text";
import * as helper from "lib/helper";
import style from "./style.css";

import FlatButton from "material-ui/FlatButton";
import RaisedButton from "material-ui/RaisedButton";
import Divider from "material-ui/Divider";

const ProvidersTotals = ({ providers }) => {
  return (
    <div>
      {providers &&
        providers.map(item => (
          <div className={style.total + " row " + style.grandTotal}>
            <div className="col-xs-7">Costo {item.name}</div>
            <div className="col-xs-5">{item.total_cost}</div>
          </div>
        ))}
    </div>
  );
};

const OrderTotals = ({ order, settings, user }) => {
  const discountTotal = helper.formatCurrency(order.discount_total, settings);
  // const subtotal = helper.formatCurrency(order.subtotal, settings);
  const taxIncludedTotal = helper.formatCurrency(
    order.tax_included_total,
    settings
  );
  const taxTotal = helper.formatCurrency(order.tax_total, settings);
  const shippingTotal = helper.formatCurrency(order.shipping_total, settings);
  const grandTotal =
    user.role === "provider"
      ? helper.formatCurrency(order.cost_total, settings)
      : helper.formatCurrency(order.grand_total, settings);
  const profit = helper.formatCurrency(
    order.grand_total - order.cost_total,
    settings
  );
  const costTotal = helper.formatCurrency(order.cost_total, settings);
  const itemTax = helper.formatCurrency(order.item_tax, settings);
  const shippingTax = helper.formatCurrency(order.shipping_tax, settings);
  const shippingDiscount = helper.formatCurrency(
    order.shipping_discount,
    settings
  );
  const shippingPrice = helper.formatCurrency(order.shipping_price, settings);
  let discountsDescription =
    order.coupon && order.coupon.length > 0
      ? ` (${messages.coupon}: ${order.coupon})`
      : "";

  let transactionsTotal = 0;
  for (const transaction of order.transactions) {
    if (transaction.success === true) {
      transactionsTotal += transaction.amount;
    }
  }
  const paidTotal = helper.formatCurrency(transactionsTotal, settings);

  return (
    <div>
      {user.role === "superadmin" && (
        <div className={style.total + " row"}>
          <div className="col-xs-7">
            <span>Costo total</span>
          </div>
          <div className="col-xs-5">- {costTotal}</div>
        </div>
      )}
      <div className={style.total + " row"}>
        <div className="col-xs-7">
          <span>{messages.orderShipping}</span>
        </div>
        <div className="col-xs-5">{shippingTotal}</div>
      </div>
      <div className={style.total + " row"}>
        <div className="col-xs-7">
          <span>{messages.orderTax}</span>
        </div>
        <div className="col-xs-5">{taxIncludedTotal}</div>
      </div>
      <div className={style.total + " row"}>
        <div className="col-xs-7">
          <span>
            {messages.orderDiscount}
            {discountsDescription}
          </span>
        </div>
        <div className="col-xs-5">{discountTotal}</div>
      </div>
      <div className={style.total + " row " + style.grandTotal}>
        <div className="col-xs-7">{messages.grandTotal}</div>
        <div className="col-xs-5">{grandTotal}</div>
      </div>
      {user.role === "superadmin" && (
        <div className={style.total + " row " + style.grandTotal}>
          <div className="col-xs-7">Ganancia</div>
          <div className="col-xs-5" style={{ color: "green" }}>
            {profit}
          </div>
        </div>
      )}
      {user.role === "superadmin" && (
        <ProvidersTotals providers={order.providers} />
      )}
      <Divider
        style={{
          marginTop: 20,
          marginBottom: 20
        }}
      />

      {user.role === "client" && (
        <div className={style.total + " row"}>
          <div className="col-xs-7">
            <span>{messages.amountPaid}</span>
          </div>
          <div className="col-xs-5">{paidTotal}</div>
          {order.payment_url && (
            <a href={order.payment_url} target="_blank">
              <RaisedButton label="PAGAR" />
            </a>
          )}
        </div>
      )}
    </div>
  );
};

export default OrderTotals;
