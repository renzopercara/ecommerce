import React from "react";
import { Link } from "react-router-dom";

import messages from "lib/text";
import * as helper from "lib/helper";
import style from "./style.css";

import Paper from "material-ui/Paper";
import Divider from "material-ui/Divider";
import IconButton from "material-ui/IconButton";
import FlatButton from "material-ui/FlatButton";
import FontIcon from "material-ui/FontIcon";
import IconMenu from "material-ui/IconMenu";
import MenuItem from "material-ui/MenuItem";
import SelectField from "material-ui/SelectField";
import CheckboxField from "material-ui/Checkbox";
import Dialog from "material-ui/Dialog";
import TextField from "material-ui/TextField";

const iconButtonElement = (
  <IconButton touch={true}>
    <FontIcon color="rgb(189, 189, 189)" className="material-icons">
      more_vert
    </FontIcon>
  </IconButton>
);

const ProductOption = ({ option, onChange, selectedOptions }) => {
  const selectedValue = selectedOptions[option.id];
  const values = option.values
    .sort((a, b) => (a.name > b.name ? 1 : b.name > a.name ? -1 : 0))
    .map((value, index) => (
      <MenuItem key={index} value={value.id} primaryText={value.name} />
    ));

  return (
    <SelectField
      floatingLabelText={option.name}
      fullWidth={true}
      value={selectedValue}
      onChange={(event, index, value) => {
        onChange(option.id, value);
      }}
    >
      {values}
    </SelectField>
  );
};

const ConfirmProductOption = ({ value, onChange }) => {
  console.log("value confirm product" + value);
  return (
    <div>
      <span>Confirmar</span>
      <CheckboxField
        value="OK"
        checked={value === "OK" || value === "OK2"}
        onCheck={(event, checked) => {
          const newValue = event.target.checked ? "OK" : null;
          onChange(newValue);
        }}
        inputProps={{ "aria-label": "Confirmado" }}
      />
    </div>
  );
};

const ItemCost = ({ cost, costTotal, quantity, style, discountTotal }) => {
  return (
    <div className="row row--no-gutter middle-xs">
      <div className="col-xs-2" style={{ textAlign: "right" }}>
        {cost}
      </div>
      <div className="col-xs-2" style={{ textAlign: "center" }}>
        x {quantity}
      </div>
      <div className="col-xs-2" style={{ textAlign: "right" }}>
        {costTotal}
        {discountTotal > 0 && <small className={style}>{discountTotal}</small>}
      </div>
    </div>
  );
};

const ItemPrice = ({ price, priceTotal, quantity }) => {
  return (
    <div className="row row--no-gutter middle-xs">
      <div className="col-xs-2" style={{ textAlign: "right" }}>
        {price}
      </div>
      <div className="col-xs-2" style={{ textAlign: "center" }}>
        x {quantity}
      </div>
      <div className="col-xs-2" style={{ textAlign: "right" }}>
        {priceTotal}
      </div>
    </div>
  );
};

const ItemPriceCost = ({
  profit,
  price,
  priceTotal,
  cost,
  costTotal,
  quantity,
  discountTotal,
  style
}) => {
  return (
    <div>
      <div className={style.total + " row"} style={{ margin: 5 }}>
        <div className="col-xs-7">
          <span>Precio unitario</span>
        </div>
        <div className="col-xs-5">{price}</div>
      </div>
      <div className={style.total + " row"} style={{ margin: 5 }}>
        <div className="col-xs-7">
          <span>Costo unitario</span>
        </div>
        <div className="col-xs-5" style={{ color: "red" }}>
          - {cost}
        </div>
      </div>
      <div
        className={style.total + " row"}
        style={{ margin: 5, fontWeight: "bold" }}
      >
        <div className="col-xs-7">
          <span>Precio Total</span>
        </div>
        <div className="col-xs-5">{priceTotal}</div>
      </div>
      <div className={style.total + " row"} style={{ margin: 5 }}>
        <div className="col-xs-7">
          <span>Cantidad</span>
        </div>
        <div className="col-xs-5">{quantity}</div>
      </div>
      <div className={style.total + " row"} style={{ margin: 5 }}>
        <div className="col-xs-7">
          <span>Costo Total</span>
        </div>
        <div className="col-xs-5" style={{ color: "red" }}>
          - {costTotal}
        </div>
      </div>
      <div className={style.total + " row"} style={{ margin: 5 }}>
        <div className="col-xs-7">
          <span>Descuento Total</span>
        </div>
        <div className="col-xs-5">{discountTotal}</div>
      </div>
      <div className={style.total + " row"} style={{ margin: 5 }}>
        <div className="col-xs-7">
          <span style={{ fontWeight: "bold" }}>Ganancia</span>
        </div>
        <div
          className="col-xs-5"
          style={{ fontWeight: "bold", color: "green" }}
        >
          {profit}
        </div>
      </div>
    </div>
  );
};

const ProductOptions = ({ options, onChange, selectedOptions }) => {
  if (options) {
    const items = options.map((option, index) => (
      <ProductOption
        key={index}
        option={option}
        onChange={onChange}
        selectedOptions={selectedOptions}
      />
    ));
    return <div className="product-options">{items}</div>;
  } else {
    return null;
  }
};

export class OrderItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity: props.item.quantity,
      variantId: props.item.variant_id,
      observations: props.item.observations || "",
      provider_status: props.item.provider_status || "",
      selectedOptions: this.getOptionsByVariant(),
      selectedVariant: this.getCurrentVariant(),
      showEdit: false
    };
  }

  showEditForm = () => {
    this.setState({ showEdit: true });
  };

  hideEditForm = () => {
    this.setState({ showEdit: false });
  };

  quantityChange = (event, index, value) => {
    this.setState({ quantity: value });
  };

  observationsChange = event => {
    this.setState({ observations: event.target.value });
  };

  provider_statusChange = (event, index, value) => {
    this.setState({ provider_status: value });
  };

  confirmProduct = value => {
    console.log("entro aca ", value);
    this.setState({ provider_status: value }, () => this.submitEditForm());
  };

  submitEditForm = () => {
    this.hideEditForm();
    const newVariantId =
      this.state.selectedVariant && this.state.selectedVariant.id
        ? this.state.selectedVariant.id
        : this.state.variantId;
    this.props.onItemUpdate(
      this.props.item.id,
      this.state.quantity,
      newVariantId,
      this.state.provider_status,
      this.state.observations
    );
  };

  deleteItem = () => {
    this.props.onItemDelete(this.props.item.id);
  };

  getStatusColor = status => {
    switch (status) {
      case "NT":
        return "red";
      case "NG":
        return "red";
      case "NC":
        return "red";
      case "OK":
        return "green";
      case "OK2":
        return "green";
      default:
        return "orange";
    }
  };

  getStatusDescription = status => {
    switch (status) {
      case "NT":
        return "Sin stock para el talle";
      case "NG":
        return "Sin stock general";
      case "NC":
        return "Sin stock para el color";
      case "OK":
        return "Confirmado";
      case "OK2":
        return "Confirmado y pagado";
      default:
        return "Pendiente de confirmación";
    }
  };

  onOptionChange = (optionId, valueId) => {
    this.setState({ quantity: 1 });
    let { selectedOptions } = this.state;

    if (valueId === "") {
      delete selectedOptions[optionId];
    } else {
      selectedOptions[optionId] = valueId;
    }

    this.setState({ selectedOptions: selectedOptions });
    this.findVariantBySelectedOptions();
  };

  findVariantBySelectedOptions = () => {
    const { selectedOptions } = this.state;
    const product = this.props.item.product;
    for (const variant of product.variants) {
      const variantMutchSelectedOptions = variant.options.every(
        variantOption =>
          selectedOptions[variantOption.option_id] === variantOption.value_id
      );
      if (variantMutchSelectedOptions) {
        this.setState({ selectedVariant: variant });
        return;
      }
    }

    this.setState({ selectedVariant: null });
  };

  getCurrentVariant = () => {
    const variantId = this.props.item.variant_id;
    const product = this.props.item.product;
    let variant = null;

    if (
      variantId &&
      product &&
      product.variants &&
      product.variants.length > 0
    ) {
      variant = product.variants.find(v => v.id === variantId);
    }

    return variant;
  };

  getOptionsByVariant = () => {
    const variantId = this.props.item.variant_id;
    const product = this.props.item.product;
    let selectedOptions = {};
    if (
      variantId &&
      product &&
      product.variants &&
      product.variants.length > 0
    ) {
      const variant = product.variants.find(v => v.id === variantId);
      if (variant) {
        for (const option of variant.options) {
          selectedOptions[option.option_id] = option.value_id;
        }
      }
    }

    return selectedOptions;
  };

  render() {
    const { item, settings, allowEdit, user } = this.props;

    const editFormActions = [
      <FlatButton
        label={messages.cancel}
        onClick={this.hideEditForm}
        style={{ marginRight: 10 }}
      />,
      <FlatButton
        label={messages.save}
        primary={true}
        onClick={this.submitEditForm}
      />
    ];

    let { quantity } = this.state;
    let { observations } = this.state;
    let { provider_status } = this.state;
    const { selectedOptions, selectedVariant } = this.state;
    const product = item.product;
    const profit = helper.formatCurrency(
      item.price_total - item.cost_total,
      settings
    );
    const price = helper.formatCurrency(item.price, settings);
    const priceTotal = helper.formatCurrency(item.price_total, settings);
    const cost = helper.formatCurrency(item.cost_price, settings);
    const costTotal = helper.formatCurrency(item.cost_total, settings);
    const discountTotal = helper.formatCurrency(item.discount_total, settings);
    const imageUrl =
      product && product.images.length > 0 ? product.images[0].url : null;
    const thumbnailUrl = helper.getThumbnailUrl(imageUrl, 100);

    let maxItems = product ? product.stock_quantity : 0;
    if (selectedVariant) {
      maxItems = selectedVariant.stock_quantity;
    } else if (product && product.options && product.options.length > 0) {
      // product variant not exists with this options
      maxItems = 0;
    }

    const quantityItems = [];
    if (maxItems === 0) {
      quantityItems.push(
        <MenuItem
          key={0}
          value={0}
          primaryText={messages.products_outOfStock}
        />
      );
      quantity = 0;
    } else {
      for (let i = 1; i <= maxItems, i <= 100; i++) {
        quantityItems.push(
          <MenuItem key={i} value={i} primaryText={i.toString()} />
        );
      }
    }

    return (
      <div>
        <div className={style.item + " row row--no-gutter middle-xs"}>
          <div className="col-xs-2">
            {thumbnailUrl && thumbnailUrl !== "" && (
              <img src={thumbnailUrl} className={style.itemImage} />
            )}
          </div>
          <div className={style.itemName + " col-xs-4"}>
            <Link to={`/admin/product/${item.product_id}`}>{item.name}</Link>
            <div>{item.variant_name}</div>
            {item.sku && (
              <div>
                {messages.products_sku}: {item.sku}
              </div>
            )}
            {user.role !== "client" && (
              <div>
                Estado del proveedor:{" "}
                <span
                  style={{ color: this.getStatusColor(item.provider_status) }}
                >
                  {" "}
                  {this.getStatusDescription(item.provider_status)}
                </span>
              </div>
            )}
            {user.role !== "client" && (
              <div>Proveedor: {item.provider_username}</div>
            )}
            {user.role !== "client" && (
              <ConfirmProductOption
                value={item.provider_status}
                onChange={this.confirmProduct}
              />
            )}
          </div>
          <div className="col-xs-6">
            {user && user.role === "client" && (
              <ItemPrice
                price={price}
                priceTotal={priceTotal}
                quantity={item.quantity}
              />
            )}
            {user && user.role === "provider" && (
              <ItemCost
                quantity={item.quantity}
                cost={cost}
                costTotal={costTotal}
                discountTotal={discountTotal}
                style={style.itemDiscount}
              />
            )}
            {user && user.role === "superadmin" && (
              <ItemPriceCost
                quantity={item.quantity}
                price={price}
                priceTotal={priceTotal}
                cost={cost}
                costTotal={costTotal}
                discountTotal={discountTotal}
                profit={profit}
                style={style.itemDiscount}
              />
            )}
          </div>
          {allowEdit && (
            <div>
              <div className="col-xs-1" style={{ textAlign: "center" }}>
                <IconMenu
                  iconButtonElement={iconButtonElement}
                  targetOrigin={{ horizontal: "right", vertical: "top" }}
                  anchorOrigin={{ horizontal: "right", vertical: "top" }}
                >
                  <MenuItem onClick={this.showEditForm}>
                    {messages.edit}
                  </MenuItem>
                </IconMenu>
              </div>
            </div>
          )}
        </div>
        <Divider />
        <Dialog
          title={messages.editOrderItem}
          actions={editFormActions}
          modal={false}
          open={this.state.showEdit}
          onRequestClose={this.hideEditForm}
          contentStyle={{ width: 400 }}
        >
          <div>
            {/*<SelectField
              floatingLabelText={messages.quantity}
              fullWidth={true}
              value={quantity}
              onChange={this.quantityChange}
            >
              {quantityItems}
            </SelectField>*/}
            <SelectField
              floatingLabelText="Estado"
              fullWidth={true}
              value={provider_status}
              onChange={this.provider_statusChange}
            >
              <MenuItem
                key="OK2"
                value="OK2"
                primaryText={this.getStatusDescription("OK2")}
              />
              <MenuItem
                key="OK"
                value="OK"
                primaryText={this.getStatusDescription("OK")}
              />
              <MenuItem
                key="NT"
                value="NT"
                primaryText={this.getStatusDescription("NT")}
              />
              <MenuItem
                key="NC"
                value="NC"
                primaryText={this.getStatusDescription("NC")}
              />
              <MenuItem
                key="NG"
                value="NG"
                primaryText={this.getStatusDescription("NG")}
              />
            </SelectField>
            <TextField
              type="text"
              value={observations}
              placeHolder="Observaciones"
              onChange={this.observationsChange}
              label="Observaciones"
              fullWidth={true}
            />
          </div>
        </Dialog>
      </div>
    );
  }
}

const OrderItems = ({ order, settings, onItemDelete, onItemUpdate, user }) => {
  const allowEdit =
    order.closed === false &&
    order.cancelled === false &&
    user.role !== "client";
  const items = order.items.map((item, index) => (
    <OrderItem
      user={user}
      key={index}
      item={item}
      settings={settings}
      onItemDelete={onItemDelete}
      onItemUpdate={onItemUpdate}
      allowEdit={allowEdit}
    />
  ));
  return <div>{items}</div>;
};

export default OrderItems;
