import React from "react";
import Subheader from "material-ui/Subheader";
import Checkbox from "material-ui/Checkbox";
import messages from "lib/text";

export default ({ onSelectAll, user }) => (
  <Subheader style={{ paddingRight: 16 }}>
    <div
      style={{ display: "flex", flexDirection: "row" }}
      className="middle-xs"
    >
      {user.role === "superadmin" && (
        <div style={{ minWidth: 81 }} className="col-xs-1">
          <Checkbox
            onCheck={(event, isInputChecked) => {
              onSelectAll(isInputChecked);
            }}
          />
        </div>
      )}

      <div style={{ minWidth: 81 }} className="col-xs-1" />
      <div style={{ minWidth: 81 }} className="col-xs-2">
        {messages.order}
      </div>
      <div style={{ minWidth: 81 }} className="col-xs-2">
        {messages.orders_shippingTo}
      </div>
      <div
        style={{ minWidth: 81 }}
        className="col-xs-2"
        style={{ textAlign: "right" }}
      >
        {messages.orders_total}
      </div>
      <div
        className="col-xs-2"
        style={{ minWidth: 81, textAlign: "right", paddingRight: 16 }}
      >
        {messages.orders_status}
      </div>
      {user.role !== "client" && (
        <div
          className="col-xs-2"
          style={{ minWidth: 81, textAlign: "right", paddingRight: 16 }}
        >
          {messages.orders_provider_status}
        </div>
      )}
    </div>
  </Subheader>
);
