import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Head from "modules/head";
import Login from "routes/login";
import Logout from "routes/logout";
import Home from "routes/home";
import NotFound from "routes/notFound";
import Products from "routes/products";
import ProductDetails from "routes/products/edit";
import ProductCategories from "routes/products/categories";
import Customers from "routes/customers";
import CustomerDetails from "routes/customers/edit";
import CustomerGroups from "routes/customers/groups";
import Orders from "routes/orders";
import OrderDetails from "routes/orders/edit";
import OrderStatuses from "routes/orders/statuses";
import Pages from "routes/pages";
import PagesDetails from "routes/pages/edit";
import Settings from "routes/settings";
import Apps from "routes/apps";
import Files from "routes/files";
import Providers from "routes/providers";
import ProviderDetails from "routes/providers/edit";
import Discounts from "routes/discounts";
import DiscountDetails from "routes/discounts/edit";
import Credentials from "routes/credentials";
import OAuthCallback from "routes/oauth";

import PrivateRoute from "./layouts/PrivateRoute";

import {
  blue700,
  cyan700,
  pinkA200,
  grey100,
  grey300,
  grey400,
  white,
  darkBlack,
  fullBlack
} from "material-ui/styles/colors";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import getMuiTheme from "material-ui/styles/getMuiTheme";

const muiTheme = getMuiTheme({
  fontFamily: "Roboto, sans-serif",
  palette: {
    primary1Color: blue700,
    primary2Color: cyan700,
    primary3Color: grey400,
    accent1Color: pinkA200,
    accent2Color: grey100,
    accent3Color: blue700,
    textColor: darkBlack,
    alternateTextColor: white,
    canvasColor: white,
    borderColor: grey300,
    pickerHeaderColor: blue700,
    shadowColor: fullBlack
  },
  appBar: {}
});

export default () => (
  <Router>
    <MuiThemeProvider muiTheme={muiTheme}>
      <div id="container">
        <div id="headContainer">
          <Head />
        </div>
        <div id="bodyContainer">
          <Switch>
            <PrivateRoute path="/admin" exact component={Home} />
            <Route path="/admin/login" component={Login} />
            <PrivateRoute path="/admin/logout" component={Logout} />
            <PrivateRoute path="/admin/products" exact component={Products} />
            <PrivateRoute
              path="/admin/products/categories"
              exact
              component={ProductCategories}
            />
            <PrivateRoute path="/admin/orders" exact component={Orders} />
            <PrivateRoute
              path="/admin/orders/statuses"
              exact
              component={OrderStatuses}
            />
            <PrivateRoute
              path="/admin/order/:orderId"
              exact
              component={OrderDetails}
            />
            <PrivateRoute path="/admin/customers" exact component={Customers} />
            <PrivateRoute
              path="/admin/customers/groups"
              exact
              component={CustomerGroups}
            />
            <PrivateRoute
              path="/admin/customer/:customerId"
              exact
              component={CustomerDetails}
            />
            <PrivateRoute
              path="/admin/product/:productId"
              component={ProductDetails}
            />
            <PrivateRoute
              path="/admin/provider/:providerId?"
              exact
              component={ProviderDetails}
            />
            <PrivateRoute
              path="/admin/discount/:discountId?"
              exact
              component={DiscountDetails}
            />
            <PrivateRoute path="/admin/providers" exact component={Providers} />
            <PrivateRoute path="/admin/discounts" exact component={Discounts} />
            <PrivateRoute path="/admin/pages" exact component={Pages} />
            <PrivateRoute
              path="/admin/pages/add"
              exact
              component={PagesDetails}
            />
            <PrivateRoute
              path="/admin/pages/:pageId"
              component={PagesDetails}
            />
            <PrivateRoute path="/admin/settings" component={Settings} />
            <PrivateRoute path="/admin/apps" component={Apps} />
            <PrivateRoute path="/admin/files" exact component={Files} />
            <PrivateRoute
              path="/admin/credentials"
              exact
              component={Credentials}
            />
            <Route path="/admin/oauth/:token" component={OAuthCallback} />
            <Route component={NotFound} />
          </Switch>
        </div>
      </div>
    </MuiThemeProvider>
  </Router>
);
