import axios from "axios";
import settings from "../../../../config/admin";

export function getProductGeneralOptions() {
  const token = localStorage.getItem("dashboard_token");
  return axios
    .get(settings.apiBaseUrl + "/productsoptions?token=" + token)
    .then(res => {
      return { data: res.data };
    });
}

export function getProductOptionGeneralValues(jsonRaro) {
  let name = "";
  Object.keys(jsonRaro).forEach(key => {
    if (typeof jsonRaro[key] === "string") {
      name = name.concat(jsonRaro[key]);
    }
  });
  const token = localStorage.getItem("dashboard_token");
  return axios
    .get(
      settings.apiBaseUrl + "/productsoptionvalues/" + name + "?token=" + token
    )
    .then(res => {
      return { data: res.data };
    });
}
