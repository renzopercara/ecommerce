import axios from "axios";
import settings from "../../../../config/admin";

export function getProviders() {
  const token = localStorage.getItem("dashboard_token");
  return axios
    .get(settings.apiBaseUrl + "/providers?token=" + token)
    .then(res => {
      return { data: res.data };
    });
}

export function getProviderById(id) {
  const token = localStorage.getItem("dashboard_token");
  return axios
    .get(settings.apiBaseUrl + "/providers/" + id + "?token=" + token)
    .then(res => {
      return { data: res.data };
    });
}

export function doCreateProvider(data) {
  const token = localStorage.getItem("dashboard_token");
  return axios
    .post(settings.apiBaseUrl + "/providers?token=" + token, data)
    .then(res => {
      return { data: res.data };
    });
}

export function doUpdateProvider(id, data) {
  const token = localStorage.getItem("dashboard_token");
  return axios
    .put(settings.apiBaseUrl + "/providers/" + id + "?token=" + token, data)
    .then(res => {
      return { data: res.data };
    });
}
