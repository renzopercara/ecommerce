import axios from "axios";
import settings from "../../../../config/admin";

export function login(data) {
  return axios
    .post(settings.apiBaseUrl + "/auth/login", data)
    .then(res => {
      if (res.data && res.data.token) {
        return { token: res.data.token };
      }
      return { error: "Ocurrió un error inesperado" };
    })
    .catch(err => {
      return { error: err.response.data.error };
    });
}
