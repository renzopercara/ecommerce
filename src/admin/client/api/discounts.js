import axios from "axios";
import settings from "../../../../config/admin";

export function getDiscounts() {
  const token = localStorage.getItem("dashboard_token");
  return axios
    .get(settings.apiBaseUrl + "/discounts?token=" + token)
    .then(res => {
      return { data: res.data };
    });
}

export function getDiscountByProductId(id) {
  const token = localStorage.getItem("dashboard_token");
  return axios
    .get(settings.apiBaseUrl + "/productdiscount/" + id + "?token=" + token)
    .then(res => {
      return { data: res.data };
    });
}

export function getDiscountById(id) {
  const token = localStorage.getItem("dashboard_token");
  return axios
    .get(settings.apiBaseUrl + "/discounts/" + id + "?token=" + token)
    .then(res => {
      return { data: res.data };
    });
}

export function doCreateDiscount(data) {
  const token = localStorage.getItem("dashboard_token");
  return axios
    .post(settings.apiBaseUrl + "/discounts?token=" + token, data)
    .then(res => {
      return { data: res.data };
    });
}

export function doUpdateDiscount(id, data) {
  const token = localStorage.getItem("dashboard_token");
  return axios
    .put(settings.apiBaseUrl + "/discounts/" + id + "?token=" + token, data)
    .then(res => {
      return { data: res.data };
    });
}

export function doUpdateFactors(data) {
  const token = localStorage.getItem("dashboard_token");
  return axios
    .put(settings.apiBaseUrl + "/factors?token=" + token, data)
    .then(res => {
      return { data: res.data };
    });
}

export function getFactors() {
  const token = localStorage.getItem("dashboard_token");
  return axios
    .get(settings.apiBaseUrl + "/factors?token=" + token)
    .then(res => {
      return { data: res.data };
    });
}
