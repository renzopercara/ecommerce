import axios from "axios";
import settings from "../../../../config/admin";

export function doGetCredentials() {
  return axios
    .get(settings.apiBaseUrl + "/credentials")
    .then(res => res.data)
    .catch(err => {
      return { error: err.response.data.error };
    });
}

export function doDeleteCredential(id) {
  return axios
    .delete(settings.apiBaseUrl + "/credentials/" + id)
    .then(res => res.data)
    .catch(err => {
      return { error: err.response.data.error };
    });
}
