import React from "react";
import { Route, Redirect } from "react-router-dom";
import { validateCurrentToken, validateRoleAccess } from "../lib/auth";

const PrivateRoute = ({ component: Component, path, exact }) => (
  <Route
    path={path}
    exact={exact}
    render={props =>
      validateCurrentToken() && validateRoleAccess(path) ? (
        <Component {...props} />
      ) : (
        <Redirect to="/admin/login" />
      )
    }
  />
);

export default PrivateRoute;
