import settings from "./settings";
import api from "./api";
import messages from "./text";

const LOGIN_PATH = "/admin/login";
const HOME_PATH = "/admin/";
const STORE_PATH = "https://mintomin.com/";

export const roleRoutes = {
  provider: [
    "/admin/home",
    "/admin/order/:orderId",
    "/admin/products/categories",
    "/admin/products",
    "/admin/product/:productId",
    "/admin/orders",
    "/admin/orders/statuses",
    "/admin",
    "/admin/logout"
  ],
  client: ["/admin", "/admin/logout", "/admin/orders", "/admin/order/:orderId"]
};

const getParameterByName = (name, url) => {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return "";
  return decodeURIComponent(results[2].replace(/\+/g, " "));
};

export const getUserInfo = () => {
  if (isCurrentTokenValid()) {
    const token = localStorage.getItem("dashboard_token");
    const userData = parseJWT(token);
    return userData;
  }
  return {};
};

export const validateRoleAccess = path => {
  if (isCurrentTokenValid()) {
    const token = localStorage.getItem("dashboard_token");
    const userData = parseJWT(token);
    return (
      userData.role === "superadmin" ||
      (roleRoutes[userData.role] && roleRoutes[userData.role].includes(path))
    );
  }
  return false;
};

export const validateCurrentToken = () => {
  if (location.pathname !== LOGIN_PATH) {
    return isCurrentTokenValid();
  }
  return true;
};

export const storeToken = token => {
  const tokenData = parseJWT(token);
  saveToken({
    token,
    username: tokenData.username,
    id: tokenData._id,
    role: tokenData.role
  });
};

export const checkTokenFromUrl = () => {
  if (location.pathname === LOGIN_PATH) {
    const token = getParameterByName("token");
    if (token && token !== "") {
      const tokenData = parseJWT(token);

      if (tokenData) {
        const expiration_date = tokenData.exp * 1000;
        if (expiration_date > Date.now()) {
          saveToken({ token, email: tokenData.email, expiration_date });
          location.replace(HOME_PATH);
        } else {
          alert(messages.tokenExpired);
        }
      } else {
        alert(messages.tokenInvalid);
      }
    } else {
      if (isCurrentTokenValid()) {
        location.replace(HOME_PATH);
      }
    }
  }
};

const parseJWT = jwt => {
  try {
    const payload = jwt.split(".")[1];
    const tokenData = JSON.parse(atob(payload));
    return tokenData;
  } catch (e) {
    return null;
  }
};

const saveToken = data => {
  localStorage.setItem("dashboard_token", data.token);
  localStorage.setItem("dashboard_email", data.email);
  localStorage.setItem("dashboard_id", data.id);
  localStorage.setItem("dashboard_username", data.username);
  localStorage.setItem("dashboard_role", data.role);
  localStorage.setItem("dashboard_exp", data.expiration_date);
};

const isCurrentTokenValid = () => {
  // const expiration_date = localStorage.getItem('dashboard_exp');
  return localStorage.getItem("dashboard_token");
};

export const removeToken = () => {
  const role = localStorage.getItem("dashboard_role");
  localStorage.removeItem("dashboard_token");
  localStorage.removeItem("dashboard_role");
  localStorage.removeItem("dashboard_username");
  localStorage.removeItem("dashboard_id");
  localStorage.removeItem("dashboard_email");
  localStorage.removeItem("dashboard_exp");
  localStorage.removeItem("webstore_token");
  localStorage.removeItem("webstore_email");
  localStorage.removeItem("webstore_exp");
  if (role !== "client") {
    location.replace(LOGIN_PATH);
  } else {
    window.location = STORE_PATH;
  }
};
