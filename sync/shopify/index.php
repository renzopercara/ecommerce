<?php
$params = $_GET;

$api_key = "9f0cc7b57b036a0eaf05bb66e1d066e2";
$shared_secret = "fa2c0bf58da4bef3b338a964447c6ca0";

// Set variables for our request
$query = array(
  "client_id" => $api_key, // Your API key
  "client_secret" => $shared_secret, // Your app credentials (secret key)
  "code" => $params['code'] // Grab the access key from the URL
);

// Generate access token URL
$access_token_url = "https://" . $params['shop'] . "/admin/oauth/access_token";

// Configure curl client and execute request
$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_URL, $access_token_url);
curl_setopt($ch, CURLOPT_POST, count($query));
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($query));
$result = curl_exec($ch);
curl_close($ch);

// Store the access token
$result = json_decode($result, true);
$access_token = $result['access_token'];

$cod = explode('.',$params['shop']);

?>
<!doctype html>
<html lang="en">
  <head>
    <title>Sincronizar Proveedores</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body style="height:100vh!important;">
      <div class="row h-100">
          <div class="container">
            <div class="col-md-12 pt-5">
            <h1 class="text-center">Guardar Proveedor de Shopify</h1>
            <form action="/api/v1/providers" method="post">
                <div class="form-group">
                <label for="access_token">Token de Acceso</label>
                <input type="text"
                class="form-control" name="shopify_access_token" id="shopify_access_token" readonly value="<?php echo $access_token; ?>" aria-describedby="helpId" placeholder="">
                </div>
                <div class="form-group">
                <label for="access_token">Codigo</label>
                <input type="text"
                class="form-control" name="code" id="code" value="<?php echo $params['code']; ?>" readonly aria-describedby="helpId" placeholder="">
                </div>
                <div class="form-group">
                <label for="">Numero de Tienda</label>
                <input type="text"
                class="form-control" name="shopId" id="shopId" aria-describedby="helpId" readonly value="<?php echo $params['timestamp']; ?>" placeholder="">
                </div>
                <div class="form-group">
                <label for="">Seudonimo de Tienda</label>
                <input type="text"
                class="form-control" name="shopify_shop_name" id="shopify_shop_name" readonly value="<?php echo $cod['0']; ?>" aria-describedby="helpId" placeholder="">
                </div>
                <div class="form-group">
                <label for="">Nombre de Tienda</label>
                <input type="text"
                class="form-control" name="name" id="name" aria-describedby="helpId" placeholder="">
                </div>
                  <input type="hidden" class="form-control" name="shop_source" id="shop_source" value="shopify" aria-describedby="helpId" placeholder="">
                  <input type="hidden" class="form-control" name="synchronize" id="synchronize" value="true" aria-describedby="helpId" placeholder="">
                  <input type="hidden" class="form-control" name="status" id="status" value="active" aria-describedby="helpId" placeholder="">
                  <input type="hidden" class="form-control" name="accessToken" id="accessToken" value="<?php echo $access_token; ?>" aria-describedby="helpId" placeholder="">
                <button type="submit" class="btn btn-primary ml-auto">Guardar</button>
                </form>
            </div>
          </div>
      </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript">
      $("form").submit(function(event){
        event.preventDefault(); //prevent default action 
        var post_url = $(this).attr("action"); //get form action url
        var request_method = $(this).attr("method"); //get form GET/POST method
        var form_data = $(this).serialize(); //Encode form elements for submission
        
        $.ajax({
          url : post_url,
          type: request_method,
          data : form_data
        }).done(function(response){ //
          alert("A sido agregado como proveedor");
          close();
        });
      });
    </script>
  </body>
</html>