<?php
$cod = $_GET["code"];

?>
<?php
// abrimos la sesión cURL
$ch = curl_init();

// definimos la URL a la que hacemos la petición
curl_setopt($ch, CURLOPT_URL,"https://www.tiendanube.com/apps/authorize/token");
// indicamos el tipo de petición: POST
curl_setopt($ch, CURLOPT_POST, TRUE);
// definimos cada uno de los parámetros
curl_setopt($ch, CURLOPT_POSTFIELDS, "client_id=975&client_secret=f3XrJe8qsyF2GBpYuIdFhlzQqxQyJRNkLYgCjcEWoOch4QTt&grant_type=authorization_code&code=".$cod);

// recibimos la respuesta y la guardamos en una variable
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$remote_server_output = curl_exec ($ch);

// cerramos la sesión cURL
curl_close ($ch);


$container = json_decode($remote_server_output, true);

?>
<!doctype html>
<html lang="en">
  <head>
    <title>Sincronizar Proveedores</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body style="height:100vh!important;">
      <div class="row h-100">
          <div class="container">
            <div class="col-md-12 pt-5">
            <h1 class="text-center">Guardar Proveedor de TiendaNube</h1>
            <form action="/api/v1/providers" method="post">
                <div class="form-group">
                <label for="access_token">Token de Acceso</label>
                <input type="text"
                class="form-control" name="accessToken" id="accessToken" value="<?php echo $container['access_token']; ?>" readonly aria-describedby="helpId" placeholder="">
                </div>
                <div class="form-group">
                <label for="">Numero de Tienda</label>
                <input type="text"
                class="form-control" name="shopId" id="shopId" value="<?php echo $container['user_id']; ?>" readonly aria-describedby="helpId" placeholder="">
                </div>
                <div class="form-group">
                <label for="">Nombre de Tienda</label>
                <input type="text"
                class="form-control" name="name" id="name" aria-describedby="helpId" placeholder="">
                </div>
                <div class="form-group">
                <label for="">Correo de Tienda</label>
                <input type="text"
                class="form-control" name="userAgent" id="userAgent" aria-describedby="helpId" placeholder="">
                </div>
                  <input type="hidden" class="form-control" name="shop_source" id="shop_source" value="tiendanube" aria-describedby="helpId" placeholder="">
                  <input type="hidden" class="form-control" name="synchronize" id="synchronize" value="true" aria-describedby="helpId" placeholder="">
                  <input type="hidden" class="form-control" name="code" id="code" value="<?php echo $cod; ?>" aria-describedby="helpId" placeholder="">
                  <input type="hidden" class="form-control" name="status" id="status" value="active" aria-describedby="helpId" placeholder="">
                <button type="submit" class="btn btn-primary ml-auto">Guardar</button>
                </form>
            </div>
          </div>
      </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript">
      $("form").submit(function(event){
        event.preventDefault(); //prevent default action 
        var post_url = $(this).attr("action"); //get form action url
        var request_method = $(this).attr("method"); //get form GET/POST method
        var form_data = $(this).serialize(); //Encode form elements for submission
        
        $.ajax({
          url : post_url,
          type: request_method,
          data : form_data
        }).done(function(response){ //
          alert("A sido agregado como proveedor");
          close();
        });
      });
    </script>
  </body>
</html>